#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=/home/xhni/buildspace/branches/Trunk/build/toolchain/arm/gnu_cortex-a9_tools_glibc/gnu_cortex-a9_tools/usr/bin/arm-none-linux-gnueabi-ranlib
CC=/home/xhni/buildspace/branches/Trunk/build/toolchain/arm/gnu_cortex-a9_tools_glibc/gnu_cortex-a9_tools/usr/bin/arm-none-linux-gnueabi-gcc
CCC=/home/xhni/buildspace/branches/Trunk/build/toolchain/arm/gnu_cortex-a9_tools_glibc/gnu_cortex-a9_tools/usr/bin/arm-none-linux-gnueabi-g++
CXX=/home/xhni/buildspace/branches/Trunk/build/toolchain/arm/gnu_cortex-a9_tools_glibc/gnu_cortex-a9_tools/usr/bin/arm-none-linux-gnueabi-g++
FC=gfortran
AS=as
AR=/home/xhni/buildspace/branches/Trunk/build/toolchain/arm/gnu_cortex-a9_tools_glibc/gnu_cortex-a9_tools/usr/bin/arm-none-linux-gnueabi-ar

# Macros
CND_PLATFORM=NXP-Linux-x86
CND_CONF=NXP
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile.nb

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/ucciplayer.o \
	${OBJECTDIR}/src/iccspos.o \
	${OBJECTDIR}/src/actiontimer.o \
	${OBJECTDIR}/src/stdreferee.o \
	${OBJECTDIR}/src/gametimer.o \
	${OBJECTDIR}/src/alarm.o \
	${OBJECTDIR}/src/utility.o \
	${OBJECTDIR}/doc/doxygen/example/example0.o \
	${OBJECTDIR}/src/chessrow.o \
	${OBJECTDIR}/src/chessbook.o \
	${OBJECTDIR}/src/clock.o \
	${OBJECTDIR}/src/coordinator.o \
	${OBJECTDIR}/src/stdcoordinator.o \
	${OBJECTDIR}/src/chesspiece.o \
	${OBJECTDIR}/src/fen.o \
	${OBJECTDIR}/src/chessarea.o \
	${OBJECTDIR}/src/ucciengine.o \
	${OBJECTDIR}/doc/doxygen/example/example2.o \
	${OBJECTDIR}/src/gamemanager.o \
	${OBJECTDIR}/src/ucciutil.o \
	${OBJECTDIR}/src/stdrule.o \
	${OBJECTDIR}/src/iccsmotion.o \
	${OBJECTDIR}/src/piecepos.o \
	${OBJECTDIR}/doc/doxygen/example/example1.o \
	${OBJECTDIR}/src/game.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess \
	${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess

# C Compiler Flags
CFLAGS=--sysroot=/home/xhni/buildspace/branches/Trunk/build/toolchain/arm/gnu_cortex-a9_tools_glibc/gnu_cortex-a9_tools/

# CC Compiler Flags
CCFLAGS=--sysroot=/home/xhni/buildspace/branches/Trunk/build/toolchain/arm/gnu_cortex-a9_tools_glibc/gnu_cortex-a9_tools/ -std=c++0x
CXXFLAGS=--sysroot=/home/xhni/buildspace/branches/Trunk/build/toolchain/arm/gnu_cortex-a9_tools_glibc/gnu_cortex-a9_tools/ -std=c++0x

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess.a

${OBJECTDIR}/src/ucciplayer.o: src/ucciplayer.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/ucciplayer.o src/ucciplayer.cc

${OBJECTDIR}/src/iccspos.o: src/iccspos.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/iccspos.o src/iccspos.cc

${OBJECTDIR}/src/actiontimer.o: src/actiontimer.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/actiontimer.o src/actiontimer.cc

${OBJECTDIR}/src/stdreferee.o: src/stdreferee.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/stdreferee.o src/stdreferee.cc

${OBJECTDIR}/src/gametimer.o: src/gametimer.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/gametimer.o src/gametimer.cc

${OBJECTDIR}/src/alarm.o: src/alarm.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/alarm.o src/alarm.cc

${OBJECTDIR}/src/utility.o: src/utility.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/utility.o src/utility.cc

${OBJECTDIR}/doc/doxygen/example/example0.o: doc/doxygen/example/example0.cc 
	${MKDIR} -p ${OBJECTDIR}/doc/doxygen/example
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/doc/doxygen/example/example0.o doc/doxygen/example/example0.cc

${OBJECTDIR}/src/chessrow.o: src/chessrow.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/chessrow.o src/chessrow.cc

${OBJECTDIR}/src/chessbook.o: src/chessbook.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/chessbook.o src/chessbook.cc

${OBJECTDIR}/src/clock.o: src/clock.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/clock.o src/clock.cc

${OBJECTDIR}/src/coordinator.o: src/coordinator.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/coordinator.o src/coordinator.cc

${OBJECTDIR}/src/stdcoordinator.o: src/stdcoordinator.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/stdcoordinator.o src/stdcoordinator.cc

${OBJECTDIR}/src/chesspiece.o: src/chesspiece.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/chesspiece.o src/chesspiece.cc

${OBJECTDIR}/src/fen.o: src/fen.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/fen.o src/fen.cc

${OBJECTDIR}/src/chessarea.o: src/chessarea.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/chessarea.o src/chessarea.cc

${OBJECTDIR}/src/ucciengine.o: src/ucciengine.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/ucciengine.o src/ucciengine.cc

${OBJECTDIR}/doc/doxygen/example/example2.o: doc/doxygen/example/example2.cc 
	${MKDIR} -p ${OBJECTDIR}/doc/doxygen/example
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/doc/doxygen/example/example2.o doc/doxygen/example/example2.cc

${OBJECTDIR}/src/gamemanager.o: src/gamemanager.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/gamemanager.o src/gamemanager.cc

${OBJECTDIR}/src/ucciutil.o: src/ucciutil.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/ucciutil.o src/ucciutil.cc

${OBJECTDIR}/src/stdrule.o: src/stdrule.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/stdrule.o src/stdrule.cc

${OBJECTDIR}/src/iccsmotion.o: src/iccsmotion.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/iccsmotion.o src/iccsmotion.cc

${OBJECTDIR}/src/piecepos.o: src/piecepos.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/piecepos.o src/piecepos.cc

${OBJECTDIR}/doc/doxygen/example/example1.o: doc/doxygen/example/example1.cc 
	${MKDIR} -p ${OBJECTDIR}/doc/doxygen/example
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/doc/doxygen/example/example1.o doc/doxygen/example/example1.cc

${OBJECTDIR}/src/game.o: src/game.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/game.o src/game.cc

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-conf ${TESTFILES}
${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestAlarm.o ${TESTDIR}/tests/TestAlarmRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestChessArea.o ${TESTDIR}/tests/TestChessAreaRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestChessBook.o ${TESTDIR}/tests/TestChessBookRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestChessPiece.o ${TESTDIR}/tests/TestChessPieceRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestChessRow.o ${TESTDIR}/tests/TestChessRowRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestFEN.o ${TESTDIR}/tests/TestFENRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestGame.o ${TESTDIR}/tests/TestGameRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestGameManager.o ${TESTDIR}/tests/TestGameManagerRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestGameTimer.o ${TESTDIR}/tests/TestGameTimerRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestICCSMotion.o ${TESTDIR}/tests/TestICCSMotionRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestPiecePos.o ${TESTDIR}/tests/TestPiecePosRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestRealClock.o ${TESTDIR}/tests/TestRealClockRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestStdCoordinator.o ${TESTDIR}/tests/TestStdCoordinatorRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestStdrule.o ${TESTDIR}/tests/TestStdruleRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestUCCIEngine.o ${TESTDIR}/tests/TestUCCIEngineRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess: ${TESTDIR}/tests/TestUCCIPlayer.o ${TESTDIR}/tests/TestUCCIPlayerRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc}   -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess $^ ${LDLIBSOPTIONS} 


${TESTDIR}/tests/TestAlarm.o: tests/TestAlarm.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestAlarm.o tests/TestAlarm.cc


${TESTDIR}/tests/TestAlarmRunner.o: tests/TestAlarmRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestAlarmRunner.o tests/TestAlarmRunner.cc


${TESTDIR}/tests/TestChessArea.o: tests/TestChessArea.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestChessArea.o tests/TestChessArea.cc


${TESTDIR}/tests/TestChessAreaRunner.o: tests/TestChessAreaRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestChessAreaRunner.o tests/TestChessAreaRunner.cc


${TESTDIR}/tests/TestChessBook.o: tests/TestChessBook.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestChessBook.o tests/TestChessBook.cc


${TESTDIR}/tests/TestChessBookRunner.o: tests/TestChessBookRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestChessBookRunner.o tests/TestChessBookRunner.cc


${TESTDIR}/tests/TestChessPiece.o: tests/TestChessPiece.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestChessPiece.o tests/TestChessPiece.cc


${TESTDIR}/tests/TestChessPieceRunner.o: tests/TestChessPieceRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestChessPieceRunner.o tests/TestChessPieceRunner.cc


${TESTDIR}/tests/TestChessRow.o: tests/TestChessRow.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestChessRow.o tests/TestChessRow.cc


${TESTDIR}/tests/TestChessRowRunner.o: tests/TestChessRowRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestChessRowRunner.o tests/TestChessRowRunner.cc


${TESTDIR}/tests/TestFEN.o: tests/TestFEN.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestFEN.o tests/TestFEN.cc


${TESTDIR}/tests/TestFENRunner.o: tests/TestFENRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestFENRunner.o tests/TestFENRunner.cc


${TESTDIR}/tests/TestGame.o: tests/TestGame.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestGame.o tests/TestGame.cc


${TESTDIR}/tests/TestGameRunner.o: tests/TestGameRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestGameRunner.o tests/TestGameRunner.cc


${TESTDIR}/tests/TestGameManager.o: tests/TestGameManager.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestGameManager.o tests/TestGameManager.cc


${TESTDIR}/tests/TestGameManagerRunner.o: tests/TestGameManagerRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestGameManagerRunner.o tests/TestGameManagerRunner.cc


${TESTDIR}/tests/TestGameTimer.o: tests/TestGameTimer.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestGameTimer.o tests/TestGameTimer.cc


${TESTDIR}/tests/TestGameTimerRunner.o: tests/TestGameTimerRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestGameTimerRunner.o tests/TestGameTimerRunner.cc


${TESTDIR}/tests/TestICCSMotion.o: tests/TestICCSMotion.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestICCSMotion.o tests/TestICCSMotion.cc


${TESTDIR}/tests/TestICCSMotionRunner.o: tests/TestICCSMotionRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestICCSMotionRunner.o tests/TestICCSMotionRunner.cc


${TESTDIR}/tests/TestPiecePos.o: tests/TestPiecePos.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestPiecePos.o tests/TestPiecePos.cc


${TESTDIR}/tests/TestPiecePosRunner.o: tests/TestPiecePosRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestPiecePosRunner.o tests/TestPiecePosRunner.cc


${TESTDIR}/tests/TestRealClock.o: tests/TestRealClock.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestRealClock.o tests/TestRealClock.cc


${TESTDIR}/tests/TestRealClockRunner.o: tests/TestRealClockRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestRealClockRunner.o tests/TestRealClockRunner.cc


${TESTDIR}/tests/TestStdCoordinator.o: tests/TestStdCoordinator.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestStdCoordinator.o tests/TestStdCoordinator.cc


${TESTDIR}/tests/TestStdCoordinatorRunner.o: tests/TestStdCoordinatorRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestStdCoordinatorRunner.o tests/TestStdCoordinatorRunner.cc


${TESTDIR}/tests/TestStdrule.o: tests/TestStdrule.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestStdrule.o tests/TestStdrule.cc


${TESTDIR}/tests/TestStdruleRunner.o: tests/TestStdruleRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestStdruleRunner.o tests/TestStdruleRunner.cc


${TESTDIR}/tests/TestUCCIEngine.o: tests/TestUCCIEngine.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestUCCIEngine.o tests/TestUCCIEngine.cc


${TESTDIR}/tests/TestUCCIEngineRunner.o: tests/TestUCCIEngineRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestUCCIEngineRunner.o tests/TestUCCIEngineRunner.cc


${TESTDIR}/tests/TestUCCIPlayer.o: tests/TestUCCIPlayer.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestUCCIPlayer.o tests/TestUCCIPlayer.cc


${TESTDIR}/tests/TestUCCIPlayerRunner.o: tests/TestUCCIPlayerRunner.cc 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/TestUCCIPlayerRunner.o tests/TestUCCIPlayerRunner.cc


${OBJECTDIR}/src/ucciplayer_nomain.o: ${OBJECTDIR}/src/ucciplayer.o src/ucciplayer.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/ucciplayer.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/ucciplayer_nomain.o src/ucciplayer.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/ucciplayer.o ${OBJECTDIR}/src/ucciplayer_nomain.o;\
	fi

${OBJECTDIR}/src/iccspos_nomain.o: ${OBJECTDIR}/src/iccspos.o src/iccspos.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/iccspos.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/iccspos_nomain.o src/iccspos.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/iccspos.o ${OBJECTDIR}/src/iccspos_nomain.o;\
	fi

${OBJECTDIR}/src/actiontimer_nomain.o: ${OBJECTDIR}/src/actiontimer.o src/actiontimer.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/actiontimer.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/actiontimer_nomain.o src/actiontimer.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/actiontimer.o ${OBJECTDIR}/src/actiontimer_nomain.o;\
	fi

${OBJECTDIR}/src/stdreferee_nomain.o: ${OBJECTDIR}/src/stdreferee.o src/stdreferee.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/stdreferee.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/stdreferee_nomain.o src/stdreferee.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/stdreferee.o ${OBJECTDIR}/src/stdreferee_nomain.o;\
	fi

${OBJECTDIR}/src/gametimer_nomain.o: ${OBJECTDIR}/src/gametimer.o src/gametimer.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/gametimer.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/gametimer_nomain.o src/gametimer.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/gametimer.o ${OBJECTDIR}/src/gametimer_nomain.o;\
	fi

${OBJECTDIR}/src/alarm_nomain.o: ${OBJECTDIR}/src/alarm.o src/alarm.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/alarm.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/alarm_nomain.o src/alarm.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/alarm.o ${OBJECTDIR}/src/alarm_nomain.o;\
	fi

${OBJECTDIR}/src/utility_nomain.o: ${OBJECTDIR}/src/utility.o src/utility.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/utility.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/utility_nomain.o src/utility.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/utility.o ${OBJECTDIR}/src/utility_nomain.o;\
	fi

${OBJECTDIR}/doc/doxygen/example/example0_nomain.o: ${OBJECTDIR}/doc/doxygen/example/example0.o doc/doxygen/example/example0.cc 
	${MKDIR} -p ${OBJECTDIR}/doc/doxygen/example
	@NMOUTPUT=`${NM} ${OBJECTDIR}/doc/doxygen/example/example0.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/doc/doxygen/example/example0_nomain.o doc/doxygen/example/example0.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/doc/doxygen/example/example0.o ${OBJECTDIR}/doc/doxygen/example/example0_nomain.o;\
	fi

${OBJECTDIR}/src/chessrow_nomain.o: ${OBJECTDIR}/src/chessrow.o src/chessrow.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/chessrow.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/chessrow_nomain.o src/chessrow.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/chessrow.o ${OBJECTDIR}/src/chessrow_nomain.o;\
	fi

${OBJECTDIR}/src/chessbook_nomain.o: ${OBJECTDIR}/src/chessbook.o src/chessbook.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/chessbook.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/chessbook_nomain.o src/chessbook.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/chessbook.o ${OBJECTDIR}/src/chessbook_nomain.o;\
	fi

${OBJECTDIR}/src/clock_nomain.o: ${OBJECTDIR}/src/clock.o src/clock.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/clock.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/clock_nomain.o src/clock.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/clock.o ${OBJECTDIR}/src/clock_nomain.o;\
	fi

${OBJECTDIR}/src/coordinator_nomain.o: ${OBJECTDIR}/src/coordinator.o src/coordinator.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/coordinator.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/coordinator_nomain.o src/coordinator.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/coordinator.o ${OBJECTDIR}/src/coordinator_nomain.o;\
	fi

${OBJECTDIR}/src/stdcoordinator_nomain.o: ${OBJECTDIR}/src/stdcoordinator.o src/stdcoordinator.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/stdcoordinator.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/stdcoordinator_nomain.o src/stdcoordinator.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/stdcoordinator.o ${OBJECTDIR}/src/stdcoordinator_nomain.o;\
	fi

${OBJECTDIR}/src/chesspiece_nomain.o: ${OBJECTDIR}/src/chesspiece.o src/chesspiece.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/chesspiece.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/chesspiece_nomain.o src/chesspiece.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/chesspiece.o ${OBJECTDIR}/src/chesspiece_nomain.o;\
	fi

${OBJECTDIR}/src/fen_nomain.o: ${OBJECTDIR}/src/fen.o src/fen.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/fen.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/fen_nomain.o src/fen.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/fen.o ${OBJECTDIR}/src/fen_nomain.o;\
	fi

${OBJECTDIR}/src/chessarea_nomain.o: ${OBJECTDIR}/src/chessarea.o src/chessarea.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/chessarea.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/chessarea_nomain.o src/chessarea.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/chessarea.o ${OBJECTDIR}/src/chessarea_nomain.o;\
	fi

${OBJECTDIR}/src/ucciengine_nomain.o: ${OBJECTDIR}/src/ucciengine.o src/ucciengine.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/ucciengine.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/ucciengine_nomain.o src/ucciengine.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/ucciengine.o ${OBJECTDIR}/src/ucciengine_nomain.o;\
	fi

${OBJECTDIR}/doc/doxygen/example/example2_nomain.o: ${OBJECTDIR}/doc/doxygen/example/example2.o doc/doxygen/example/example2.cc 
	${MKDIR} -p ${OBJECTDIR}/doc/doxygen/example
	@NMOUTPUT=`${NM} ${OBJECTDIR}/doc/doxygen/example/example2.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/doc/doxygen/example/example2_nomain.o doc/doxygen/example/example2.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/doc/doxygen/example/example2.o ${OBJECTDIR}/doc/doxygen/example/example2_nomain.o;\
	fi

${OBJECTDIR}/src/gamemanager_nomain.o: ${OBJECTDIR}/src/gamemanager.o src/gamemanager.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/gamemanager.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/gamemanager_nomain.o src/gamemanager.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/gamemanager.o ${OBJECTDIR}/src/gamemanager_nomain.o;\
	fi

${OBJECTDIR}/src/ucciutil_nomain.o: ${OBJECTDIR}/src/ucciutil.o src/ucciutil.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/ucciutil.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/ucciutil_nomain.o src/ucciutil.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/ucciutil.o ${OBJECTDIR}/src/ucciutil_nomain.o;\
	fi

${OBJECTDIR}/src/stdrule_nomain.o: ${OBJECTDIR}/src/stdrule.o src/stdrule.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/stdrule.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/stdrule_nomain.o src/stdrule.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/stdrule.o ${OBJECTDIR}/src/stdrule_nomain.o;\
	fi

${OBJECTDIR}/src/iccsmotion_nomain.o: ${OBJECTDIR}/src/iccsmotion.o src/iccsmotion.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/iccsmotion.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/iccsmotion_nomain.o src/iccsmotion.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/iccsmotion.o ${OBJECTDIR}/src/iccsmotion_nomain.o;\
	fi

${OBJECTDIR}/src/piecepos_nomain.o: ${OBJECTDIR}/src/piecepos.o src/piecepos.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/piecepos.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/piecepos_nomain.o src/piecepos.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/piecepos.o ${OBJECTDIR}/src/piecepos_nomain.o;\
	fi

${OBJECTDIR}/doc/doxygen/example/example1_nomain.o: ${OBJECTDIR}/doc/doxygen/example/example1.o doc/doxygen/example/example1.cc 
	${MKDIR} -p ${OBJECTDIR}/doc/doxygen/example
	@NMOUTPUT=`${NM} ${OBJECTDIR}/doc/doxygen/example/example1.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/doc/doxygen/example/example1_nomain.o doc/doxygen/example/example1.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/doc/doxygen/example/example1.o ${OBJECTDIR}/doc/doxygen/example/example1_nomain.o;\
	fi

${OBJECTDIR}/src/game_nomain.o: ${OBJECTDIR}/src/game.o src/game.cc 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/game.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Werror -Isrc/include/cnchess -I. -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/game_nomain.o src/game.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/src/game.o ${OBJECTDIR}/src/game_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	    ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libcnchess.a

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
