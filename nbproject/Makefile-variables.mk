#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug=libcnchess.a
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux-x86/libcnchess.a
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=libchess.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/libchess.tar
# Release configuration
CND_PLATFORM_Release=MinGW-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW-Windows
CND_ARTIFACT_NAME_Release=liblibchess.a
CND_ARTIFACT_PATH_Release=dist/Release/MinGW-Windows/liblibchess.a
CND_PACKAGE_DIR_Release=dist/Release/MinGW-Windows/package
CND_PACKAGE_NAME_Release=libchess.tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW-Windows/package/libchess.tar
# NXP configuration
CND_PLATFORM_NXP=NXP-Linux-x86
CND_ARTIFACT_DIR_NXP=dist/NXP/NXP-Linux-x86
CND_ARTIFACT_NAME_NXP=libcnchess.a
CND_ARTIFACT_PATH_NXP=dist/NXP/NXP-Linux-x86/libcnchess.a
CND_PACKAGE_DIR_NXP=dist/NXP/NXP-Linux-x86/package
CND_PACKAGE_NAME_NXP=libcnchess.tar
CND_PACKAGE_PATH_NXP=dist/NXP/NXP-Linux-x86/package/libcnchess.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
