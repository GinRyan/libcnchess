/**
 * 实例2: example1.cc 演示一个真正的象棋游戏
 * 
 * 编译方法:
 * g++ -Ilibcnchess/src/include -lcnchess -lm -lrt -pthread -o example1 example1.cc
 */

#if 0

#include <iostream>
// 包含 libcnchess 的头文件
#include "cnchess/cnchess.h"

using namespace std;
// 导入 libcnchess 的命名空间
using namespace cnchess;

class UIMain;
class UIDemoPlayer;


/* 主入口函数 */
int main() 
{
	// 创建你扮演的玩家
	GamePlayer* red = new UIDemoPlayer();

	// 创建 UCCI AI 引擎玩家, 并指定使用 "象眼" AI 引擎
	GamePlayer* black = new UCCIPlayer("/usr/bin/eleyee");

	// 创建裁判, 该裁判支持基本的中国象棋规则
	GameReferee* referee = new STDReferee();

	// 可选, 设定游戏时间为 60 分钟, 用户每次最多思考时间 3 分钟
	TimerInfo* timeri = new TimerInfo(60 * 60, 60 * 3);

	// 创建开局棋盘
	ChessBook cb;
	stdrule_initchessbook(cb);
	// 创建棋局, 并设置红方先手
	Game* game = new Game(cb, red, black, referee, timeri, RL_RED);
	game->setName("珍胧棋局");
	game->setDescription("迎得清风驱魔障 - 为夺百年北冥神功功力, 丁春秋飘渺峰上珍胧棋局 PK 逍遥子");
	// 从用 FEN 字符串(参见fen规范) 表示的当时局势, 创建残局棋盘
	fen_parse(game->getBoard(), "rnbakabnr/9/1c5c1/p1p1p1p1p/9/9/9/1C5C1/9/RNBAKABNR");
	// 设置下一步轮到黑方
	game->setNext(RL_BLACK);
	
	// 创建 UI 主程序
	UIMain ui;

	// 创建游戏管理者
	GameManager gmgr(game);
	
	// 设置当玩家每一步超时或者全部时间用完后的处理函数
	gmgr.setExpiredHandler(UIMain::expiredHandler, &ui);
	
	// ui 独立主线程进入死循环, 开始绘制
	ui.loopForever();
	
	// 启动游戏, 双方玩家会自己互动, 直至有一方认输, 求和, 或者一方的帅被吃掉
	Game::Status gamest = gmgr.start();

	// 游戏完结, 输出游戏状态
	cout << "game over, status is " << gamest << endl;
	
	return 0;
}

class UIMain {
public:
	
	void loopForever()
	{
		// ui 创建主线程, 主循环, 主动查询玩家和game状态等 ....
	}
	
	/* 重新绘制棋盘 */
	void boardRender(const ChessBook& newcb);
	
	/* 显示加时对话框 */
	void showOvertimeDialog();
	
	/**
	 当有玩家超时的时候会被调用
	 */
	static void expiredHandler(OvertimeHandle& handle, void* ctx, 
		const Game* game, const GamePlayer* player, GameTimer::ExpiredType exptype)
	{
		UIMain* ui = (UIMain*) ctx;
		
		//弹出对话框问对手是否允许给超时方加时
		if (ui->showOvertimeDialog()) {
			handle.result = OvertimeHandle::OVERTIME;
			// 给超时者加时 3 分钟
			handle.overtime = 60 * 3;
		}
	}
	
};

/**
 * UI 将扮演一个象棋玩家
 */
class UIDemoPlayer : public GamePlayer {

public:
	
	// 实现此方法来响应对手和游戏管理者的请求
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		// 启动时钟计时
		timer.start();
		
		// 根据请求类型来决定动作
		switch (ac.action) {
		case AC_REQ_MOVE:
			/* 对手或者游戏管理者要求我移动棋子(下棋) */
			
			
			/* 棋盘有更新, 也就是有玩家下棋了, 则重新绘制棋盘 */
			ui->boardRender(getGame()->getHistory()[getGame()->getHistory().size() - 1]);
		
			// a9a8 为 ICCS 标准的棋子移动表示法
			if (coordinator.move(this, "a9a8")) {
				return HANDLED;
			} else {
				return ERROR;
			}
		case AC_REQ_DRAW:
			/* 对手请求和棋 */
			
			// 忽略请求, 也就是不同意和棋
			return IGNORED;
		case AC_REQ_GIVEUP:
			/* 对手熟了或者认输了 */
			
			return HANDLED;
		case AC_REQ_RETRACT:
			/* 对手请求悔棋 */
			
			// 忽略请求, 也就是不同意悔棋
			return IGNORED;
		};
		
		return ERROR;
	}

}

#endif
