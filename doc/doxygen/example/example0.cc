/**
 * 实例1: example0.cc 演示一个最简单的象棋游戏
 * 
 * 编译方法:
 * g++ -Ilibcnchess/src/include -lcnchess -lm -lrt -pthread -o example0 example0.cc
 */

#if 0

#include <iostream>
// 包含 libcnchess 的头文件
#include "cnchess/cnchess.h"

using namespace std;
// 导入 libcnchess 的命名空间
using namespace cnchess;

class UIDemoPlayer;


/* 主入口函数 */
int main() 
{
	// 创建你扮演的玩家
	GamePlayer* red = new UIDemoPlayer();

	// 创建 UCCI AI 引擎玩家, 并指定使用 "象眼" AI 引擎
	GamePlayer* black = new UCCIPlayer(new UCCIEngine("/usr/bin/eleeye"));

	// 创建裁判, 该裁判支持基本的中国象棋规则
	GameReferee* referee = new STDReferee();

	// 可选, 设定游戏时间为 60 分钟, 用户每次最多思考时间 3 分钟
	TimerInfo* timeri = new TimerInfo(60 * 60, 60 * 3);

	// 创建一个标准中国象棋开局的棋盘
	ChessBook cb;
	stdrule_initchessbook(cb);
	
	// 创建新游戏, 这将创建一个标准中国象棋布局的棋盘
	Game* game = new Game(cb, red, black, referee, timeri);

	// 创建游戏管理者
	GameManager gmgr(game);

	// 启动游戏, 双方玩家会自己互动, 直至有一方认输, 求和, 或者一方的帅被吃掉
	Game::Status gamest = gmgr.start();

	// 游戏完结, 输出游戏状态
	cout << "game over, status is " << gamest << endl;
	
	return 0;
}

/**
 * UI 将扮演一个象棋玩家
 */
class UIDemoPlayer : public GamePlayer {

public:
	
	// 实现此方法来响应对手和游戏管理者的请求
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		// 启动时钟计时
		timer.start();
		
		// 根据请求类型来决定动作
		switch (ac.action) {
		case AC_REQ_MOVE:
			/* 对手或者游戏管理者要求我移动棋子(下棋) */
			
			// a9a8 为 ICCS 标准的棋子移动表示法
			if (coordinator.move(this, "a9a8")) {
				return HANDLED;
			} else {
				return ERROR;
			}
		case AC_REQ_DRAW:
			/* 对手请求和棋 */
			
			// 忽略请求, 也就是不同意和棋
			return IGNORED;
		case AC_REQ_GIVEUP:
			/* 对手熟了或者认输了 */
			
			return HANDLED;
		case AC_REQ_RETRACT:
			/* 对手请求悔棋 */
			
			// 忽略请求, 也就是不同意悔棋
			return IGNORED;
		};
		
		return ERROR;
	}

}

#endif
