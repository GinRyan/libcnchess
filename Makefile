RELEASE ?= Debug
PLATFORM ?= linux

all:
	$(MAKE) -f nbproject/Makefile-$(RELEASE).mk .build-conf

doc:
	doxygen doc/doxygen/doxy.config
	
clean:
	$(MAKE) -f nbproject/Makefile-$(RELEASE).mk .clean-conf
	
test:
	$(MAKE) -f nbproject/Makefile-$(RELEASE).mk test
	
.PHONY : clean doc all test

