/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_UTILITIES_H_
#define CNCS_UTILITIES_H_ 1

#include <string>
#include <cstdio>
#include <cstdarg>
#include <vector>
#include <time.h>

#ifdef ANDROID
#define pdebug(fmt, ...)	androidLog(fmt,##__VA_ARGS__)
#else

#if !defined(NDEBUG)
#define pdebug(fmt, ...)									\
	do {													\
		fprintf(stderr, "[D] [%s:%d]\n", __FILE__, __LINE__);	\
		fprintf(stderr, "[D] ");							\
		fprintf(stderr, (fmt), ##__VA_ARGS__);				\
		fprintf(stderr, "\n");								\
	} while (0)
#else
#define pdebug(fmt, ...)					
#endif

#endif

#define ts2sec(ts) ((ts).tv_sec + (long)((ts).tv_nsec / 1000000000))
#define ts2millisec(ts) ((ts).tv_sec * 1000 + (long)((ts).tv_nsec / 1000000))
#define ts2microsec(ts) ((ts).tv_sec * 1000000 + (long)((ts).tv_nsec / 1000))
#define ts2nanosec(ts)	((ts).tv_sec * 1000000000 + (ts).tv_nsec)


typedef std::vector<std::string> StringList;

std::string format(const std::string& str, ...);
std::string vformat(const std::string& str, va_list arg_list);
std::string& ltrim(std::string& dest, const std::string& str);
std::string& rtrim(std::string& dest, const std::string& str);
std::string& trim(std::string& dest, const std::string& str);
std::string join(const std::string& str, const StringList& slist);
StringList& split(StringList& dest, const std::string& str);
StringList& split(StringList& dest, const std::string& str, char delim);

std::string asstring(int number);
std::string asstring(unsigned int number);
std::string asstring(long number);
std::string asstring(unsigned long number);
std::string asstring(float number);
std::string asstring(double number);

std::string ashexstring(int number);
std::string ashexstring(unsigned int number);
std::string ashexstring(long number);
std::string ashexstring(unsigned long number);

std::string asupper(const std::string& str);
std::string aslower(const std::string& str);

int ctoi(char c);

/**
 * sleep and ignored signal
 * 
 * @param millseconds
 */
void sleepfor(int millseconds);

/**
 * 检查一个文件对于当前用户来说是否可执行
 * 
 * @param file
 * @return 
 */
bool is_executable(const std::string& file);

/**
 * 等待一个信号发生直到超时
 * 
 * @param signo
 * @param timeout
 * @return 信号发生返回 0, 超时返回 1, 错误返回 -1, 并设置 errno
 */
int wait4signal(int signo, const struct timespec& timeout);

/**
 * \brief io 状态类型
 * \see wait4io()
 */
enum IOStatus {
	IO_READABLE,
	IO_WRITABLE,
	IO_ERROR
};

/**
 * 检测一个文件描述符的状态
 * 
 * @param fd
 * @param iost
 * @param timeo 如果为 NULL 则一直等待, 如果为 timeo 都为 0 则立即检测 io 状态并返回
 * @return 成功返回 true, 失败或者超时返回 false
 */
bool wait4io(int fd, enum IOStatus iost, const struct timeval* timeo);
#ifdef ANDROID
void androidLog(const char * fmt, ...);
#endif

#endif
