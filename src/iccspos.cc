#include <ctype.h>
#include <stdexcept>
#include "iccsmotion.h"
#include "utility.h"

using namespace std;

namespace cnchess {

	ICCSPos::ICCSPos(const PiecePos& pos)
	{
		x = iccsmotion_x_range[pos.getX()];
		y = -(pos.getY() - (ChessBook::size() - 1));
	}

	void ICCSPos::setX(char x)
	{
		char x_upper = toupper(x);
		for (size_t i = 0; i < sizeof(iccsmotion_x_range); i++) {
			if (iccsmotion_x_range[i] == x_upper) {
				this->x = x_upper;
				return;
			}
		}
		throw invalid_argument(format("x '%d' invalid", x));
	}

	void ICCSPos::setY(int y)
	{
		if ((y >= cnchess::iccsmotion_y_range[0]) &&
			(y <= cnchess::iccsmotion_y_range[9])) {
			this->y = y;
		} else {
			throw invalid_argument(format("y '%d' invalid", y));
		}
	}

	PiecePos* ICCSPos::toPiecePos(PiecePos& pos) const
	{
		pos.setY(iccsmotion_ypos4chessbook(y));
		pos.setX(iccsmotion_xpos4chessbook(x));
		return &pos;
	}

}
