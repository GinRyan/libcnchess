/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#include <assert.h>
#include "game.h"
#include "gameplayer.h"
#include "gamereferee.h"
#include "gametimer.h"

using namespace std;

namespace cnchess {

	Game::Game(const ChessBook& cb, GamePlayer *red_player, GamePlayer *black_player,
		GameReferee* referee, TimerInfo* timeri, GameRole firstplayer)
	: red_player(red_player), black_player(black_player),
	referee(referee), current_player(NULL), status(Game::PAUSED)
	{
		assert(red_player && black_player && referee);
		assert(red_player != black_player);

		if (firstplayer == RL_RED) {
			first = red_player;
			next = black_player;
		} else {
			first = black_player;
			next = red_player;
		}
		current_player = first;

		chessbook = cb;
		board = chessbook;

		timer_info = NULL;
		if (timeri) {
			setTimer(timeri);
		}
	}

	Game::~Game()
	{
		red_player->decref();
		black_player->decref();
		referee->decref();
		if (timer_info) {
			timer_info->decref();
		}
	}

	void Game::setName(const std::string& name)
	{
		mutex.lock();
		this->name = name;
		mutex.unlock();
	}

	void Game::setDescription(const std::string& description)
	{
		mutex.lock();
		this->description = description;
		mutex.unlock();
	}

	const TimerInfo* Game::getTimer() const
	{
		if (timer_info) {
			return timer_info->total ? timer_info : NULL;
		} else {
			return NULL;
		}
	}
	
	void Game::setTimer(TimerInfo* timer)
	{
		long total = timer->total;
		long used = timer->used;
		long prestep = timer->prestep;

		if (total <= 0) {
			total = 0;
			used = 0;
			prestep = 0;
		}
		if (used > total) {
			used = total;
		}
		if (prestep > total) {
			prestep = total;
		}
		mutex.lock();
		timer_info = timer;
		timer_info->total = total;
		timer_info->used = used;
		timer_info->prestep = prestep;
		mutex.unlock();
	}

	void Game::setNext(GamePlayer* next)
	{
		assert((next == red_player) || (next == black_player));

		mutex.lock();
		this->next = next;
		this->current_player = getAdversary(next);
		mutex.unlock();
	}

	void Game::setNext(GameRole next)
	{
		mutex.lock();
		if (next == RL_RED) {
			this->next = red_player;
		} else {
			this->next = black_player;
		}
		this->current_player = getAdversary(this->next);
		mutex.unlock();
	}

	void Game::setStatus(Game::Status st)
	{
		mutex.lock();
		this->status = st;
		mutex.unlock();
		status_ev_dispatcher.fire(this, st);
	}

}
