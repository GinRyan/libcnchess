/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#include <stdexcept>
#include "chessrow.h"
#include "utility.h"

#define get_out_of_range_emsg(index)	format("index %d out of range", index).c_str()


using namespace std;

namespace cnchess {

	ChessRow::ChessRow()
	{
		mutex = new Mutex;
	}

	ChessRow::~ChessRow()
	{
		delete mutex;
	}

	ChessRow::ChessRow(const ChessRow& other)
	{
		for (int i = 0; i < 9; i++) {
			this->row[i] = other.row[i];
		}
		this->mutex = new Mutex;
	}

	ChessRow& ChessRow::operator =(const ChessRow& other)
	{
		this->mutex->lock();
		for (int i = 0; i < 9; i++) {
			this->row[i] = other.row[i];
		}
		this->mutex->unlock();
		return *this;
	}

	ChessPiece& ChessRow::operator[](int index)
	{
		if (index >= (int)size() || index < 0) {
			throw out_of_range(get_out_of_range_emsg(index));
		}
		return row[index];
	}

	const ChessPiece& ChessRow::operator[](int index) const
	{
		if (index >= size() || index < 0) {
			throw out_of_range(get_out_of_range_emsg(index));
		}
		return row[index];
	}

	void ChessRow::clear()
	{
		mutex->lock();
		for (int i = 0; i < size(); i++) {
			row[i].setType(ChessPiece::NONE);
			row[i].setOwner(RL_NONE);
		}
		mutex->unlock();
	}

	bool ChessRow::equal(const ChessRow& other) const
	{
		mutex->lock();
		for (int i = 0; i < size(); i++) {
			if (!row[i].isType(other[i].getType()) || (row[i].getOwner() != other[i].getOwner())) {
				mutex->unlock();
				return false;
			}
		}
		mutex->unlock();
		return true;
	}

	bool ChessRow::isEmpty() const
	{
		mutex->lock();
		for (int j = 0; j < size(); j++) {
			if (!row[j].isType(ChessPiece::NONE)) {
				mutex->unlock();
				return false;
			}
		}
		mutex->unlock();
		return true;
	}

}
