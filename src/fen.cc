/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#include <assert.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <algorithm>
#include <ctype.h>
#include <cerrno>
#include "fen.h"
#include "config.h"
#include "utility.h"


using namespace std;

namespace cnchess {

	bool is_piece(char piece)
	{
		char piece_upper = toupper(piece);
		ChessPiece::Type res = (ChessPiece::Type)piece_upper;
		return(res == ChessPiece::KING) ||
			(res == ChessPiece::ADVISOR) ||
			(res == ChessPiece::BISHOP) ||
			(res == ChessPiece::KNIGHT) ||
			(res == ChessPiece::ROOK) ||
			(res == ChessPiece::CANNON) ||
			(res == ChessPiece::PAWN) ||
			(res == ChessPiece::NONE);
	}

	ChessPiece::Type char2piece(char piece)
	{
		char piece_upper = toupper(piece);
		if (is_piece(piece_upper)) {
			return(ChessPiece::Type)piece_upper;
		} else {
			return ChessPiece::NONE;
		}
	}

	wchar_t piece2chiness(char piece, GameRole person)
	{
		switch (char2piece(piece)) {
		case ChessPiece::KING:
			return(person == RL_RED) ? L'帅' : L'将';
		case ChessPiece::ADVISOR:
			return(person == RL_RED) ? L'仕' : L'士';
		case ChessPiece::BISHOP:
			return(person == RL_RED) ? L'相' : L'象';
		case ChessPiece::KNIGHT:
			return L'马';
		case ChessPiece::ROOK:
			return L'车';
		case ChessPiece::CANNON:
			return L'炮';
		case ChessPiece::PAWN:
			return(person == RL_RED) ? L'兵' : L'卒';
		case ChessPiece::NONE:
			return L'\0';
		default:
			return L'\0';
		}
	}

	ChessBook* fen_parse(ChessBook& cb, const std::string& str)
	{
		string fen;
		trim(fen, str);

		size_t found = fen.find(" ");
		if (found != string::npos) {
			fen = fen.substr(0, found);
		}

		rtrim(fen, fen);
		StringList sl;
		split(sl, fen, '/');

		if (sl.size() != CHESSBOOK_ROW) {
			return NULL;
			//throw invalid_argument(format("fen string invalid '%s'", str.c_str()));
		}
		
		for (int i = 0; i < sl.size(); i++) {
			string row;
			trim(row, sl[i]);
			if (row.size() == 0) {
				return NULL;
				//throw invalid_argument(format("fen string invalid '%s'", str.c_str()));
			}
			int pos_in_row = 0;
			for (int j = 0; j < row.length(); j++) {
				char piece_char = row[j];
				if (isdigit(piece_char)) {
					int none_piece_num = ctoi(piece_char);
					if ((none_piece_num == 0) || (none_piece_num > CHESSBOOK_COL)) {
						return NULL;
						//throw invalid_argument(format("fen string invalid '%s'", str.c_str()));
					} else {
						for (int j1 = 0; j1 < none_piece_num; j1++) {
							if (pos_in_row < CHESSBOOK_COL) {
								cb[i][pos_in_row].setType(ChessPiece::NONE);
								pos_in_row++;
							} else {
								break;
							}
						}
						if (pos_in_row == CHESSBOOK_COL) {
							break;
						}
					}
				} else {
					if (!is_piece(piece_char)) {
						return NULL;
						//throw invalid_argument(format("fen string invalid '%s'", str.c_str()));
					}
					if (pos_in_row < CHESSBOOK_COL) {
						cb[i][pos_in_row].setOwner(isupper(piece_char) ? RL_RED : RL_BLACK);
						cb[i][pos_in_row].setType(char2piece(piece_char));
						pos_in_row++;
					} else {
						break;
					}
				}
			}
		}
		return &cb;
	}

	ChessBook* fen_parse(ChessBook& cb, FILE* fp)
	{
		assert(fp);

		string fen;
		while (!feof(fp)) {
			char buf;
			size_t __attribute__((unused)) rlen = fread(&buf, 1, 1, fp);
			if (ferror(fp)) {
				throw runtime_error(format("read stream error: %s", strerror(errno)));
			}
			fen.append(1, buf);
		}
		return fen_parse(cb, fen);
	}

	ChessBook* fen_parse(ChessBook& cb, int fd)
	{
		FILE* fp = fdopen(fd, "r");
		if (!fp) {
			throw invalid_argument(format("open file(fd:%d) fail: %s", fd, strerror(errno)));
		}
		try {
			return fen_parse(cb, fp);
		} catch (...) {
			fclose(fp);
			throw;
		}
	}

	ChessBook* fen_parse(ChessBook& cb, std::istream& stream)
	{
		string fen;
		while (!stream.eof()) {
			char buf;
			stream.read(&buf, 1);
			if (stream.bad()) {
				throw runtime_error("read stream error");
			}
			fen.append(1, buf);
		}
		return fen_parse(cb, fen);
	}

	ChessBook* fen_parse(const std::string& fen)
	{
		ChessBook *res = new ChessBook;
		return fen_parse(*res, fen);
	}

	ChessBook* fen_parse(FILE* fp)
	{
		ChessBook *res = new ChessBook;
		return fen_parse(*res, fp);
	}

	ChessBook* fen_parse(int fd)
	{
		ChessBook *res = new ChessBook;
		return fen_parse(*res, fd);
	}

	ChessBook* fen_parse(std::istream& stream)
	{
		ChessBook *res = new ChessBook;
		return fen_parse(*res, stream);
	}

	std::string& fen_tostring(std::string& chessbook_str, const ChessBook& cb)
	{
		chessbook_str.clear();
		for (int i = 0; i < CHESSBOOK_ROW; i ++) {
			for (int j = 0; j < CHESSBOOK_COL; j++) {
				if (!cb[i][j].isType(ChessPiece::NONE)) {
					if (cb[i][j].getOwner() == RL_RED) {
						chessbook_str.append(1, (char) toupper(cb[i][j].getType()));
					} else if (cb[i][j].getOwner() == RL_BLACK) {
						chessbook_str.append(1, (char) tolower(cb[i][j].getType()));
					}
				} else {
					int empty_piece_num = 1;
					for (int j1 = j + 1; j1 < CHESSBOOK_COL; j1++) {
						if (cb[i][j1].isType(ChessPiece::NONE)) {
							empty_piece_num++;
							j++;
						} else {
							break;
						}
					}
					chessbook_str.append(asstring(empty_piece_num));
				}
			}
			if (i != CHESSBOOK_ROW - 1) {
				chessbook_str.append("/");
			}
		}
		return chessbook_str;
	}

	std::string fen_tostring(const ChessBook& cb)
	{
		std::string result;
		return fen_tostring(result, cb);
	}


}
