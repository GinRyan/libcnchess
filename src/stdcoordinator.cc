/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#include <assert.h>
#include "gamemanager.h"
#include "game.h"
#include "utility.h"
#include "stdcoordinator.h"
#include "stdrule.h"


#define playerrole_tostring(player) ((player)->getMyRole() == RL_RED ? "red" : "black")

using namespace std;

namespace cnchess {

	StdCoordinator::StdCoordinator(Game* game, GameTimer* gtimer_red, GameTimer* gtimer_black)
	: Coordinator(game, gtimer_red, gtimer_black)
	{
		referee = game->getReferee();
		referee->incref();
		dummy_timer = new DummyActionTimer;
	}

	StdCoordinator::~StdCoordinator()
	{
		delete dummy_timer;
		referee->decref();
		for (int i = 0; i < action_history.size(); i++) {
			if ((action_history[i].action.action == AC_MOVED) &&
				action_history[i].action.data) {
				delete (ICCSMotion*) (action_history[i].action.data);
			}
		}
	}

	Coordinator::Result StdCoordinator::move(const GamePlayer* player,
		const ICCSMotion& motion)
	{
		assert(player);

		if (isOver()) {
			return INVALID;
		}

		Result res = REFUSE;
		ICCSMotion* mymotion = new ICCSMotion;
		*mymotion = motion;
			
		GameAction ac = {
			AC_MOVED,
			this,
			player,
			mymotion
		};
		ChessBook cb;
		if (!referee->requestMove(player, *mymotion)) {
			pdebug("motion %s not allowed", iccsmotion_tostring(*mymotion).c_str());
			res = REFUSE;
			requestFinished(player, ac, res);
			
			return res;
		} else {
			cb = getGame()->getBoard();
			res = AGREE;
			requestFinished(player, ac, res);
		}
				
		/*
		 * 根据用户的移动棋子请求模拟棋盘, 如果用户当前这步棋吃掉了对方的 KING, 那么认为对方 GiveUp
		 * 并代替对方玩家发出 GiveUp 请求
		 */
		GamePlayer* adversary = getGame()->getAdversary(player);
		if (iccsmotion_apply(cb, *mymotion)) {
			PieceMotion posmotion;
			mymotion->start.toPiecePos(posmotion.start);
			mymotion->end.toPiecePos(posmotion.end);
			GameRole rl = stdrule_whowin(cb, posmotion);
			if (player->getMyRole() == rl) {
				pdebug("%s player losed", playerrole_tostring(adversary));
				return requestGiveUp(adversary, GURSON_LOSED);
			} else if (adversary->getMyRole() == rl) {
				pdebug("%s player losed", playerrole_tostring(player));
				return requestGiveUp(player, GURSON_LOSED);
			}
		} else {
			pdebug("motion %s invalid", iccsmotion_tostring(*mymotion).c_str());
			return INVALID;
		}
		
		return res;
	}

	Coordinator::Result StdCoordinator::requestMove(const GamePlayer* player)
	{
		assert(player);

		if (isOver()) {
			return INVALID;
		}

		GamePlayer* adversary = getGame()->getAdversary(player);
		GameAction ac = {
			AC_REQ_MOVE,
			this,
			player,
			NULL
		};
		
		pdebug("%s player request %s player to move", playerrole_tostring(player), playerrole_tostring(adversary));
		
		IActionTimer* gptimer = createTimerForPlayer(adversary);
		Result res = (adversary->requestHandle(ac, *this, *gptimer) == GamePlayer::HANDLED) ? AGREE : REFUSE;
		if (gptimer->isExpired()) {
			res = Result::EXPIRED;
		}

		delete gptimer;

		requestFinished(player, ac, res);

		return res;
	}

	Coordinator::Result StdCoordinator::requestDraw(const GamePlayer* player)
	{
		assert(player);

		if (isOver()) {
			return INVALID;
		}

		GamePlayer* adversary = getGame()->getAdversary(player);
		GameAction ac = {
			AC_REQ_DRAW,
			this,
			player,
			NULL
		};
		Result res = REFUSE;
		if (referee->requestDraw(player)) {
			IActionTimer* gptimer = createTimerForPlayer(adversary);
			res = (adversary->requestHandle(ac, *this, *gptimer) == GamePlayer::HANDLED) ? AGREE : REFUSE;
			if (gptimer->isExpired()) {
				res = Result::EXPIRED;
			}
			delete gptimer;
		} else {
			res = REFUSE;
		}

		requestFinished(player, ac, res);

		return res;
	}

	Coordinator::Result StdCoordinator::requestRetract(const GamePlayer* player)
	{
		assert(player);

		if (isOver()) {
			return INVALID;
		}

		GamePlayer* adversary = getGame()->getAdversary(player);
		GameAction ac = {
			AC_REQ_RETRACT,
			this,
			player,
			NULL
		};
		Result res = REFUSE;
		if (referee->requestRetract(player)) {
			IActionTimer* gptimer = createTimerForPlayer(adversary);
			res = (adversary->requestHandle(ac, *this, *gptimer) == GamePlayer::HANDLED) ? AGREE : REFUSE;
			if (gptimer->isExpired()) {
				res = Result::EXPIRED;
			}
			delete gptimer;
		} else {
			res = REFUSE;
		}

		requestFinished(player, ac, res);

		return res;
	}

	Coordinator::Result StdCoordinator::requestGiveUp(const GamePlayer* player, GiveUpReason reason)
	{
		assert(player);

		if (isOver()) {
			return INVALID;
		}

		GamePlayer* adversary = getGame()->getAdversary(player);
		GameAction ac = {
			AC_REQ_GIVEUP,
			this,
			player,
			&reason
		};
		
		pdebug("%s player requestGiveUp", playerrole_tostring(player));
		
		adversary->requestHandle(ac, *this, *dummy_timer);

		requestFinished(player, ac, AGREE);

		return AGREE;
	}

	bool StdCoordinator::isOver() const
	{
		if (getGame()->getStatus() != Game::PLAYING) {
			return true;
		}

		/* 如果已经请求过认输或者已经和棋, 则不再处理请求 */
		for (int i = 0; i < action_history.size(); i++) {
			if (((action_history[i].action.action == AC_REQ_GIVEUP) ||
				(action_history[i].action.action == AC_REQ_DRAW)) && (action_history[i].res == AGREE)) {
				return true;
			}
		}
		
		return false;
	}

	void StdCoordinator::requestFinished(const GamePlayer* player, const GameAction& ac, Result res)
	{
		ActionEntry ac_entry = {
			ac,
			res
		};

		mutex.lock();
		action_history.push_back(ac_entry);
		mutex.unlock();

		fireActionEvent(player, ac, res);
	}

	IActionTimer* StdCoordinator::createTimerForPlayer(const GamePlayer* player, bool autostart)
	{
		IActionTimer* gptimer = NULL;
		GameTimer* gtimer = getTimer(getGame()->getRole(player));
		if (gtimer) {
			ActionTimer* gptimer_tmp = new ActionTimer(gtimer);
			if (autostart) {
				gptimer_tmp->start();
			}
			gptimer = gptimer_tmp;
		} else {
			gptimer = new DummyActionTimer;
		}
		return gptimer;
	}


}
