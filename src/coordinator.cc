#include "coordinator.h"
#include "gametimer.h"
#include "utility.h"

namespace cnchess {

	Coordinator::Coordinator(Game* game, GameTimer* gtimer_red, GameTimer* gtimer_black)
	: game(game), gtimer_red(gtimer_red), gtimer_black(gtimer_black)
	{
		assert(game);

		game->incref();
		if (gtimer_red && gtimer_black) {
			gtimer_red->incref();
			gtimer_black->incref();
		}
	}

	Coordinator::~Coordinator()
	{
		game->decref();
		if (gtimer_red && gtimer_black) {
			gtimer_red->decref();
			gtimer_black->decref();
		}
	}

	GameTimer* Coordinator::getTimer(GameRole role)
	{
		if (role == RL_RED) {
			return gtimer_red;
		} else if (role == RL_BLACK) {
			return gtimer_black;
		}
		return NULL;
	}

	const GameTimer* Coordinator::getTimer(GameRole role) const
	{
		if (role == RL_RED) {
			return gtimer_red;
		} else if (role == RL_BLACK) {
			return gtimer_black;
		}
		return NULL;
	}
	
	Coordinator::Result Coordinator::move(const GamePlayer* player, const std::string& strmotion)
	{
		ICCSMotion motion;
		if (!iccsmotion_parse(motion, strmotion)) {
			return INVALID;
		}
		return move(player, motion);
	}
	
	Coordinator::Result Coordinator::move(const GamePlayer* player, const PieceMotion& pcemotion)
	{
		ICCSMotion motion;
		return move(player, *iccsmotion_parse(motion, pcemotion));
	}

}