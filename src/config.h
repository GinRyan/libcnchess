/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_CONFIG_H_
#define CNCS_CONFIG_H_ 1

#include <signal.h>

#define CNCS_AUTHOR		"alecH"
#define CNCS_EMAIL		"huanghao@toltech.cn"
#define CNCS_COPYRIGHT	"Copyright by toltech.cn 2011, All Right Reserved."

#if !defined(CNCS_VERSION)
#define CNCS_VERSION	"0.0.1"
#endif

#define CNCS_REALCLOCK_SIGMIN	SIGRTMIN
#define CNCS_REALCLOCK_SIGMAX	SIGRTMAX

#define CNCS_MOVE_MAX	5000

#endif
