/* 
 * File:   gamerefereebasic.h
 * Author: alec
 *
 * Created on 2012年5月24日, 上午11:38
 */

#include "stdreferee.h"
#include "utility.h"
#include "helper.h"
#include "gameplayer.h"
#include "stdrule.h"


using namespace std;

namespace cnchess {

	bool STDReferee::requestMove(const GamePlayer* player, const ICCSMotion& mymotion)
	{
		if (!iccsmotion_isvalid(mymotion)) {
			pdebug("%s invalid", iccsmotion_tostring(mymotion).c_str());
			return false;
		}
		PiecePos start_pos;
		PiecePos end_pos;
		mymotion.start.toPiecePos(start_pos);
		mymotion.end.toPiecePos(end_pos);
		const ChessPiece& start_piece = this->getGame()->getBoard().get(start_pos);
		if (start_piece.isType(ChessPiece::NONE)) {
			pdebug("%s invalid, start piece is NONE", iccsmotion_tostring(mymotion).c_str());
			return false;
		} else if (start_piece.getOwner() != player->getMyRole()) {
			// 不能移动对手的棋子
			pdebug("%s invalid, can't move piece of the adversary", iccsmotion_tostring(mymotion).c_str());
			return false;
		}

		PathList pathlist;
		if (stdrule_getpath(pathlist, this->getGame()->getBoard(), start_piece)) {
			for (int i = 0; i < pathlist.size(); i++) {
				if (pathlist[i].start.equal(start_pos) && pathlist[i].end.equal(end_pos)) {
					return true;
				}
			}
		}
		
		pdebug("%s not in path list", iccsmotion_tostring(mymotion).c_str());
		
		return false;
	}

}
