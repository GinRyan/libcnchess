/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#include <assert.h>
#include "gamemanager.h"
#include "game.h"
#include "utility.h"
#include "stdcoordinator.h"
#include "config.h"
#include "stdrule.h"

#define playerrole_tostring(player) ((player)->getMyRole() == RL_RED ? "red" : "black")


namespace cnchess {

	GameManager::GameManager(Game *game, TimerInfo* timeri, Coordinator* coordinator)
	: game(game), status(IDLE)
	{
		assert(game);

		referee = game->getReferee();

		if (timeri) {
			this->gtimer_red = new GameTimer(*timeri);
			this->gtimer_red->addListener(timerListener, this);
			this->gtimer_black = new GameTimer(*timeri);
			this->gtimer_black->addListener(timerListener, this);
			timeri->decref();
		} else {
			const TimerInfo* timeri = game->getTimer();
			if (timeri) {
				this->gtimer_red = new GameTimer(*timeri);
				this->gtimer_red->addListener(timerListener, this);
				this->gtimer_black = new GameTimer(*timeri);
				this->gtimer_black->addListener(timerListener, this);
			} else {
				this->gtimer_red = NULL;
				this->gtimer_black = NULL;
			}
		}
		exphandler_obj.ctx = NULL;
		exphandler_obj.handler = NULL;

		if (coordinator) {
			this->coordinator = coordinator;
		} else {
			this->coordinator = new StdCoordinator(game, this->gtimer_red, this->gtimer_black);
		}
		this->coordinator->addListener(actionListener, this);
	}

	GameManager::~GameManager()
	{
		coordinator->decref();
		game->decref();
		if (gtimer_red && gtimer_black) {
			gtimer_red->decref();
			gtimer_black->decref();
		}
	}

	Game::Status GameManager::start()
	{
		if (getStatus() == STARTED) {
			return game->getStatus();
		}

		game->getReferee()->onGameStart(game);
		game->getPlayer(RL_RED)->onGameStart(game, game->getRole(game->getPlayer(RL_RED)));
		game->getPlayer(RL_BLACK)->onGameStart(game, game->getRole(game->getPlayer(RL_BLACK)));
		game->setStatus(Game::PLAYING);

		pdebug("start game, %s player first", game->getCurrent()->getMyRole() == RL_RED ? "red" : "black");

		if (gtimer_red && gtimer_black) {
			gtimer_red->start();
			gtimer_black->start();

			pdebug("start timer");
		}
		exitFlag = false;
		while (game->getStatus() == Game::PLAYING ) {
			if(exitFlag == true){
				exitFlag = false;
				if (game->getStatus() == Game::Status::PLAYING) {
					setOver(Game::Status::PAUSED);
				} else {
					setOver(game->getStatus());
				}
			}
			GamePlayer* curr_player = game->getCurrent();

			pdebug("request %s player to move", playerrole_tostring(game->getCurrent()));

			Coordinator::Result res = coordinator->requestMove(game->getNext());

			switch (res) {
			case Coordinator::AGREE:
				game->setNext(curr_player);
				break;
			case Coordinator::REFUSE:

				pdebug("%s player refuse for move", playerrole_tostring(game->getCurrent()));

				Game::Status gamest;
				if (game->getRole(curr_player) == RL_RED) {
					gamest = Game::WBLACK;
				} else {
					gamest = Game::WRED;
				}
				game->setStatus(gamest);
				break;
			case Coordinator::EXPIRED:
				if (!handleExpired(curr_player)) {
					Game::Status gamest;
					if (game->getRole(curr_player) == RL_RED) {
						gamest = Game::WBLACK;
					} else {
						gamest = Game::WRED;
					}
					game->setStatus(gamest);
				} else {
					game->setNext(game->getAdversary(curr_player));
				}
				break;
			case Coordinator::INVALID:

				pdebug("request %s player invalid", playerrole_tostring(game->getCurrent()));

				if (game->getRole(curr_player) == RL_RED) {
					gamest = Game::WBLACK;
				} else {
					gamest = Game::WRED;
				}
				game->setStatus(gamest);
				break;
			}
		}


		Game::Status gamest = game->getStatus();
		setOver(gamest);

		if (game->getHistory().size()) {
			ICCSMotion motion = game->getHistory()[game->getHistory().size() - 1].motion;
			PieceMotion piecemotion;
			motion.start.toPiecePos(piecemotion.start);
			motion.end.toPiecePos(piecemotion.end);
			GameRole rl = stdrule_whowin(game->getBoard(), piecemotion);
			if (RL_RED == rl) {
				return Game::WRED;
			} else if (RL_BLACK == rl) {
				return Game::WBLACK;
			} else {
				return Game::PAUSED;
			}
		} else {
			return Game::PAUSED;
		}

		// @FIXME: �����߼������⣬ �¾�һ��û�ߵ�����£� ���ﷵ�ص��� black win���������޸��ɣ�������
		// return gamest;
	}

	void GameManager::stop()
	{
		if (getStatus() == STOPPED) {
			return;
		}
		exitFlag = true;
	}

	void GameManager::setOver(Game::Status gamest)
	{
		if (status == STOPPED) {
			return;
		}

		if (gtimer_red && gtimer_black) {
			gtimer_red->press();
			gtimer_black->press();
		}
		game->getPlayer(RL_RED)->onGameOver(gamest);
		game->getPlayer(RL_BLACK)->onGameOver(gamest);
		game->getReferee()->onGameOver(gamest);

		setStatus(STOPPED);
	}

	bool GameManager::handleExpired(const GamePlayer* player)
	{
		GameTimer::ExpiredType exptype = GameTimer::EXPIRED_STEP;
		GameRole role = game->getRole(player);
		GameTimer* gtimer = NULL;
		if (role == RL_RED) {
			gtimer = gtimer_red;
		} else if (role == RL_BLACK) {
			gtimer = gtimer_black;
		}
		if (gtimer) {
			if (gtimer->getStatus() == EXPIRED) {
				exptype = GameTimer::EXPIRED_TIMER;
			}
		}
		if (exphandler_obj.handler) {
			OvertimeHandle handle;
			exphandler_obj.handler(handle, exphandler_obj.ctx, game, player, exptype);
			if ((handle.handle == OvertimeHandle::OVERTIME) && (handle.overtime > 0)) {
				if (gtimer) {
					gtimer->overtime(handle.overtime);
					return true;
				}
			}
		}

		return false;
	}

	void GameManager::actionListener(const Coordinator* coordinator, void* ctx,
			const GamePlayer* player, const GameAction& ac, Coordinator::Result res)
	{
		assert(player && ctx);

		GameManager* self = (GameManager*) ctx;

		if (self->game->getStatus() != Game::PLAYING) {
			return;
		}
		switch (ac.action) {
		case GameActionType::AC_MOVED:
			if (res == Coordinator::AGREE && ac.data) {
				ICCSMotion& mymotion = *(ICCSMotion*) ac.data;

				if (iccsmotion_apply(self->game->getBoard(), mymotion)) {

					pdebug("apply %s for %s player and add to game history", iccsmotion_tostring(mymotion).c_str(), playerrole_tostring(player));

					PlayHistory history_entry(mymotion, player);
					self->mutex.lock();
					self->game->getHistory().push_back(history_entry);
					self->mutex.unlock();
				}
			}
			break;
		case GameActionType::AC_REQ_RETRACT:
			if (res == Coordinator::AGREE) {
				size_t history_size = self->game->getHistory().size();
				if (!history_size) {
					break;
				}
				if (self->game->getHistory()[history_size - 1].player == player) {
					self->mutex.lock();
					self->game->getHistory().pop_back();
					self->mutex.unlock();
				} else {
					if (history_size < 2) {
						break;
					}
					self->mutex.lock();
					if (!self->game->getHistory().empty()) {
						self->game->getHistory().pop_back();
					}
					if (!self->game->getHistory().empty()) {
						self->game->getHistory().pop_back();
					}
					self->mutex.unlock();
				}
				
				ChessBook cb = self->game->getChessBook();
				for (int i = 0; i < self->game->getHistory().size(); i++) {
					iccsmotion_apply(cb, self->game->getHistory()[i].motion);
				}
				self->game->getBoard() = cb;
			}
		}
	}

	void GameManager::timerListener(const GameTimer* timer, void* ctx, GameTimer::ExpiredType exptype)
	{
		pdebug("timer expired, expire type is %d", exptype);

		GameManager* self = (GameManager*) ctx;
		if (self->getGame()->getStatus() == Game::PLAYING) {
			const GamePlayer* player = self->getGame()->getCurrent();
			if (!self->handleExpired(player)) {
				Game::Status gamest;
				if (self->getGame()->getRole(player) == RL_RED) {
					gamest = Game::WBLACK;
				} else {
					gamest = Game::WRED;
				}
				self->setOver(gamest);
			}
		}
	}

	inline void GameManager::setExpiredHandler(ExpiredHandler handler, void* ctx)
	{
		assert(handler);

		exphandler_obj.handler = handler;
		exphandler_obj.ctx = ctx;
	}

}
