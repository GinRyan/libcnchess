/* 
 * File:   alarm.h
 * Author: alec
 *
 * Created on 2012年5月18日, 下午12:37
 */

#include <assert.h>
#include <stdexcept>
#include "alarm.h"
#include "utility.h"


using namespace std;

namespace cnchess {

	Alarm::Alarm(IClock* clock, long expire_time, int once)
	: clock(clock), status(IDLE), expiretime(expire_time), remaintime(0),
	once(once)
	{
		assert(clock && expire_time > 0);
		long millisec = ts2millisec(clock->getInterval());
		if (millisec > expire_time) {
			throw invalid_argument("`expire_time` less than clock's interval value");
		}
		clock->incref();
	}

	Alarm::~Alarm()
	{
		stop();
		clock->decref();
	}

	void Alarm::start()
	{
		if (status == STARTED) {
			return;
		}
		remaintime = expiretime;
		clock->addListener(clockListener, this);
		setStatus(STARTED);
	}

	void Alarm::stop()
	{
		if (status == STOPPED || status == IDLE) {
			return;
		}
		clock->removeListener(clockListener);
		remaintime = 0;
		setStatus(STOPPED);
	}

	void Alarm::clockListener(const IClock* clock, void* ctx)
	{
		Alarm* self = (Alarm*) ctx;
		const struct timespec& ts = clock->getInterval();
		long millisec = ts2millisec(ts);
		self->remaintime -= millisec;
		if (self->remaintime <= 0) {
			if (self->once == ONCE) {
				self->stop();
			} else {
				self->remaintime = self->expiretime;
			}
			self->dispatcher.fire(self);
		}
	}

}