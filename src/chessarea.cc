#include "chessarea.h"
#include "define.h"
#include "chessrow.h"
#include "chessbook.h"

using namespace std;

namespace cnchess {

	ChessArea::ChessArea()
	{
		rtop.setX(ChessRow::size() - 1);
		lbtm.setY(ChessBook::size() - 1);
		rbtm.setX(ChessRow::size() - 1);
		rbtm.setY(ChessBook::size() - 1);
	}

	ChessArea* chessarea_mkred(ChessArea& area)
	{
		area.ltop.setX(0);
		area.ltop.setY(5);
		area.rtop.setX(ChessRow::size() - 1);
		area.rtop.setY(5);

		area.lbtm.setX(0);
		area.lbtm.setY(ChessBook::size() - 1);
		area.rbtm.setX(ChessRow::size() - 1);
		area.rbtm.setY(ChessBook::size() - 1);

		return &area;
	}

	ChessArea* chessarea_mkblack(ChessArea& area)
	{
		area.ltop.setX(0);
		area.ltop.setY(0);
		area.rtop.setX(ChessRow::size() - 1);
		area.rtop.setY(0);

		area.lbtm.setX(0);
		area.lbtm.setY(4);
		area.rbtm.setX(ChessRow::size() - 1);
		area.rbtm.setY(4);

		return &area;
	}

	ChessArea* chessarea_mkking(ChessArea& area, int red_or_black)
	{
		if (red_or_black == RED) {
			area.ltop.setX(3);
			area.ltop.setY(7);
			area.rtop.setX(5);
			area.rtop.setY(7);

			area.lbtm.setX(3);
			area.lbtm.setY(ChessBook::size() - 1);
			area.rbtm.setX(5);
			area.rbtm.setY(ChessBook::size() - 1);
		} else {
			area.ltop.setX(3);
			area.ltop.setY(0);
			area.rtop.setX(5);
			area.rtop.setY(0);

			area.lbtm.setX(3);
			area.lbtm.setY(2);
			area.rbtm.setX(5);
			area.rbtm.setY(2);
		}

		return &area;
	}

	bool chessarea_contains(const ChessArea& area, const PiecePos& pos)
	{
		if ((pos.getX() < area.ltop.getX()) || (pos.getY() < area.ltop.getY())) {
			return false;
		}
		if ((pos.getX() > area.rtop.getX()) || (pos.getY() < area.rtop.getY())) {
			return false;
		}
		if ((pos.getX() < area.lbtm.getX()) || (pos.getY() > area.lbtm.getY())) {
			return false;
		}
		if ((pos.getX() > area.rbtm.getX()) || (pos.getY() > area.rbtm.getY())) {
			return false;
		}
		return true;
	}

	bool chessarea_contains(const ChessArea& area, int x, int y)
	{
		PiecePos pos(x, y);
		return chessarea_contains(area, pos);
	}

	PiecePosRange* chessarea_torange(const ChessArea& area, PiecePosRange& range)
	{
		for (int i = area.ltop.getY(); i <= area.lbtm.getY(); i++) {
			for (int j = area.ltop.getX(); j <= area.rtop.getX(); j++) {
				range.push_back(PiecePos(j, i));
			}
		}
		return &range;
	}

}
