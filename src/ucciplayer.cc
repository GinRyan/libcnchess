#include <stdexcept>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include "ucciplayer.h"
#include "utility.h"
#include "fen.h"
#include "stdrule.h"
#include "ucciutil.h"

using namespace std;

namespace cnchess {

	UCCIPlayer::UCCIPlayer(UCCIEngine* uccieg, Degree degree)
	: uccieg(uccieg), round(0), degree(degree)
	{
		assert(uccieg);

		switch (degree) {
		case EASY:
			depth = 5;
			break;
		case MIDDLE:
			depth = 5;
			break;
		case HARD:
			depth = 6;
			break;
		}
	}

	UCCIPlayer::~UCCIPlayer()
	{
		uccieg->stop();
		uccieg->decref();
	}

	void UCCIPlayer::onGameStart(const Game* game, GameRole myrole)
	{
		GamePlayer::onGameStart(game, myrole);
		if (!uccieg->start()) {
			throw runtime_error(format("can't start UCCI engine(%s)", this->uccieg->getEngine().c_str()));
		}

		switch (degree) {
		case EASY:
			uccieg->sendRequest("setoption usebook false");
			uccieg->sendRequest("setoption randomness huge");
			uccieg->sendRequest("setoption style risky");
			uccieg->sendRequest("setoption knowledge small");
			break;
		case MIDDLE:
			uccieg->sendRequest("setoption usebook false");
			uccieg->sendRequest("setoption randomness medium");
			uccieg->sendRequest("setoption style risky");
			uccieg->sendRequest("setoption knowledge small");
			break;
		case HARD:
			uccieg->sendRequest("setoption usebook true");
			uccieg->sendRequest("setoption randomness none");
			uccieg->sendRequest("setoption style normal");
			uccieg->sendRequest("setoption knowledge large");
			break;
		}
	}

	void UCCIPlayer::onGameOver(Game::Status status)
	{
		GamePlayer::onGameOver(status);
		uccieg->stop();
	}

	int UCCIPlayer::handleMoveRequest(const GameAction& ac, Coordinator& coordinator,
			IActionTimer& timer)
	{
		timer.start();

		StringList move_history;
		const Game::History& history = this->getGame()->getHistory();
		for (size_t i = 0; i < history.size(); i++) {
			string iccsstr;
			move_history.push_back(aslower(*iccsmotion_tostring(iccsstr, history[i].motion)));
		}
		string position_str = ucci_reqbuild_postion(getGame()->getChessBook(), getMyRole(), &move_history);
		
		if (!uccieg->sendRequest(position_str)) {
			return ERROR;
		}
		
		// 这里要检查 AI 是否有长手， 如果有长手， 必须要禁手
		const ICCSMotion* banmove = stdrule_findbanmove(getGame()->getHistory(), this->getMyRole());
		if (banmove) {
			uccieg->sendRequest(format("banmoves %s", aslower(iccsmotion_tostring(*banmove)).c_str()));
		}

		string go_str = "go";
		if (timer.getStatus() != IDLE) {
			go_str += " time " + asstring(timer.remainTime()) + " increment 0";
		} else if (depth > 0) {
			go_str += " depth " + asstring(depth);
		} else {
			go_str += " depth 5";
		}

		if (!uccieg->sendRequest(go_str)) {
			return ERROR;
		}

		bool has_bestmove = false;
		string resp;
		while (true) {
			if (!uccieg->getResponse(resp)) {
				return ERROR;
			}
			if (resp.substr(0, strlen("bestmove")) == "bestmove") {
				has_bestmove = true;
				break;
			} else if (resp.substr(0, strlen("nobestmove")) == "nobestmove") {
				has_bestmove = false;
				break;
			}
		}

		string eg_bestmove;

		ICCSMotion iccsmotion1;
		PieceMotion piecemotion1;
		if (stdrule_laststep(&piecemotion1, getGame()->getBoard(), getMyRole())) {
			// @FIXME: 因为eleeye引擎在可以一步杀死对方帅的时候， 总是颠倒自己身份，走出一步错棋， 
			// 所以这里先判断引擎是否可以一步将死对方
			iccsmotion_parse(iccsmotion1, piecemotion1);
			iccsmotion_tostring(eg_bestmove, iccsmotion1);
		} else {

			if (has_bestmove) {
				StringList resp_parts;
				split(resp_parts, resp);
				eg_bestmove = resp_parts[1];
				string eg_action = resp_parts[resp_parts.size() - 1];
				if (eg_action == "resign") {
					// 引擎认输
					coordinator.requestGiveUp(this, GURSON_LOSED);
					return IGNORED;
				} else if (eg_action == "draw") {
					// 引擎请求和棋
					if (Coordinator::AGREE == coordinator.requestDraw(this)) {
						return IGNORED;
					} else {
						// 对手如果不同意和棋， 引擎认输
						//coordinator.requestGiveUp(this);
						//return IGNORED;
					}
				}
			} else {
				// 引擎没有更好的着法推荐
				pdebug("engine no bestmove");
				
				if (stdrule_laststep(NULL, getGame()->getBoard(), getGame()->getAdversary(this)->getMyRole())) {
					// 如果对方可以一步将死引擎， 引擎就认输
					coordinator.requestGiveUp(this, GURSON_LOSED);
					return IGNORED;
				} else {
					coordinator.requestGiveUp(this, GURSON_GIVEUP);
					return IGNORED;
				}
				
#if 0
				ICCSMotion iccsmotion;
				PieceMotion piecemotion;

				if (stdrule_laststep(NULL, getGame()->getBoard(), getGame()->getAdversary(this)->getMyRole())) {
					// 如果对方可以一步将死引擎， 引擎就认输
					//coordinator.requestGiveUp(this);
					//return IGNORED;
					
					// 没有别的棋子好走了， 只好尝试移动一下自己的帅
					PiecePosList pos;
					const ChessBook& curr_cb = getGame()->getBoard();
					// 找出自己 “帅“ 的位置
					if (curr_cb.find(pos, ChessPiece::KING, getMyRole()) != 1) {
						return ERROR;
					}
					PathList king_pathlist;
					if (!stdrule_getpath(king_pathlist, curr_cb, curr_cb.get(pos[0])) || (king_pathlist.size() == 0)) {
						// 引擎连 “帅“ 动不了了， 认输
						coordinator.requestGiveUp(this);
						return IGNORED;
					}
					// 随机走一步“帅“
					iccsmotion_parse(iccsmotion, king_pathlist[0]);
				} else {		
					ExcludePiece exclude_king;
					exclude_king.push_back(ChessPiece::KING);
					// 随机走除了帅之外的一步
					if (stdrule_findstep(piecemotion, getGame()->getBoard(), this->getMyRole(), &exclude_king)) {
						iccsmotion_parse(iccsmotion, piecemotion);
					} else {
						// 没有别的棋子好走了， 只好尝试移动一下自己的帅
						PiecePosList pos;
						const ChessBook& curr_cb = getGame()->getBoard();
						// 找出自己 “帅“ 的位置
						if (curr_cb.find(pos, ChessPiece::KING, getMyRole()) != 1) {
							return ERROR;
						}
						PathList king_pathlist;
						if (!stdrule_getpath(king_pathlist, curr_cb, curr_cb.get(pos[0])) || (king_pathlist.size() == 0)) {
							// 引擎连 “帅“ 动不了了， 认输
							coordinator.requestGiveUp(this);
							return IGNORED;
						}
						// 随机走一步“帅“
						iccsmotion_parse(iccsmotion, king_pathlist[0]);
					}
				}

				iccsmotion_tostring(eg_bestmove, iccsmotion);
#endif
			}
		}
		if (Coordinator::AGREE == coordinator.move(this, eg_bestmove)) {
			round++;
			return HANDLED;
		} else {
			pdebug("can't move to %s", eg_bestmove.c_str());
			return ERROR;
		}
	}

	int UCCIPlayer::requestHandle(const GameAction& ac, Coordinator& coordinator,
			IActionTimer& timer)
	{
		if (ac.action == GameActionType::AC_REQ_MOVE) {
			return handleMoveRequest(ac, coordinator, timer);
		} else if (ac.action == GameActionType::AC_REQ_GIVEUP) {
			pdebug("adversary give-up");
			return HANDLED;
		} else if (ac.action == GameActionType::AC_REQ_RETRACT) {
			pdebug("adversary request retract, accepted");
			return HANDLED;
		} else {
			pdebug("not supported action %d", ac.action);
			return ERROR;
		}
	}



}
