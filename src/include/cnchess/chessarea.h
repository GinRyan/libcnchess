/* 
 * File:   chessarea.h
 * Author: alec
 *
 * Created on 2012年6月12日, 上午10:05
 */

#ifndef CNCS_CHESSAREA_H
#define	CNCS_CHESSAREA_H

#include "piecepos.h"

namespace cnchess {

	/*! \addtogroup chessbook
	 *  @{
	 */

	/**
	 * \class ChessArea
	 * 
	 * \brief 代表棋盘上的一个区域, 一个区域由 4 个 (x,y) 定义, 区域应该是一个规则矩形
	 */
	struct ChessArea {
		/**
		 * \brief 左上角坐标
		 */
		PiecePos ltop;

		/**
		 * \brief 右上角坐标
		 */
		PiecePos rtop;

		/**
		 * \brief 左下角坐标
		 */
		PiecePos lbtm;

		/**
		 * \brief 右下角坐标
		 */
		PiecePos rbtm;

		/**
		 * 初始化为整个棋盘区域
		 */
		ChessArea();
	};

	/**
	 * 生成黑方区域坐标
	 * 
	 * @param area
	 * @return 
	 */
	ChessArea* chessarea_mkblack(ChessArea& area);

	/**
	 * 生成红方区域坐标
	 * 
	 * @param area
	 * @return 
	 */
	ChessArea* chessarea_mkred(ChessArea& area);

	/**
	 * 皇宫区域坐标
	 * 
	 * @param area
	 * @param red_or_black	RED or BLACK
	 * @return 
	 */
	ChessArea* chessarea_mkking(ChessArea& area, int red_or_black);

	/**
	 * 检查一个坐标是否在区域内
	 * 
	 * @param area
	 * @param pos
	 * @return 
	 */
	bool chessarea_contains(const ChessArea& area, const PiecePos& pos);
	bool chessarea_contains(const ChessArea& area, int x, int y);

	/**
	 * 转换 ChessArea 为 PiecePosRange
	 * 
	 * @param range
	 * @param area
	 * @return 
	 */
	PiecePosRange* chessarea_torange(const ChessArea& area, PiecePosRange& range);

	/*! @} */

}

#endif	/* CNCS_CHESSAREA_H */

