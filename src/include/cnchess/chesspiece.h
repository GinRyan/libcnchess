/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_CHESSPIECE_H_
#define CNCS_CHESSPIECE_H_

#include "define.h"


namespace cnchess {

	/*! \addtogroup chessbook
	 *  @{
	 */

	/*! \class ChessPiece
	 *  
	 * \brief 代表棋盘上一个可放置棋子的点或者棋子
	 */
	class ChessPiece {
	public:

		/*! \brief 棋子类型 */
		enum Type {
			KING = 'K', /*!< 帅/将 */
			ADVISOR = 'A', /*!< 士 */
			BISHOP = 'B', /*!< 相/像 */
			KNIGHT = 'N', /*!< 马 */
			ROOK = 'R', /*!< 车 */
			CANNON = 'C', /*!< 炮 */
			PAWN = 'P', /*!< 兵/卒 */
			NONE = '\0', /*!< 点, 也就是没有棋子 */
		};

		/**
		 * \brief 创建一个棋盘点
		 * 
		 * @param owner 属于红方还是黑方, 注意， NONE 棋子的属主总是 RL_NONE
		 * @param piece	棋子
		 */
		explicit ChessPiece(GameRole owner = RL_NONE,
			Type piece = NONE);
		~ChessPiece();

		/**
		 * \brief 取得棋子类型
		 * 
		 * @return 
		 */
		const Type& getType() const;

		/**
		 * \brief 设置棋子类型
		 * @param piece
		 */
		void setType(Type piece);

		/**
		 * \brief 检查是否是某个特定的棋子类型
		 * 
		 * @param piece
		 * @return 
		 */
		bool isType(Type piece) const;

		/**
		 * \brief 取得所有方, 既是红方还是黑方所有, 或者哪方都不属于
		 * @return 
		 */
		GameRole getOwner() const;

		/**
		 * \brief 设置所有方, 既是红方还是黑方所有, 或者哪方都不属于
		 * @return 
		 */
		void setOwner(GameRole owner);
	private:
		Type piece_type;
		GameRole owner;
	};

	inline const ChessPiece::Type& ChessPiece::getType() const
	{
		return piece_type;
	}

	inline bool ChessPiece::isType(ChessPiece::Type piece) const
	{
		return piece_type == piece;
	}

	inline GameRole ChessPiece::getOwner() const
	{
		return owner;
	}

	inline void ChessPiece::setOwner(GameRole owner)
	{
		this->owner = owner;
	}

	/*! @} */

}

#endif // CNCS_CHESSPIECE_H_
