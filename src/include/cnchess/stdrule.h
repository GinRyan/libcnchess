/* 
 * File:   stdrule.h
 * Author: alec
 *
 * Created on 2012年6月5日, 下午1:45
 */

#ifndef CNCS_STDRULE_H
#define	CNCS_STDRULE_H

#include <vector>
#include "chessbook.h"
#include "iccsmotion.h"
#include "piecepos.h"
#include "game.h"

namespace cnchess {

	/** @defgroup stdrule 中国象棋标准规则
	 *
	 *  标准规则相关的工具集合
	 *
	 *  @{
	 */

	typedef std::vector<PieceMotion> PathList;
	typedef std::vector<ChessPiece::Type> ExcludePiece;
	
	/**
	 * \brief 填充标准的中国象棋布局到 `ChessBook` 里
	 * 
	 * @param cb
	 * @return 返回指向 `cb` 的指针
	 */
	ChessBook* stdrule_initchessbook(ChessBook& cb);

	/**
	 * 根据中国象棋标准规则, 取得一个棋子在当前棋盘上的可走路径
	 * 
	 * @param pathlist
	 * @param cb
	 * @param piece
	 * 
	 * @note 棋子必须是当前棋盘内的, 否则会失败
	 * 
	 * @return 成功返回指向 pathlist 的指针, 失败返回 NULL;
	 */
	PathList* stdrule_getpath(PathList& pathlist, const ChessBook& cb,
		const ChessPiece& piece);

	/**
	 * 根据标准中国象棋规则, 检查一个棋子的位置在棋盘上是否有效
	 * 
	 * piece 必须是一个有效的棋子, 也就是 ChessPiece::getType() 不能是 ChessPiece::NONE, 并且
	 * 棋子必须属于红黑其中一方
	 * 
	 * @param pos
	 * @param piece
	 * @return 
	 */
	bool stdrule_posvalid(const PiecePos& pos, const ChessPiece& piece);
	bool stdrule_posvalid(int x, int y, const ChessPiece& piece);

	/**
	 * 取得一个棋子的有效落点范围
	 * 
	 * @param range
	 * @param piece 一个有效棋子
	 * @return 如果 piece 无效, 也就是不属于 RL_RED 或者 RL_BLACK, 则返回 NULL, 
	 * 否则返回指向 range 的指针
	 */
	PiecePosRange* stdrule_getrange(PiecePosRange& range, const ChessPiece& piece);

	/**
	 * 随机产生一步
	 * 
     * @param motion
     * @param cb 当前棋盘
     * @param role 查找对象
	 * @param exclude_piece 排除的棋子
     * @return 成功返回指向 `motion` 的指针， 如果一个棋盘上 `role` 的子一个都不可走， 则返回 NULL
     */
	PieceMotion* stdrule_findstep(PieceMotion& motion, const ChessBook& cb, GameRole role, const ExcludePiece* exclude = NULL);
	
	/**
	 * 取得最后一步
	 * 
	 * “最后一步“ 也就是可以吃掉对方 “帅” 的那步， 如果 `motion` 为 NULL, 则相当于判断是否有这 “最后一步“
	 * 
     * @param motion 
     * @param cb
     * @param role 查找对象
     * @return 找到返回 true， 找不到返回 false
     */
	bool stdrule_laststep(PieceMotion* motion, const ChessBook& cb, GameRole role);
	
	/**
	 * 判断当前棋局是谁赢了
	 * 
     * @param cb
     * @param lastmotion
     * @return 红方赢返回 RL_RED, 黑方赢返回 RL_BLACK， 未分胜负返回 RL_NONE
     */
	GameRole stdrule_whowin(const ChessBook& cb, const PieceMotion& lastmotion);
	
	
	/**
	 * 查找长手, 目前只能识别两步循环
	 * 
     * @param history
     * @param role
     * @return 有循环则返回该棋步, 否则返回 NULL
     */
	const ICCSMotion* stdrule_findbanmove(const Game::History& history, GameRole role, int roundmax = 3);
	
	/** @} */ // end of stdrule

}

#endif	/* CNCS_STDRULE_H */

