/* 
 * File:   coordinator.h
 * Author: alec
 *
 * Created on 2012年5月23日, 上午9:26
 */

#ifndef CNCS_COORDINATOR_H
#define	CNCS_COORDINATOR_H

#include <assert.h>
#include <vector>
#include "iccsmotion.h"
#include "refcounter.h"
#include "mutex.h"
#include "game.h"
#include "gamereferee.h"
#include "dispatcher.h"
#include "actiontimer.h"


namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */

	class GameAction;
	class GamePlayer;
	class GameManager;
	class GameReferee;
	class Coordinator;

	/**
	 * \brief 玩家动作类型
	 */
	enum GameActionType {
		AC_MOVED = 100, /*!< 移动棋子 */
		AC_REQ_MOVE, /*!< 请求对方移动棋子 */
		AC_REQ_DRAW, /*!< 请求对方和棋 */
		AC_REQ_RETRACT, /*!< 请求对方悔棋 */
		AC_REQ_GIVEUP /*!< 认输 */
	};

	/**
	 * \class GameAction
	 * \brief 玩家动作
	 */
	struct GameAction {
		/*! 动作类型 */
		GameActionType action;
		/*! 协调员 */
		const Coordinator* coordinator;
		/*! 执行动作的玩家 */
		const GamePlayer* player; /* sender */
		/*! 额外附加数据, 在动作类型是 AC_MOVED 的时候, 这里就是 ICCSMotion 指针 */
		void* data;
	};

	enum GiveUpReason {
		GURSON_LOSED,
		GURSON_GIVEUP
	};
	
	/**
	 * \class Coordinator
	 * 
	 * \brief 双方玩家交互协调员基类, Coordinator 被 GameManager 用于协调玩家的交互
	 * 
	 * 通过实现该类, 你可以控制双方玩家的交互
	 */
	class Coordinator : public RefCounter {
	public:

		/**
		 * \brief 处理结果
		 */
		enum Result {
			REFUSE = 0, /*!< 拒绝 */
			AGREE, /*!< 同意 */
			INVALID, /*!< 无效请求 */
			EXPIRED /*!< 处理超时 */
		};

		typedef Dispatcher<Coordinator, void*, const GamePlayer*,
		const GameAction&, Result> ActionDispatcher;

		/**
		 * \brief 处理玩家的 action listener
		 * 
		 * 该 listener 在 Coordinator 处理完玩家的 action 后会被调用
		 * 
		 * 签名为 void (const Coordinator*, void*, const GamePlayer*, 
		 * const GameAction&, Result)
		 */
		typedef ActionDispatcher::Listener ActionListener;

		/**
		 * 移动棋子
		 * 
		 * @param player
		 * @param motion
		 * @return 
		 */
		virtual Result move(const GamePlayer* player, const ICCSMotion& motion) = 0;
		Result move(const GamePlayer* player, const std::string& motion);
		Result move(const GamePlayer* player, const PieceMotion& motion);
		
		/**
		 * 请求对手移动棋子
		 * 
		 * @param player
		 * @return 
		 */
		virtual Result requestMove(const GamePlayer* player) = 0;

		/**
		 * 向对手求和
		 * 
		 * @param player
		 * @return 
		 */
		virtual Result requestDraw(const GamePlayer* player) = 0;

		/**
		 * 向对手请求悔棋
		 * 
		 * @param player
		 * @return 
		 */
		virtual Result requestRetract(const GamePlayer* player) = 0;

		/**
		 * 主动认输
		 * 
		 * @param player
		 * @return 
		 */
		virtual Result requestGiveUp(const GamePlayer* player, GiveUpReason reason) = 0;

		/**
		 * 添加 listener
		 * 
		 * @param listener
		 * @param ctx
		 */
		void addListener(ActionListener listener, void* ctx = NULL);

		/**
		 * 移除 listener
		 * 
		 * @param listener
		 */
		void removeListener(ActionListener listener);

	protected:

		/**
		 * 构造
		 * 
		 * @param game Game 对象
		 * @param gtimer_red 红方的计时器
		 * @param gtimer_black 黑方的计时器
		 */
		Coordinator(Game* game, GameTimer* gtimer_red = NULL, GameTimer* gtimer_black = NULL);
		virtual ~Coordinator();

		/**
		 * 获得 Game 对象
		 * 
		 * @return 
		 */
		Game* getGame();
		const Game* getGame() const;

		/**
		 * 获得玩家的定时器
		 * 
		 * @param role
		 * @return 如果不限时则返回 NULL
		 */
		GameTimer* getTimer(GameRole role);
		const GameTimer* getTimer(GameRole role) const;

		/**
		 * 广播 action 事件给所有的 listener
		 * 
		 * @param player
		 * @param ac
		 * @param res action处理结果
		 */
		void fireActionEvent(const GamePlayer* player, const GameAction& ac, Result res);
	private:
		Game* game;
		GameTimer* gtimer_red;
		GameTimer* gtimer_black;
		ActionDispatcher action_ev_dispatcher;

		Coordinator(const Coordinator&);
		Coordinator& operator =(const Coordinator&);
	};

	inline Game* Coordinator::getGame()
	{
		return game;
	}

	inline const Game* Coordinator::getGame() const
	{
		return game;
	}

	inline void Coordinator::addListener(Coordinator::ActionListener listener, void* ctx)
	{
		action_ev_dispatcher.add(listener, ctx);
	}

	inline void Coordinator::removeListener(Coordinator::ActionListener listener)
	{
		action_ev_dispatcher.remove(listener);
	}

	inline void Coordinator::fireActionEvent(const GamePlayer* player,
		const GameAction& ac, Result res)
	{
		action_ev_dispatcher.fire(this, player, ac, res);
	}

	/*! @} */

}

#endif	/* CNCS_COORDINATOR_H */

