/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_CNCHESS_H_
#define CNCS_CNCHESS_H_	1

#include "chessbook.h"
#include "iccsmotion.h"
#include "fen.h"
#include "gametimer.h"
#include "game.h"
#include "gameplayer.h"
#include "stdreferee.h"
#include "stdcoordinator.h"
#include "gamemanager.h"
#include "stdrule.h"
#include "chessarea.h"
#include "ucciplayer.h"
#include "ucciutil.h"

/********************************************************
 * for doxygen
 * 
 * 
 */

/*!
 * \example example0.cc
 * \example example1.cc
 * \example example2.cc
 * 
 */

/**
 *  @defgroup global 全局的一些定义
 *  通用定义
 */

/**
 *  @defgroup utility 辅助工具
 *  内部外部皆有用的工具
 */

/**
 *  @defgroup chessbook 棋盘
 *
 *  各种棋盘数据结构和分析工具
 */

/**
 *  @defgroup game 对弈
 *  游戏管理
 */

#endif
