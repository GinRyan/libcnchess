/* 
 * File:   listener.h
 * Author: alec
 *
 * Created on 2012年5月18日, 下午1:32
 */

#ifndef CNCS_LISTENER_H
#define	CNCS_LISTENER_H

#include <vector>
#include <assert.h>
#include "mutex.h"

namespace cnchess {

	/*! \addtogroup utility
	 *  @{
	 */

	/**
	 * \class Dispatcher
	 * 
	 * \brief 线程安全的回调函数容器和事件分发器
	 * 
	 * @note 本模板用到了 c++0x 中的 "可变长度模板参数" 特性, 所以你的编译器必须支持这个特性, 
	 * gcc-v4.3 以上都支持本特性
	 */
	template<typename TSender, typename TContext = void*, typename... TArgs>
		class Dispatcher {
	public:
		typedef void (*Listener)(const TSender* sender, TContext ctx, TArgs...);

		/**
		 * \brief 添加新 listener
		 * 
		 * @param listener
		 * @param ctx 上下文, 在 listener 被调用的时候会作为第二个参数传给 listener
		 * @note 同一个 listener 允许添加多次
		 */
		void add(Listener listener, TContext ctx)
		{
			assert(listener);

			ListenerObject obj = {listener, ctx};
			mutex.lock();
			listeners.push_back(obj);
			mutex.unlock();
		}

		/**
		 * \brief 移除一个存在的 listener
		 * 
		 * @param listener
		 * @param all 是否移除所有同样的 listener
		 */
		void remove(Listener listener, bool all = false)
		{
			assert(listener);

			for (size_t i = 0; i < listeners.size(); i++) {
				if (listeners[i].listener && (listeners[i].listener == listener)) {
					mutex.lock();
					listeners.erase(listeners.begin() + i);
					mutex.unlock();
					if (!all) {
						return;
					}
				}
			}
		}

		/**
		 * \brief 清除所有已添加的 listener
		 */
		void clear()
		{
			mutex.lock();
			listeners.clear();
			mutex.unlock();
		}

		/**
		 * \brief 取得已添加的某个 listener 的数量
		 * 
		 * @return 
		 */
		size_t count(Listener listener) const
		{
			assert(listener);

			size_t res = 0;
			for (int i = 0; i < listeners.size(); i++) {
				if (listeners[i].listener && (listeners[i].listener == listener)) {
					res++;
				}
			}
			return res;
		}

		/**
		 * \brief 取得已添加的所有 listener 的数量
		 * 
		 * @return 
		 */
		size_t size() const
		{
			return listeners.size();
		}

		/**
		 * \brief 检查某个 listener 是否存在
		 * 
		 * @param listener
		 * @return 
		 */
		bool exists(Listener listener) const
		{
			assert(listener);

			for (int i = 0; i < listeners.size(); i++) {
				if (listeners[i].listener && (listeners[i].listener == listener)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * \brief 发送事件给所有 listener
		 * 
		 * @param sender 事件发送者
		 * @param args 额外参数
		 */
		void fire(const TSender* sender, TArgs... args)
		{
			for (size_t i = 0; i < listeners.size(); i++) {
				if (listeners[i].listener) {
					listeners[i].listener(sender, listeners[i].ctx, args...);
				}
			}
		}

	private:

		struct ListenerObject {
			Listener listener;
			TContext ctx;
		};
		typedef std::vector<ListenerObject> ListenerContainer;

		ListenerContainer listeners;
		mutable Mutex mutex;
	};

	/*! @} */

}

#endif	/* CNCS_LISTENER_H */

