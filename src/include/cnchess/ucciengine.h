/* 
 * File:   ucciengine.h
 * Author: alec
 *
 * Created on 2012年6月18日, 下午4:37
 */

#ifndef CNCS_UCCIENGINE_H
#define	CNCS_UCCIENGINE_H

#include <time.h>
#include <sys/time.h>
#include <vector>
#include <map>
#include <string>
#include "refcounter.h"

namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */

	/**
	 * \class UCCIEngine
	 * \brief UCCI 标准引擎支持
	 */
	class UCCIEngine : public RefCounter {
	public:
		typedef std::vector<std::string> CmdLineArgs;

		/**
		 * constructor
		 * 
		 * @param ucci_engine 引擎可执行文件路径
		 */
		UCCIEngine(const std::string& ucci_engine);

		/**
		 * constrcutor
		 * 
		 * @param ucci_engine
		 * @param argv	启动引擎时的额外命令行参数
		 */
		UCCIEngine(const std::string& ucci_engine, const CmdLineArgs& argv);
		~UCCIEngine();

		/**
		 * 启动引擎
		 * 
		 * @return 成功返回 true, 失败返回 false
		 */
		bool start();

		/**
		 * 停止引擎
		 */
		void stop();

		/**
		 * 取得引擎路径
		 * 
		 * @return 
		 */
		const std::string& getEngine() const;

		/**
		 * 取得当前运行状态
		 * 
		 * @return IDLE/STARTED/STOPPED
		 * 
		 * @see RunStatus
		 */
		int getStatus() const;

		/**
		 * 读取引擎反馈
		 * 
		 * 本函数会读取引擎的反馈, 以行为单位, 如果反馈有多行, 你需要多次调用本函数.
		 * 本函数是阻塞式的等待读取, 如果引擎一直不反馈则会一直等待
		 * 
		 * @param resp
		 * @return 成功返回指向 `resp` 的指针, 失败返回 NULL
		 */
		std::string* getResponse(std::string& resp);

		/**
		 * 读取引擎反馈
		 * 
		 * `timeo` 为超时时间, 如果 `timeo` 为 0, 并且没有反馈可读, 则立即返回, 
		 * 如果 `timeo` 不为 0, 则尝试读取反馈直到 `timeo` 设置的时间超时
		 * 
		 * @param resp
		 * @param timeo 
		 * @return 成功返回指向 `resp` 的指针, 失败或者超时返回 NULL
		 */
		std::string* getResponse(std::string& resp, const timeval& timeo);

		/**
		 * 发送请求
		 * 
		 * @param req
		 * @return 成功返回 true
		 */
		bool sendRequest(const std::string& req);
	private:
		std::string ucci_engine;
		CmdLineArgs ucci_engine_argv;
		int ucci_eg_rpipe[2];
		int ucci_eg_wpipe[2];
		pid_t cpid;
		int status;

		static void onParentProcExit(int signo);
		bool openPipe();
		void closePipe();
		bool ready();
		bool ucciStart();
		void ucciStop();
		bool engineIsRunning() const;
		ssize_t writeToEngine(const std::string& req);
		ssize_t readFromEngine(std::string& resp);
		ssize_t readFromEngine(std::string& resp, const timeval& timeo);
		bool waitResponse(const timeval& timeo);
		void killChildProcess();
		void setStatus(int st);
		UCCIEngine(const UCCIEngine&);
		UCCIEngine& operator =(const UCCIEngine&);
	};

	/*! @} */

}

#endif	/* CNCS_UCCIENGINE_H */

