/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_GAMEREFEREE_H
#define CNCS_GAMEREFEREE_H


#include "define.h"
#include "iccsmotion.h"
#include "refcounter.h"
#include "game.h"


namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */

	class GamePlayer;

	/**
	 * \class GameReferee
	 * 
	 * \brief 游戏裁判基类, 裁判是玩家行为的仲裁者, 游戏规则的制定者
	 */
	class GameReferee : public RefCounter {
	public:

		/**
		 * 游戏开始时, 本函数被 GameManager 调用
		 * 
		 * @param game
		 * \note 如果你重载了本函数, 那么你的函数里应调用父函数
		 */
		virtual void onGameStart(const Game* game);

		/**
		 * 游戏结束时, 本函数被 GameManager 调用
		 * 
		 * @param status 游戏状态
		 * 
		 * \note 如果你重载了本函数, 那么你的函数里应调用父函数
		 */
		virtual void onGameOver(Game::Status status);

		/**
		 * 取得代表当前游戏的 Game 对象
		 * 
		 * @return 
		 */
		virtual const Game* getGame() const;

		/**
		 * 玩家求和, 是否批准
		 * 
		 * 默认实现是批准
		 * 
		 * @param player
		 * @return 
		 */
		virtual bool requestDraw(const GamePlayer* player);

		/**
		 * 玩家请求悔棋, 是否批准
		 * 
		 * 默认实现是批准
		 * 
		 * @param player
		 * @return 
		 */
		virtual bool requestRetract(const GamePlayer* player);

		/**
		 * 玩家请求移动棋子, 是否批准
		 * 
		 * @param player
		 * @param motion
		 * @return 
		 */
		virtual bool requestMove(const GamePlayer* player, const ICCSMotion& motion) = 0;

	protected:
		GameReferee();
		virtual ~GameReferee();
	private:
		const Game* game;
		GameReferee(const GameReferee& other);
		GameReferee& operator =(const GameReferee& other);
	};

	inline GameReferee::GameReferee()
	{

	}

	inline GameReferee::~GameReferee()
	{

	}

	inline void GameReferee::onGameStart(const Game* game)
	{
		this->game = game;
	}

	inline void GameReferee::onGameOver(Game::Status status)
	{
		;
	}

	inline const Game* GameReferee::getGame() const
	{
		return game;
	}

	inline bool GameReferee::requestDraw(const GamePlayer* player)
	{
		return true;
	}

	inline bool GameReferee::requestRetract(const GamePlayer* player)
	{
		return true;
	}

	/**
	 * \class DummyReferee
	 * 
	 * \brief 假裁判, 批准用户所有行为, 本类一般用于测试
	 */
	class DummyReferee : public GameReferee {
	public:
		bool requestMove(const GamePlayer* player, const ICCSMotion& motion);
	};

	inline bool DummyReferee::requestMove(const GamePlayer* player, const ICCSMotion& motion)
	{
		return true;
	}

	/*! @} */

}

#endif // CNCS_GAMEREFEREE_H
