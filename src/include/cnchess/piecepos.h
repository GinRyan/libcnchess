/* 
 * File:   piecepos.h
 * Author: alec
 *
 * Created on 2012年6月11日, 上午10:23
 */

#ifndef CNCS_PIECEPOS_H
#define	CNCS_PIECEPOS_H

#include <vector>


namespace cnchess {

	/*! \addtogroup chessbook
	 *  @{
	 */

	/**
	 * \class PiecePos
	 * \brief 代表 ChessBook 的一个位置
	 */
	class PiecePos {
	public:

		/**
		 * 初始化一个棋盘位置
		 * 
		 * @param x
		 * @param y
		 * 
		 * @exception std::invalid_argument x or y invalid
		 */
		PiecePos(int x = 0, int y = 0);

		/**
		 * 取得 X 坐标
		 * 
		 * @return 
		 */
		int getX() const;

		/**
		 * 设置 X 坐标
		 * 
		 * @param x
		 * \exception std::invalid_argument x 无效
		 */
		void setX(int x);

		/**
		 * 取得 Y 坐标
		 * 
		 * @return 
		 */
		int getY() const;

		/**
		 * 设置 Y 坐标
		 * 
		 * @param y
		 * \exception std::invalid_argument x 无效
		 */
		void setY(int y);

		/**
		 * 检查当前坐标是否在范围内
		 * 
		 * @param range
		 * @return 
		 */
		bool inrange(const std::vector<PiecePos>& range) const;

		/**
		 * 检查两个位置是否相等
		 * 
		 * @param other
		 * @return 
		 */
		bool equal(const PiecePos& other) const;

	private:
		int x;
		int y;
	};

	/**
	 * \brief 表示一个坐标范围
	 */
	typedef std::vector<PiecePos> PiecePosRange;
	typedef PiecePosRange PiecePosList;
	
	/**
	 * \class PieceMotion
	 * 
	 * \brief 代表一个棋子在两点之间移动
	 * 
	 * @see ICCSMotion
	 */
	struct PieceMotion {
		/**
		 * \brief 起始坐标
		 */
		PiecePos start;

		/**
		 * \brief 终点坐标
		 */
		PiecePos end;
		
		/**
		 * constructor
		 * 
		 * 初始化后 start 和 end 分别指向 (0, 0), (0, 1) 位置
         */
		PieceMotion();
		
		/**
		 * 构建路径
		 * 
		 * @param start
		 * @param end
		 * 
		 * @exception std::invalid_argument 两个点 x,y 均相同
		 */
		PieceMotion(const PiecePos& start, const PiecePos & end);

		/**
		 * 检测两个 PieceMotion 的 start/end 是否一样
		 * 
		 * @param other
		 * @return 
		 */
		bool equal(const PieceMotion & other) const;
	};

	inline PieceMotion::PieceMotion() : end(0, 1) {
		
	}
	
	inline bool PieceMotion::equal(const PieceMotion& other) const
	{
		return start.equal(other.start) && end.equal(other.end);
	}

	inline PiecePos::PiecePos(int x, int y)
	{
		setX(x);
		setY(y);
	}

	inline int PiecePos::getX() const
	{
		return x;
	}

	inline int PiecePos::getY() const
	{
		return y;
	}

	inline bool PiecePos::equal(const PiecePos& other) const
	{
		return(x == other.getX()) && (y == other.getY());
	}

	/*! @} */

}

#endif	/* CNCS_PIECEPOS_H */

