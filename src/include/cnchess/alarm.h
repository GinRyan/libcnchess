/* 
 * File:   alarm.h
 * Author: alec
 *
 * Created on 2012年5月18日, 下午12:37
 */

#ifndef CNCS_ALARM_H
#define	CNCS_ALARM_H

#include "clock.h"
#include "mutex.h"
#include "dispatcher.h"

namespace cnchess {

	/*! \addtogroup utility
	 *  @{
	 */

	/*! \class Alarm
	 *  
	 * \brief 代表一个闹钟, 可用于提醒, 超时
	 * \see IClock, RealClock
	 */
	class Alarm {
	public:

		/*! \brief 闹钟到时的 listener
		 * 
		 * 签名为 void listener(const Alarm* alarm, void* ctx) 
		 */
		typedef Dispatcher<Alarm>::Listener Listener;

		/*! \brief 闹钟行为 */
		enum {
			REPEAT = 0, /*!< 到期之后再重新启动计时 */
			ONCE = 1 /*!< 到期之后闹钟停止, 不再计时 */
		};

		/**
		 * \brief 创建一个闹钟
		 * 
		 * @param clock	时钟
		 * @param expire_time 到期时间, 毫秒
		 * @param once 闹钟行为
		 */
		Alarm(IClock* clock, long expire_time, int once = ONCE);
		~Alarm();

		/**
		 * \brief 取得闹钟使用的时钟
		 * 
		 * @return 
		 */
		IClock* getClock();
		const IClock* getClock() const;

		/**
		 * \brief 取得闹钟的状态
		 * 
		 * @return 
		 */
		RunStatus getStatus() const;

		/**
		 * \brief 取得闹钟的到期时间
		 * 
		 * @return 
		 */
		long getExpireTime() const;

		/**
		 * \brief 取得离到期时间的剩余时间
		 * 
		 * @return 
		 */
		long getRemainTime() const;

		/**
		 * \brief 添加 listener
		 * 
		 * @param listener
		 * @param ctx
		 */
		void addListener(Listener listener, void* ctx = NULL);

		/**
		 * \brief 删除 listener
		 * 
		 * @param listener
		 */
		void removeListener(Listener listener);

		/**
		 * \brief 启动闹钟, 开始计时
		 */
		void start();

		/**
		 * \brief 停止闹钟
		 */
		void stop();

		/**
		 * \brief 重启闹钟, 并重新开始计时
		 */
		void restart();
	private:
		IClock* clock;
		RunStatus status;
		long expiretime;
		long remaintime;
		int once;
		Dispatcher<Alarm> dispatcher;
		mutable Mutex mutex;

		static void clockListener(const IClock* clock, void* ctx);
		Alarm(const Alarm& other);
		Alarm& operator =(const Alarm& other);
		void setStatus(RunStatus newst);
	};

	inline void Alarm::setStatus(RunStatus newst)
	{
		mutex.lock();
		status = newst;
		mutex.unlock();
	}

	inline IClock* Alarm::getClock()
	{
		return clock;
	}

	inline const IClock* Alarm::getClock() const
	{
		return clock;
	}

	inline RunStatus Alarm::getStatus() const
	{
		return status;
	}

	inline long Alarm::getExpireTime() const
	{
		return expiretime;
	}

	inline long Alarm::getRemainTime() const
	{
		if (status == STARTED) {
			return(remaintime < 0) ? 0 : remaintime;
		} else {
			return -1;
		}
	}

	inline void Alarm::addListener(Alarm::Listener listener, void* ctx)
	{
		dispatcher.add(listener, ctx);
	}

	inline void Alarm::removeListener(Alarm::Listener listener)
	{
		dispatcher.remove(listener);
	}

	inline void Alarm::restart()
	{
		stop();
		start();
	}

	/*! @} */

}

#endif	/* CNCS_ALARM_H */

