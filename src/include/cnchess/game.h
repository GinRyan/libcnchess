/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_GAME_H
#define CNCS_GAME_H

#include <vector>
#include <string>
#include <assert.h>
#include "iccsmotion.h"
#include "chessbook.h"
#include "mutex.h"
#include "refcounter.h"
#include "define.h"
#include "gametimer.h"
#include "dispatcher.h"


namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */

	class GamePlayer;
	class GameReferee;

	/**
	 * \class PlayHistory
	 * \brief 棋步历史记录
	 */
	struct PlayHistory {
		ICCSMotion motion;
		const GamePlayer *player;
		PlayHistory(const std::string& strmotion, const GamePlayer * player);
		PlayHistory(const ICCSMotion& motion, const GamePlayer * player);
	};

	inline PlayHistory::PlayHistory(const std::string& strmotion,
		const GamePlayer *player) : player(player)
	{
		motion = ICCSMotion(strmotion);
	}

	inline PlayHistory::PlayHistory(const ICCSMotion& motion,
		const GamePlayer *player) : motion(motion), player(player)
	{
	}
	
	/**
	 * \class Game
	 * \brief 代表一个新棋局或者残局
	 * 
	 * 一个进行中的 Game 的状态由 GameManager 维护, 在 Game 进行中请勿通过其他途径改变 Game 内的状态
	 */
	class Game : public RefCounter {
	public:

		/**
		 * \brief 棋局状态
		 */
		enum Status {
			WRED, /*!< 红方胜 */
			WBLACK, /*!< 黑方胜 */
			PAUSED, /*!< 棋局暂停 */
			PLAYING /*!< 棋局正在进行中,未分胜负 */
		};

		/**
		 * \brief 棋步历史记录
		 */
		typedef std::vector<PlayHistory> History;

		typedef Dispatcher<Game, void*, Status> StatusDispatcher;

		/**
		 * \brief 棋局状态变化 listener
		 * 
		 * 当棋局状态产生了变化, 例如有一方得胜, listener 将获得通知
		 * listener 签名为 void (const Game*, void*, Game::Status)
		 */
		typedef StatusDispatcher::Listener StatusListener;

		/**
		 * 创建新棋局
		 * 
		 * @param chessbook		开局棋盘
		 * @param red_player	红方玩家
		 * @param black_player	黑方玩家
		 * @param referee		裁判
		 * @param timeri		计时器
		 * @param firstplayer	哪一方先手, 默认是红方
		 */
		Game(const ChessBook& chessbook, GamePlayer *red_player,
			GamePlayer *black_player, GameReferee* referee,
			TimerInfo* timeri = NULL, GameRole firstplayer = RL_RED);

		~Game();

		/**
		 * 设置棋局名, 例如: 珍胧棋局
		 * 
		 * @param name
		 */
		void setName(const std::string& name);

		/**
		 * 获取棋局名
		 * 
		 * @return 默认为空
		 */
		const std::string& getName() const;

		/**
		 * 设置棋局描述
		 * 
		 * @param description
		 */
		void setDescription(const std::string& description);

		/**
		 * 获取棋局描述
		 * 
		 * @return 
		 */
		const std::string& getDescription() const;

		/**
		 * 取得棋局的计时器信息
		 * 
		 * @return 
		 */
		const TimerInfo* getTimer() const;

		/**
		 * 取得棋局中下第一步的玩家
		 * 
		 * @return 
		 */
		GamePlayer* getFirst();
		const GamePlayer* getFirst() const;

		/**
		 * 设置下一步轮到哪方玩家
		 * 
		 * @param next
		 */
		void setNext(GamePlayer* next);
		void setNext(GameRole next);

		/**
		 * 取得下一步轮到哪方玩家
		 * 
		 * @return 
		 */
		GamePlayer* getNext();
		const GamePlayer* getNext() const;

		/**
		 * 取得当前轮到下棋的玩家
		 * 
		 * @return 
		 */
		GamePlayer* getCurrent();
		const GamePlayer* getCurrent() const;

		/**
		 * 根据指定角色获得棋局中的一个玩家
		 * 
		 * @param role
		 * @return 
		 */
		GamePlayer* getPlayer(GameRole role);
		const GamePlayer* getPlayer(GameRole role) const;

		/**
		 * 根据给出的玩家, 获取该玩家在棋局中的对手
		 * @param me
		 * @return 
		 */
		GamePlayer* getAdversary(const GamePlayer* me);
		const GamePlayer* getAdversary(const GamePlayer* me) const;

		/**
		 * 获取一个玩家在棋局中的角色
		 * 
		 * @param player
		 * @return 
		 */
		GameRole getRole(const GamePlayer* player) const;

		/**
		 * 取得当前棋局的裁判
		 * 
		 * @return 
		 */
		GameReferee* getReferee();
		const GameReferee* getReferee() const;

		/**
		 * 设置棋局状态
		 * 
		 * @param st
		 */
		void setStatus(Game::Status st);

		/**
		 * 获取棋局状态
		 * @return 
		 */
		Game::Status getStatus() const;

		/**
		 * 获取当前棋盘信息
		 * 
		 * @return 
		 */
		ChessBook& getBoard();
		const ChessBook& getBoard() const;

		/**
		 * 获取开局棋盘信息
		 * 
		 * @return 
		 */
		const ChessBook& getChessBook() const;

		/**
		 * 取得双方棋步历史
		 * 
		 * @return 
		 */
		History& getHistory();
		const History& getHistory() const;

		/**
		 * 添加 listener
		 * 
		 * @param listener
		 * @param ctx
		 */
		void addListener(StatusListener listener, void* ctx = NULL);

		/**
		 * 移除 listener
		 * 
		 * @param listener
		 */
		void removeListener(StatusListener listener);

	private:
		std::string name;
		std::string description;
		GameReferee* referee;
		ChessBook chessbook;
		ChessBook board;
		History history;
		GamePlayer* first;
		GamePlayer* next;
		GamePlayer* red_player;
		GamePlayer* black_player;
		GamePlayer* current_player;
		TimerInfo* timer_info;
		Game::Status status;
		StatusDispatcher status_ev_dispatcher;
		mutable Mutex mutex;

		Game(const Game& rhs);
		Game& operator=(const Game& rhs);
		void setTimer(TimerInfo* timer);
	};

	inline ChessBook& Game::getBoard()
	{
		return board;
	}

	inline const ChessBook& Game::getBoard() const
	{
		return board;
	}

	inline const ChessBook& Game::getChessBook() const
	{
		return chessbook;
	}

	inline const std::string& Game::getDescription() const
	{
		return description;
	}

	inline GamePlayer* Game::getFirst()
	{
		return first;
	}

	inline const GamePlayer* Game::getFirst() const
	{
		return first;
	}

	inline GamePlayer* Game::getCurrent()
	{
		return this->current_player;
	}

	inline const GamePlayer* Game::getCurrent() const
	{
		return this->current_player;
	}

	inline GamePlayer* Game::getPlayer(GameRole role)
	{
		return(role == RL_RED) ? red_player : black_player;
	}

	inline const GamePlayer* Game::getPlayer(GameRole role) const
	{
		return(role == RL_RED) ? red_player : black_player;
	}

	inline GamePlayer* Game::getAdversary(const GamePlayer* one)
	{
		return(one == red_player) ? black_player : red_player;
	}

	inline const GamePlayer* Game::getAdversary(const GamePlayer* one) const
	{
		return(one == red_player) ? black_player : red_player;
	}

	inline GameRole Game::getRole(const GamePlayer* player) const
	{
		return(getPlayer(RL_RED) == player) ? RL_RED : RL_BLACK;
	}

	inline Game::History& Game::getHistory()
	{
		return history;
	}

	inline const Game::History& Game::getHistory() const
	{
		return history;
	}

	inline const std::string& Game::getName() const
	{
		return name;
	}

	inline GamePlayer* Game::getNext()
	{
		return next;
	}

	inline const GamePlayer* Game::getNext() const
	{
		return next;
	}

	inline GameReferee* Game::getReferee()
	{
		return referee;
	}

	inline const GameReferee* Game::getReferee() const
	{
		return referee;
	}

	inline Game::Status Game::getStatus() const
	{
		return status;
	}

	inline void Game::addListener(Game::StatusListener listener, void* ctx)
	{
		status_ev_dispatcher.add(listener, ctx);
	}

	inline void Game::removeListener(Game::StatusListener listener)
	{
		status_ev_dispatcher.remove(listener);
	}

	/*! @} */

}

#endif // CNCS_GAME_H
