/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_CHESSBOOK_H_
#define CNCS_CHESSBOOK_H_	1

#include <stdint.h>
#include <string>
#include <vector>
#include <cstdio>
#include <cstddef>
#include <stdexcept>
#include <istream>
#include "chessrow.h"
#include "mutex.h"
#include "piecepos.h"
#include "define.h"

namespace cnchess {

	/*! \addtogroup chessbook
	 *  @{
	 */

	/*! \class ChessBook
	 *  
	 * \brief 代表一个棋盘
	 * 
	 * 上(black) 下(red)
	 */
	class ChessBook {
	public:

		/*
		typedef ChessPiece ChessPieceArray[CHESSBOOK_ROW][CHESSBOOK_COL];
		typedef ChessPiece (*ChessPieceArrayPtr)[CHESSBOOK_ROW][CHESSBOOK_COL];
		 */

		/**
		 * \brief 取得棋盘的行数
		 * 
		 * @return 
		 */
		static std::size_t size();

		/**
		 * \brief 构建一个空棋盘
		 */
		ChessBook();
		ChessBook(const ChessBook& other);
		ChessBook& operator =(const ChessBook& other);
		~ChessBook();

		/**
		 * \brief 取得棋盘内的一行
		 * 
		 * @param index 行索引, 从0开始
		 * @return 
		 */
		ChessRow& operator[](int index);
		const ChessRow& operator[](int index) const;

		/**
		 * \breif 取得一个 piece
		 * 
		 * @param pos
		 * @return 
		 */
		ChessPiece& get(const PiecePos& pos);
		const ChessPiece& get(const PiecePos& pos) const;

		/**
		 * 取得一个 piece 在棋盘中的位置(PiecePos)
		 * 
		 * piece 的查找比较是地址比较, 也就是说 piece 必须是当前棋盘中的一个棋子
		 * 
		 * @param pos
		 * @param piece
		 * @return 成功返回 true, 失败返回 false, 如果 pos 为 NULL, 则等于查找一个 piece 在不在当前棋盘中
		 */
		bool locate(PiecePos* pos, const ChessPiece& piece) const;
		bool locate(PiecePos& pos, const ChessPiece& piece) const;
		
		/**
		 * 根据棋子类型， 查找不为 ChessPiece::NONE 的棋子， 并返回这些棋子的位置
		 * 
         * @param pos 位置列表
         * @param piecetype
         * @return 找到的数目
         */
		size_t find(PiecePosList& pos, ChessPiece::Type piecetype, GameRole role = RL_NONE) const;
		
		/**
		 * 检查一个 piece 是否在当前棋盘中
		 * 
		 * @param 
		 * @return 
		 */
		bool contains(const ChessPiece& piece) const;

		/**
		 * 移动棋子
		 * 
		 * 将一个棋子移动到棋盘上的目标地点, 如果目标地点已经有棋子, 该棋子将被替换
		 * 
		 * @param start
		 * @param end
		 * @return 成功返回 true, 如果 start 的位置不是一个有效的棋子, 返回 false
		 */
		bool move(const PiecePos& start, const PiecePos& end);
		bool move(const PieceMotion& motion);
		bool move(const ChessPiece& piece, const PiecePos& end);
		bool move(const ChessPiece& piece, int x, int y);

		/**
		 * \brief 清空整个棋盘的棋子
		 */
		void clear();

		/**
		 * \brief 比较两个 `ChessBook` 是否一样
		 * 
		 * @return true/false
		 */
		bool equal(const ChessBook& other) const;

		/**
		 * \brief 当前是否为空, 空也就是一个棋子都没有
		 * 
		 * @return 
		 */
		bool isEmpty() const;

	private:
		mutable Mutex* mutex;
		ChessRow rows[CHESSBOOK_ROW];
	};

	inline size_t ChessBook::size()
	{
		return CHESSBOOK_ROW;
	}

	inline ChessPiece& ChessBook::get(const PiecePos& pos)
	{
		return rows[pos.getY()][pos.getX()];
	}

	inline const ChessPiece& ChessBook::get(const PiecePos& pos) const
	{
		return rows[pos.getY()][pos.getX()];
	}

	inline bool ChessBook::contains(const ChessPiece& piece) const
	{
		return locate(NULL, piece);
	}

	/*! @} */

}

#endif
