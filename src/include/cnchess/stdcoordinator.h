/* 
 * File:   stdcoordinator.h
 * Author: alec
 *
 * Created on 2012年5月25日, 下午4:39
 */

#ifndef CNCS_STDCOORDINATOR_H
#define	CNCS_STDCOORDINATOR_H

#include "coordinator.h"


namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */
	
	/**
	 * \class StdCoordinator
	 * \brief 默认的 Coordinator, 标准的你来我往, 求和求悔认输的玩家交互方式
	 */
	class StdCoordinator : public Coordinator {
	public:

		struct ActionEntry {
			GameAction action;
			Result res;
		};
		typedef std::vector<ActionEntry> ActionHistory;

		StdCoordinator(Game* game, GameTimer* gtimer_red = NULL, GameTimer* gtimer_black = NULL);
		~StdCoordinator();
		Result move(const GamePlayer* player, const ICCSMotion& motion);
		Result requestMove(const GamePlayer* player);
		Result requestDraw(const GamePlayer* player);
		Result requestRetract(const GamePlayer* player);
		Result requestGiveUp(const GamePlayer* player, GiveUpReason);
		const ActionHistory& getHistory() const;
		bool isOver() const;
	private:
		DummyActionTimer* dummy_timer;
		GameReferee* referee;
		ActionHistory action_history;
		mutable Mutex mutex;

		void requestFinished(const GamePlayer* player, const GameAction& ac, Result res);
		IActionTimer* createTimerForPlayer(const GamePlayer* player, bool autostart = false);
	};

	inline const StdCoordinator::ActionHistory& StdCoordinator::getHistory() const
	{
		return action_history;
	}

	/*! @} */

}

#endif	/* CNCS_STDCOORDINATOR_H */

