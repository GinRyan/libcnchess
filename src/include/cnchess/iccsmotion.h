/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_ICCSMOTION_H_
#define CNCS_ICCSMOTION_H_	1


#include <string>
#include <stddef.h>
#include "chessbook.h"
#include "piecepos.h"


namespace cnchess {

	/*! \addtogroup chessbook
	 *  @{
	 */

	/**
	 * \brief 在 iccs 坐标体系内, x 坐标取值范围
	 */
	const char iccsmotion_x_range[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'};

	/**
	 * \brief 在 iccs 坐标体系内, y 坐标取值范围
	 */
	const int iccsmotion_y_range[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

	/*! \class ICCSPos
	 * 
	 * \brief 棋盘上的一个位置
	 */
	class ICCSPos {
	public:
		ICCSPos(const PiecePos& pos);
		ICCSPos(char x = 'A', int y = 9);
		
		/**
		 * 取得 X 坐标
		 * 
		 * \note 总是返回大写的 char 值
		 */
		char getX() const;
		int getY() const;
		void setX(char x);
		void setY(int y);
		
		/**
		 * 检查两个位置是否相等
		 * 
		 * @param other
		 * @return 
		 */
		bool equal(const ICCSPos& other) const;
		
		PiecePos* toPiecePos(PiecePos& pos) const;
	private:
		char x; /* 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i' */
		int y; /* 9 ~ 0 */
	};

	inline ICCSPos::ICCSPos(char x, int y)
	{
		setX(x);
		setY(y);
	}

	inline char ICCSPos::getX() const
	{
		return x;
	}

	inline int ICCSPos::getY() const
	{
		return y;
	}
	
	inline bool ICCSPos::equal(const ICCSPos& other) const
	{
		return (x == other.getX()) && (y == other.getY());
	}

	/*! \class ICCSMotion
	 * 
	 * \brief 描述一个棋子从棋盘上的一个位置移动到另外一个位置, 例如 'H2-E2'
	 */
	struct ICCSMotion {
		struct ICCSPos start;
		struct ICCSPos end;
		ICCSMotion();

		/**
		 * 从一个 iccsmotion 字符串创建
		 * @param str
		 */
		ICCSMotion(const std::string str);
	};

	/**
	 * 检查 iccsmotion 字符串是否有效, H2-E2 或者 h2-e2, h2e2 都是有效的
	 * @param str
	 * @return 
	 */
	bool iccsmotion_isvalid(const std::string& str);

	/**
	 * 检查 ICCSMotion 对象是否有效, 一个有效的对象应该包含正确的移动起始点和结束点
	 * 
	 * @param str
	 * @return 
	 */
	bool iccsmotion_isvalid(const ICCSMotion& motion);

	/**
	 * 将 iccsmotion 中的 y 坐标转换为 ChessBook 对象中的 y 索引值
	 * 
	 * \return return 0 on fail
	 */
	int iccsmotion_ypos4chessbook(int y);

	/**
	 * 将 iccsmotion 中的 x 坐标转换为 ChessBook 对象中的 x 索引值
	 * 
	 * \return return 0 on fail
	 */
	int iccsmotion_xpos4chessbook(char x);

	/**
	 * 根据 pos 的位置描述从棋盘对象中获得一个棋子
	 * 
	 * \return return NULL on fail
	 */
	const ChessPiece* iccsmotion_getpiece(const ChessBook& cb, const ICCSPos& pos);

	/**
	 * 将一个 iccsmotion 字符串转换为 ICCSMotion 对象
	 * 
	 * \return return NULL on fail
	 */
	struct ICCSMotion* iccsmotion_parse(ICCSMotion& motion, const std::string& str);
	
	/**
	 * 将一个 PieceMotion 转换为 ICCSMotion 对象
	 * 
	 * \return return pointer to ICCSMotion
	 */
	struct ICCSMotion* iccsmotion_parse(ICCSMotion& motion, const PieceMotion& pcemotion);
	
	/**
	 * 将一个 ICCSMotion 对象转换为 iccsmotion 字符串
	 * 
	 * \return 成功返回指向 str 的指针, 失败返回 NULL
	 */
	std::string* iccsmotion_tostring(std::string& str, const ICCSMotion& motion);

	/**
	 * 
	 * @param cb
	 * @param str
	 * @return 
	 */
	std::string iccsmotion_tostring(const ICCSMotion& motion);

	/**
	 * 应用 iccsmotion 字符串在 ChessBook 对象内移动一个棋子
	 * 
	 * \return 成功返回指向 ChessBook 对象的指针, 失败返回 NULL
	 */
	ChessBook* iccsmotion_apply(ChessBook& cb, const std::string& str);

	/**
	 * 应用 ICCSMotion 对象在 ChessBook 对象内移动一个棋子
	 * 
	 * \return 成功返回指向 ChessBook 对象的指针, 失败返回 NULL
	 */
	ChessBook* iccsmotion_apply(ChessBook& cb, const ICCSMotion& motion);

	inline ICCSMotion::ICCSMotion()
	{
	}

	inline ICCSMotion::ICCSMotion(const std::string str)
	{
		iccsmotion_parse(*this, str);
	}

	/** @} */ // end of iccsmotion

}

#endif
