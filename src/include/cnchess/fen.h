/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_FEN_H_
#define CNCS_FEN_H_	1

#include <string>
#include <istream>
#include <wchar.h>
#include "define.h"
#include "chessbook.h"

namespace cnchess {

	/*! \addtogroup chessbook
	 *  @{
	 */

	/**
	 * 解释 fen 字符串, 并填充到 cb 中
	 * 
	 * @param cb
	 * @param fen
	 * @return 成功返回指向 cb 的指针, 失败返回 NULL
	 */
	ChessBook* fen_parse(ChessBook& cb, const std::string& fen);

	/**
	 * 从文件流中读取并解释 fen 字符串, 并填充到 cb 中
	 * 
	 * @param cb
	 * @param fen
	 * @return 成功返回指向 cb 的指针, 失败返回 NULL
	 */
	ChessBook* fen_parse(ChessBook& cb, FILE* fp);

	/**
	 * 从打开的文件描述符中读取并解释 fen 字符串, 并填充到 cb 中
	 * 
	 * @param cb
	 * @param fen
	 * @return 成功返回指向 cb 的指针, 失败返回 NULL
	 */
	ChessBook* fen_parse(ChessBook& cb, int fd);

	/**
	 * 从流对象中读取并解释 fen 字符串, 并填充到 cb 中
	 * 
	 * @param cb
	 * @param fen
	 * @return 成功返回指向 cb 的指针, 失败返回 NULL
	 */
	ChessBook* fen_parse(ChessBook& cb, std::istream& stream);

	/**
	 * 解释 fen 字符串, 并返回一个堆中分配的 ChessBook 对象
	 * 
	 * @param cb
	 * @param fen
	 * @return 成功返回指向 cb 的指针, 失败返回 NULL
	 * 
	 * \note 你需要自行释放此对象
	 */
	ChessBook* fen_parse(const std::string& fen);
	ChessBook* fen_parse(FILE* fp);
	ChessBook* fen_parse(int fd);
	ChessBook* fen_parse(std::istream& stream);

	/**
	 * 将 ChessBook 转换为 fen 字符串
	 * 
	 * @param str
	 * @param cb
	 */
	std::string& fen_tostring(std::string& str, const ChessBook& cb);
	std::string fen_tostring(const ChessBook& cb);

	/**
	 * 将一个字符装换为一个 ChessPiece::Type
	 * 
	 * @param piece
	 * @return 失败返回 ChessPiece::Type::NONE
	 */
	ChessPiece::Type char2piece(char piece);

	/**
	 * 将字符转换为中文表示的棋子名称
	 * 
	 * @param piece
	 * @param person 指定红方还是黑方, 这个影响到返回值, 比如 'K' 在黑方的时候叫 '将', 红方叫 '帅'
	 * @return 
	 */
	wchar_t piece2chiness(char piece, GameRole person = RL_RED);

	bool is_piece(char piece);

	/** @} */ // end of fen

}

#endif
