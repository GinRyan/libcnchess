/* 
 * File:   refcounter.h
 * Author: alec
 *
 * Created on 2012年5月19日, 下午3:26
 */

#ifndef CNCS_REFCOUNTER_H
#define	CNCS_REFCOUNTER_H

#include "mutex.h"

namespace cnchess {

	/*! \addtogroup utility
	 *  @{
	 */

	/*! \class RefCounter
	 *  
	 * \brief 引用计数基类
	 * 
	 * 线程安全的引用计数, 你不能 delete 它, 只能加减它的引用计数
	 */
	class RefCounter {
	public:
		RefCounter();

		/**
		 * \brief 当前的引用数
		 * 
		 * @return 
		 */
		int refCount() const;

		/**
		 * \brief 增加引用计数
		 */
		void incref() const;

		/**
		 * \brief 减少引用计数
		 * 
		 * 当引用计数为0, 对象将被释放
		 */
		void decref() const;
	protected:
		virtual ~RefCounter();
	private:
		RefCounter(const RefCounter&);
		RefCounter & operator =(const RefCounter&);
	private:
		mutable int counter;
		mutable Mutex mutex;
	};

	inline RefCounter::RefCounter()
	{
		counter = 1;
	}

	inline RefCounter::~RefCounter()
	{
	}

	inline int RefCounter::refCount() const
	{
		return counter;
	}

	inline void RefCounter::incref() const
	{
		mutex.lock();
		counter += 1;
		mutex.unlock();
	}

	inline void RefCounter::decref() const
	{
		mutex.lock();
		counter -= 1;
		mutex.unlock();
		if (counter == 0) {
			delete this;
		}
	}

	/*! @} */

}

#endif	/* CNCS_REFCOUNTER_H */

