/* 
 * File:   stdreferee.h
 * Author: alec
 *
 * Created on 2012年5月24日, 上午11:38
 */

#ifndef CNCS_GAMEREFEREEBASIC_H
#define	CNCS_GAMEREFEREEBASIC_H

#include "gamereferee.h"

namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */

	/**
	 * \class STDReferee
	 * 
	 * \brief 标准中国象棋裁判, 包含基本的规则, 例如: 马只能走日, 象只能走田等
	 * 
	 * @param player
	 * @param motion
	 * @return 
	 */
	class STDReferee : public GameReferee {
	public:
		bool requestMove(const GamePlayer* player, const ICCSMotion& motion);
	};

	/*! @} */

}

#endif	/* CNCS_GAMEREFEREEBASIC_H */

