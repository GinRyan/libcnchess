/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_GAMETIMER_H
#define CNCS_GAMETIMER_H

#include <vector>
#include <assert.h>
#include "mutex.h"
#include "dispatcher.h"
#include "clock.h"
#include "alarm.h"
#include "refcounter.h"

namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */

	class GamePlayer;

	/*! \class TimerInfo
	 *  
	 * \brief 描述一个计时器的信息
	 */
	struct TimerInfo : public RefCounter {
		long total;
		long used;
		long prestep;

		/**
		 * 构造函数
		 * 
		 * @param total 总时间
		 * @param prestep 单步时间
		 * @param used 已用时间
		 */
		TimerInfo(long total, long prestep = 0, long used = 0);
	};

	inline TimerInfo::TimerInfo(long total, long prestep, long used)
	: total(total), used(used), prestep(prestep)
	{
	}

	/*! \class GameTimer
	 *  
	 * \brief 玩家计时器, 用于玩家在一盘游戏中的总计时
	 */
	class GameTimer : public RefCounter {
	public:

		/*! \brief 超时类型 */
		enum ExpiredType {
			EXPIRED_TIMER = EXPIRED, /*!< 所有时间用完 */
			EXPIRED_STEP /*!< 单步时间用完 */
		};

		/*! \brief 超时 listener
		 * 
		 * 签名为 void (const GameTimer* gtimer, void* ctx, ExpiredType exttype) 
		 */
		typedef Dispatcher<GameTimer, void*, ExpiredType>::Listener Listener;

		/**
		 * 创建计时器
		 * 
		 * @param seconds 总时间
		 * @param pre_step_time 每开始一次计时, player 的单步最大计时时间, 单位是秒, 0 为不限制
		 * @param clock	时钟, 本类会自动 delete 该 clock instance.
		 */
		GameTimer(long seconds, long pre_step_time = 0, IClock* clock = NULL);

		/**
		 * 创建计时器
		 * 
		 * @param timeri
		 */
		GameTimer(const TimerInfo& timeri);

		~GameTimer();

		/**
		 * 启动计时器
		 * 
		 * @return 成功返回 true
		 */
		bool start();

		/**
		 * 停止计时器
		 * 
		 * @return 成功返回 true
		 */
		bool press();

		/**
		 * 在运行中给定时器加时
		 * 
		 * @param seconds
		 */
		void overtime(long seconds);

		/**
		 * 当前剩余时间, 秒
		 * 
		 * @return 
		 */
		long remainTime() const;

		/**
		 * 取得总时间(秒)
		 * 
		 * @return 
		 */
		long totalTime() const;

		/**
		 * 取得单步最大时间(秒), 0 为无限制
		 * 
		 * @return 
		 */
		long preStepTime() const;

		/**
		 * 取得单步剩余时间(秒)
		 * 
		 * @return 如果单步时间不限制或者单步已经超时, 返回 -1
		 */
		long preStepRemainTime() const;

		/**
		 * 取得当前状态
		 * 
		 * @return ::RunStatus
		 */
		int getStatus() const;

		/**
		 * 添加超时 listener
		 * 
		 * @param listener
		 * @param ctx	上下文, 将作为第二个参数传递给 listener
		 */
		void addListener(Listener listener, void* ctx = NULL);

		/**
		 * 移除超时 listener
		 * 
		 * @param listener
		 */
		void removeListener(Listener listener);
	private:
		long totaltime;
		long remaintime;
		long pre_step_time;
		int status;
		IClock* clock;
		Alarm* alarm;
		Dispatcher<GameTimer, void*, ExpiredType> dispatcher;

		mutable Mutex mutex;

		GameTimer(const GameTimer &other);
		GameTimer& operator =(const GameTimer &other);
		void constructor(long seconds, long pre_step_time, IClock* clock);
		void setStatus(int newst);

		static void clockListener(const IClock* clock, void* ctx);
		static void alarmListener(const Alarm* alarm, void* ctx);

	};

	inline int GameTimer::getStatus() const
	{
		return status;
	}

	inline void GameTimer::setStatus(int newst)
	{
		mutex.lock();
		status = newst;
		mutex.unlock();
	}

	inline long GameTimer::remainTime() const
	{
		return(remaintime >= 0) ? remaintime : 0;
	}

	inline long GameTimer::totalTime() const
	{
		return totaltime;
	}

	inline long GameTimer::preStepTime() const
	{
		return pre_step_time;
	}

	inline void GameTimer::addListener(GameTimer::Listener listener, void* ctx)
	{
		dispatcher.add(listener, ctx);
	}

	inline void GameTimer::removeListener(GameTimer::Listener listener)
	{
		dispatcher.remove(listener);
	}

	/*! @} */

}

#endif // CNCS_GAMETIMER_H
