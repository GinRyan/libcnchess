/* 
 * File:   clock.h
 * Author: alec
 *
 * Created on 2012年5月18日, 上午11:09
 */

#ifndef CNCS_CLOCK_H
#define	CNCS_CLOCK_H

#include <ctime>
#include <vector>
#include <signal.h>
#include "define.h"
#include "mutex.h"
#include "dispatcher.h"
#include "refcounter.h"

namespace cnchess {

	/*! \addtogroup utility
	 *  @{
	 */

	/*! \class IClock
	 *  
	 * \brief 时钟接口, 用于给各种计时器和闹钟提供基本的时间步进服务
	 * 
	 * 一个时钟的定义是: 不停地走动, 每前进一步(例如一秒)就发出一次通知给 listener
	 */
	class IClock : public RefCounter {
	public:
		/*! 
		 * \brief 时钟每滴答一下, 就发送通知给 listener 
		 * 
		 * 签名为 void (const IClock* clock, void* ctx)
		 */
		typedef Dispatcher<IClock>::Listener Listener;

		/**
		 * \brief 取得时钟步进的长度, 也就是多久"滴答"一次
		 * 
		 * @return 
		 */
		virtual const struct timespec& getInterval() const = 0;

		/**
		 * \brief 取得时钟当前状态
		 * 
		 * @return 
		 */
		virtual RunStatus getStatus() const = 0;

		/**
		 * \brief 启动时钟
		 */
		virtual void start() = 0;

		/**
		 * \brief 停止时钟
		 */
		virtual void stop() = 0;

		virtual void addListener(Listener listener, void* ctx = NULL) = 0;
		virtual void removeListener(Listener listener) = 0;
	protected:
		typedef Dispatcher<IClock> ClockDispatcher;
		IClock();
		virtual ~IClock();
	};

	inline IClock::IClock()
	{
	}

	inline IClock::~IClock()
	{
	}

	/*! \class RealClock
	 *  
	 * \brief 实时时钟, 提供比较精确的时间
	 * 
	 * \note 本时钟利用系统定时器 + 实时信号(可能范围为 SIGRTMIN ~ SIGRTMAX)实现, 
	 * 所以应用程序应该尽量避免信号冲突
	 */
	class RealClock : public IClock {
	public:
		/**
		 * \brief 建立一个实时时钟
		 * 
		 * @param interval	步进为微秒
		 */
		static RealClock* create(long interval);

		/**
		 * \brief 建立一个实时时钟
		 * 
		 * @param ts	步进
		 */
		static RealClock* createFromTS(const struct timespec& ts);

		/**
		 * \brief 最大允许建立的时钟数
		 * 
		 * 这受限于系统的实时信号数量, 默认数量为 SIGRTMAX - SIGRTMIN + 1
		 * 
		 * @return 
		 */
		static int maxClock();

		/**
		 * \brief 可以建立的时钟数
		 * 
		 * @return 
		 */
		static int remainClock();
		~RealClock();

		RunStatus getStatus() const;
		const struct timespec& getInterval() const;

		/**
		 * 取得当前时钟使用的信号号
		 * 
		 * @return 
		 */
		int getSignalNo() const;

		void start();
		void stop();
		void addListener(IClock::Listener listener, void* ctx = NULL);
		void removeListener(IClock::Listener listener);
	private:
		static int curr_alloced_signo;

		struct timespec ts;
		timer_t timerid;
		RunStatus status;
		int signum;
		ClockDispatcher dispatcher;
		mutable Mutex mutex;

		RealClock(long interval, int signum);
		RealClock(const struct timespec& ts, int signum);
		RealClock(const RealClock& other);
		RealClock& operator =(const RealClock& other);
		void setStatus(RunStatus newst);
		static void signalHandler(int signum, siginfo_t* siginfo, void* ctx);
	};

	inline void RealClock::setStatus(RunStatus newst)
	{
		mutex.lock();
		status = newst;
		mutex.unlock();
	}

	inline RunStatus RealClock::getStatus() const
	{
		return status;
	}

	inline int RealClock::getSignalNo() const
	{
		return signum;
	}

	inline const struct timespec& RealClock::getInterval() const
	{
		return ts;
	}

	inline void RealClock::addListener(IClock::Listener listener, void* ctx)
	{
		dispatcher.add(listener, ctx);
	}

	inline void RealClock::removeListener(IClock::Listener listener)
	{
		dispatcher.remove(listener);
	}

	/*! @} */

}

#endif	/* CNCS_CLOCK_H */

