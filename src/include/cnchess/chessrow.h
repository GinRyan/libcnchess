/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_CHESSROW_H
#define CNCS_CHESSROW_H

#include "mutex.h"
#include "chesspiece.h"

namespace cnchess {

	/*! \addtogroup chessbook
	 *  @{
	 */

	/*! \class ChessRow
	 *  
	 * \brief 代表棋盘上的一行
	 */
	class ChessRow {
	public:

		/**
		 * \brief 棋子数(没有棋子的点也算)
		 * @return 
		 */
		static std::size_t size();

		/**
		 * \brief 创建空行
		 */
		ChessRow();
		ChessRow(const ChessRow& other);
		ChessRow& operator =(const ChessRow& other);
		~ChessRow();

		/**
		 * \brief 取得某个棋子
		 * 
		 * @param index 棋子索引, 从0开始
		 * @return 
		 */
		ChessPiece& operator[](int index);
		const ChessPiece& operator[](int index) const;

		/**
		 * \brief 清空整行的棋子
		 */
		void clear();

		/**
		 * \brief 检查两行是否相等
		 * 
		 * @param other
		 * @return 
		 */
		bool equal(const ChessRow& other) const;

		/**
		 * \brief 检查行是否为空, 也就是是否有棋子
		 * 
		 * @return 
		 */
		bool isEmpty() const;

	private:
		ChessPiece row[9];
		mutable Mutex* mutex;
	};

	inline std::size_t ChessRow::size()
	{
		return 9;
	}

	/*! @} */

}

#endif // CNCS_CHESSROW_H
