/* 
 * File:   actiontimer.h
 * Author: alec
 *
 * Created on 2012年5月30日, 上午10:01
 */

#ifndef CNCS_ACTIONTIMER_H
#define	CNCS_ACTIONTIMER_H

#include <assert.h>
#include "define.h"
#include "gametimer.h"


namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */

	/*! \class IActionTimer
	 *  
	 * \brief 代表玩家的 action 处理的计时器接口, 用于玩家的动作计时
	 * 
	 * \see GamePlayer
	 */
	class IActionTimer {
	public:

		virtual ~IActionTimer();

		/**
		 * \brief 取得剩余时间, 秒
		 * 
		 * @return 
		 */
		virtual long remainTime() const = 0;

		/**
		 * \brief 取得计时器总时间, 秒
		 * 
		 * @return 
		 */
		virtual long totalTime() const = 0;

		/**
		 * \brief 取得当前计时器状态
		 * 
		 * @return ::RunStatus
		 */
		virtual int getStatus() const = 0;

		/**
		 * \brief 启动计时器
		 */
		virtual void start() = 0;

		/**
		 * \brief 停止计时器
		 */
		virtual void press() = 0;

		/**
		 * \brief 是否已经超时
		 * 
		 * @return 
		 */
		virtual bool isExpired() const = 0;

	protected:
		IActionTimer();
	};

	inline IActionTimer::IActionTimer()
	{

	}

	inline IActionTimer::~IActionTimer()
	{

	}

	/*! \class DummyActionTimer
	 *  
	 * \brief 代表一个虚假的 action 处理的计时器
	 * 
	 * 本计时器实现了 IActionTimer 接口, 但实际上不计时, 也不超时
	 */
	class DummyActionTimer : public IActionTimer {
	public:
		long remainTime() const;
		long totalTime() const;
		int getStatus() const;
		void start();
		void press();
		bool isExpired() const;
	};

	inline long DummyActionTimer::remainTime() const
	{
		return 0;
	}

	inline long DummyActionTimer::totalTime() const
	{
		return 0;
	}

	inline int DummyActionTimer::getStatus() const
	{
		return IDLE;
	}

	inline void DummyActionTimer::start()
	{
		;
	}

	inline void DummyActionTimer::press()
	{
		;
	}

	inline bool DummyActionTimer::isExpired() const
	{
		return false;
	}

	/*! \class ActionTimer
	 *  
	 * \brief 默认的 action 处理的计时器, 用于玩家的动作计时
	 */
	class ActionTimer : public IActionTimer {
	public:
		ActionTimer(GameTimer* gtimer);
		~ActionTimer();
		long remainTime() const;
		long totalTime() const;
		int getStatus() const;
		void start();
		void press();
		bool isExpired() const;
	private:
		GameTimer* gtimer;
		ActionTimer(const ActionTimer&);
		ActionTimer& operator=(const ActionTimer&);
	};

	inline ActionTimer::ActionTimer(GameTimer* gtimer) : gtimer(gtimer)
	{
		assert(gtimer);

		this->gtimer->incref();
	}

	inline ActionTimer::~ActionTimer()
	{
		gtimer->decref();
	}

	inline long ActionTimer::remainTime() const
	{
		return gtimer->preStepRemainTime();
	}

	inline long ActionTimer::totalTime() const
	{
		return gtimer->preStepTime();
	}

	inline void ActionTimer::start()
	{
		gtimer->start();
	}

	inline void ActionTimer::press()
	{
		gtimer->press();
	}

	inline bool ActionTimer::isExpired() const
	{
		return getStatus() == EXPIRED;
	}

	/*! @} */

}

#endif	/* CNCS_ACTIONTIMER_H */

