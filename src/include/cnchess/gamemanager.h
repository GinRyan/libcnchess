/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_GAMEMANAGER_H
#define CNCS_GAMEMANAGER_H


#include <assert.h>
#include <deque>
#include "define.h"
#include "iccsmotion.h"
#include "gameplayer.h"
#include "gamereferee.h"
#include "coordinator.h"

namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */

	/**
	 * \class OvertimeHandle
	 * \brief 用于 GameManager 的超时控制
	 * 
	 * 当有玩家超时或者单步超时, 你可以通过本数据结构控制 GameManager 是否为玩家加时
	 */
	struct OvertimeHandle {

		/*! \brief 处理类型 */
		enum {
			OVERTIME, /*!< 加时 */
			GAMEOVER /*!< 停止游戏 */
		};

		/**
		 * 处理类型
		 */
		int handle;

		/**
		 * 加时秒数
		 */
		int overtime;
	};

	/**
	 * \class GameManager
	 * 
	 * \brief 游戏管理者, 用于游戏流程控制
	 */
	class GameManager {
	public:

		/**
		 * \brief 请求加时回调
		 */
		typedef void (*ExpiredHandler)(OvertimeHandle& handle, void* ctx, const Game* game,
			const GamePlayer* player, GameTimer::ExpiredType exptype);

		/**
		 * 创建
		 * 
		 * @param game Game 对象, 该对象在 GameManager 销毁时自动释放
		 * @param timeri 玩家定时器, 如果该值为空, 则使用 Game 对象内指定的定时器
		 * @param coordinator 
		 */
		GameManager(Game* game, TimerInfo* timeri = NULL, Coordinator* coordinator = NULL);

		~GameManager();

		/**
		 * 取得当前状态
		 * 
		 * @return ::RunStatus
		 */
		int getStatus() const;

		/**
		 * 取得 Game 对象
		 * 
		 * @return 
		 */
		const Game* getGame() const;

		/**
		 * 取得 Coordinator 对象
		 * 
		 * @return 
		 */
		const Coordinator* getCoordinator() const;

		/**
		 * 启动游戏
		 * 
		 * 本函数一旦调用, GameManager 就会自己进入循环, 直到游戏结束才返回游戏状态
		 * 
		 * @return 
		 */
		Game::Status start();

		/**
		 * 停止游戏
		 */
		void stop();

		/**
		 * 设置超时回调
		 * 
		 * 如果游戏设置了时间限制, 那么 handler 在用户超时或者单步超时的时候会被调用, 
		 * 你可以在 handler 中通过设置 OvertimeHandle 来告诉 GameManager 如何处理, 
		 * 例如加时或者直接结束游戏
		 * 
		 * @param handler
		 * @param ctx 上下文, 将作为调用 handler 时的第二个参数
		 */
		void setExpiredHandler(ExpiredHandler handler, void* ctx = NULL);

	private:
		typedef std::vector<GameAction> ActionHistory;

		struct ExpiredHandlerObject {
			ExpiredHandler handler;
			void* ctx;
		};

		int status;
		Game* game;
		GameReferee* referee;
		GameTimer* gtimer_red;
		GameTimer* gtimer_black;
		ExpiredHandlerObject exphandler_obj;
		Coordinator* coordinator;
		mutable Mutex mutex;
		int exitFlag;	//myx:
		void setStatus(int st);

		/**
		 * check timer for action
		 * 
		 * 发送请求后, 如果对手处理请求超时, 并且如果系统延长了计时器时间, 那么认为对手同意请求, 否则认为不同意
		 * 
		 * @param player
		 * @return 
		 */
		bool handleExpired(const GamePlayer* player);
		void setOver(Game::Status st);

		static void actionListener(const Coordinator* coordinator, void* ctx, const GamePlayer* player,
			const GameAction& ac, Coordinator::Result res);
		static void timerListener(const GameTimer* timer, void* ctx,
			GameTimer::ExpiredType exptype);
	};

	inline const Game* GameManager::getGame() const
	{
		return game;
	}

	inline const Coordinator* GameManager::getCoordinator() const
	{
		return coordinator;
	}

	inline int GameManager::getStatus() const
	{
		return status;
	}

	inline void GameManager::setStatus(int st)
	{
		mutex.lock();
		status = st;
		mutex.unlock();
	}

	/*! @} */

}

#endif // CNCS_GAMEMANAGER_H
