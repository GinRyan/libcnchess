/* 
 * File:   ucciplayer.h
 * Author: alec
 *
 * Created on 2012年6月18日, 上午9:37
 */

#ifndef CNCS_UCCIPLAYER_H
#define	CNCS_UCCIPLAYER_H

#include <vector>
#include <string>
#include <map>
#include "gameplayer.h"
#include "ucciengine.h"

namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */

	/**
	 * \class UCCIPlayer
	 * \brief UCCI 引擎 AI "玩家" 支持
	 */
	class UCCIPlayer : public GamePlayer {
	public:
		
		enum Degree {
			EASY,
			MIDDLE,
			HARD
		};
		
		/**
		 * constructor
		 * 
		 * @param uccieg UCCI 引擎, 如果未启动, 则 UCCIPlayer 会在需要的时候自动启动, 
		 * 本参数会在 UCCIPlayer 结束的时候自动释放
		 * @param degree 难度等级
		 */
		UCCIPlayer(UCCIEngine* uccieg, Degree degree = EASY);
		~UCCIPlayer();
		UCCIEngine* getEngine();
		const UCCIEngine* getEngine() const;
		void onGameStart(const Game* game, GameRole myrole);
		void onGameOver(Game::Status status);
		int requestHandle(const GameAction& ac, Coordinator& coordinator,
			IActionTimer& timer);
	private:
		UCCIEngine* uccieg;

		std::string prev_bestmove;
		std::string prev_ponder;
		int round;
		Degree degree;
		int depth;
#if 0
		ChessBook startup_cb;
		std::string firststep;
#endif
		int handleMoveRequest(const GameAction& ac, Coordinator& coordinator,
			IActionTimer& timer);
	};

	inline UCCIEngine* UCCIPlayer::getEngine()
	{
		return uccieg;
	}
	
	inline const UCCIEngine* UCCIPlayer::getEngine() const
	{
		return uccieg;
	}
		
	/*! @} */

}

#endif	/* CNCS_UCCIPLAYER_H */

