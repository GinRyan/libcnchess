/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_GAMEPLAYER_H_
#define CNCS_GAMEPLAYER_H_	1


#include "define.h"
#include "refcounter.h"
#include "actiontimer.h"
#include "coordinator.h"

namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */

	class Game;

	/*! \class GamePlayer
	 *  
	 * \brief 代表一个游戏玩家的基类
	 * 
	 * 子类需实现 GamePlayer::requestHandle() 来与对手和 GameManager 互动
	 * 
	 * \see GameManager
	 */
	class GamePlayer : public RefCounter {
	public:

		/*! \brief 动作处理结果 */
		enum RequestResult {
			IGNORED = 0, /*!< 忽略/拒绝 */
			HANDLED = 1, /*!< OK */
			ERROR /*!< 错误 */
		};

		/**
		 * 玩家名字
		 * 
		 * @return 空字符串
		 */
		virtual std::string getName() const;

		/**
		 * 玩家描述
		 * 
		 * @return 空字符串
		 */
		virtual std::string getDescripting() const;

		/**
		 * 游戏开始时, 本函数被 GameManager 调用
		 * 
		 * @param game
		 * @param myrole 玩家在游戏中的角色
		 * 
		 * \note 如果你重载了本函数, 那么你的函数里应调用父函数
		 */
		virtual void onGameStart(const Game* game, GameRole myrole);

		/**
		 * 游戏结束时, 本函数被 GameManager 调用
		 * 
		 * @param status 游戏状态
		 * 
		 * \note 如果你重载了本函数, 那么你的函数里应调用父函数
		 */
		virtual void onGameOver(Game::Status status);

		/**
		 * 取得代表当前游戏的 Game 对象
		 * 
		 * @return 
		 */
		const Game* getGame() const;

		/**
		 * 取得我在游戏中的角色
		 * 
		 * @return 
		 */
		GameRole getMyRole() const;

		/**
		 * 处理来自 GameManager 和对手的动作请求
		 * 
		 * 本函数会在以下几种情形被调用:
		 *  - 当 GameManager 要求你下棋
		 *  - 对手求和, 认输, 请求悔棋
		 * 
		 * 本函数实现建议的处理流程为:
		 *  - 启动计时器
		 *  - 处理请求
		 *  - 停止计时器
		 * 
		 * @param ac 动作描述
		 * @param coordinator 协调员
		 * @param timer 玩家处理动作的时间计时器
		 * @return #RequestResult
		 */
		virtual int requestHandle(const GameAction& ac, Coordinator& coordinator,
			IActionTimer& timer) = 0;
	protected:
		GamePlayer();
		virtual ~GamePlayer();
	private:
		std::string name;
		std::string description;
		const Game* game;
		GameRole myrole;
		GamePlayer(const GamePlayer& other);
		GamePlayer& operator=(const GamePlayer& other);
	};

	inline GamePlayer::GamePlayer() : game(NULL), myrole(RL_NONE)
	{

	}

	inline GamePlayer::~GamePlayer()
	{

	}

	inline std::string GamePlayer::getName() const
	{
		return name;
	}

	inline std::string GamePlayer::getDescripting() const
	{
		return description;
	}

	inline void GamePlayer::onGameStart(const Game *game, GameRole myrole)
	{
		this->game = game;
		this->myrole = myrole;
	}

	inline void GamePlayer::onGameOver(Game::Status __attribute__((unused)) status)
	{
		;
	}

	inline const Game* GamePlayer::getGame() const
	{
		return game;
	}

	inline GameRole GamePlayer::getMyRole() const
	{
		return myrole;
	}

	/*! @} */

}

#endif
