/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_MUTEX_H
#define CNCS_MUTEX_H

#include <pthread.h>
#include <stdexcept>
#include <cstring>


namespace cnchess {

	/*! \addtogroup utility
	 *  @{
	 */

	/*! \class Mutex
	 *  
	 * \brief 线程互斥锁
	 */
	class Mutex {
	public:
		Mutex();
		~Mutex();

		/**
		 * \brief 加锁
		 */
		void lock();

		/**
		 * \brief 解锁
		 */
		void unlock();

		/**
		 * \brief 尝试加锁, 失败返回 false
		 * 
		 * @return 
		 */
		bool tryLock();
	private:
		pthread_mutex_t mutex;
		Mutex(const Mutex& rhs);
		Mutex& operator=(const Mutex& rhs);
	};

	inline Mutex::Mutex()
	{
		pthread_mutex_init(&mutex, NULL);
	}

	inline Mutex::~Mutex()
	{
		pthread_mutex_destroy(&mutex);
	}

	inline void Mutex::lock()
	{
		int res = pthread_mutex_lock(&mutex);
		if (0 != res) {
			throw std::runtime_error(strerror(res));
		}
	}

	inline void Mutex::unlock()
	{
		int res = pthread_mutex_unlock(&mutex);
		if (0 != res) {
			throw std::runtime_error(strerror(res));
		}
	}

	inline bool Mutex::tryLock()
	{
		return pthread_mutex_trylock(&mutex) == 0;
	}

	/*! @} */

}

#endif // CNCS_MUTEX_H
