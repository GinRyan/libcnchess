/* 
 * File:   ucciutil.h
 * Author: alec
 *
 * Created on 2012年7月4日, 上午11:04
 */

#ifndef CNCS_UCCIUTIL_H
#define	CNCS_UCCIUTIL_H

#include <string>
#include <vector>
#include "ucciengine.h"
#include "iccsmotion.h"

namespace cnchess {

	/*! \addtogroup game
	 *  @{
	 */
	
	typedef std::vector<std::string> StringList;

	/**
	 * 让 UCCI 引擎为玩家推荐一步
	 * 
     * @param bestmove
     * @param eg UCCI 引擎， 必须已经启动
     * @param cb 开局
     * @param myrole 玩家的角色（红方， 黑方）
     * @param move_history 棋局的棋步历史
     * @param depth UCCI 引擎的搜索深度， 深度越大得到推荐结果的时间越慢
     * @return 成功返回指向 `bestmove` 的指针， 失败或者无推荐返回 NULL
	 * 
	 * \note UCCI 引擎是不支持多线程的， 如果 UCCI 引擎当前处于忙碌状态（被另一线程使用中）
     */
	ICCSMotion* ucci_recommend(ICCSMotion& bestmove, UCCIEngine& eg, const ChessBook& cb,
			GameRole myrole = RL_RED, StringList* move_history = NULL, int depth = 5);

	std::string ucci_reqbuild_postion(const ChessBook& cb, GameRole myrole = RL_RED,
			StringList* move_history = NULL, int round = -1);

	/*! @} */
	
}

#endif	/* CNCS_UCCIUTIL_H */

