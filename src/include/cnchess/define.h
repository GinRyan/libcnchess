/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#ifndef CNCS_DEFINE_H_
#define CNCS_DEFINE_H_	1

namespace cnchess {

	/*! \addtogroup global
	 *  @{
	 */

	/*! \brief 玩家角色 */
	enum GameRole {
		RL_RED = 'r', /*!< 红方 */
		RL_BLACK = 'b', /*!< 黑方 */
		RL_NONE = '\0' /*!< 未定义 */
	};

	/*! \brief 红/黑定义 */
	enum {
		RED = RL_RED,
		BLACK = RL_BLACK
	};

	/*! \brief 代表通用的运行时状态表示 */
	enum RunStatus {
		IDLE = 10, /*!< 空闲/未运行 */
		STARTED = 20, /*!< 启动/运行中 */
		STOPPED = 30, /*!< 停止 */
		PAUSED = 40, /*!< 暂停 */
		EXPIRED = 50, /*!< 超过期限 */
		BUSY = 60 /*!< 忙, 无法响应 */
	};

	/*! \brief 棋盘长宽 */
	enum {
		CHESSBOOK_COL = 9,
		CHESSBOOK_ROW = 10
	};

	/*! @} */

}

#endif
