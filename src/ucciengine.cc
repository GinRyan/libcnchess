#include <stdexcept>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/select.h>

#ifdef ANDROID
#include <asm/signal.h>
#include <asm/fcntl.h>
#include <fcntl.h>
#include "androidai.h"
#else
#include <sys/signal.h>
#include <sys/fcntl.h>

#endif
#include <sys/prctl.h>
#include <string.h>
#include <errno.h>
#include "ucciengine.h"
#include "utility.h"
#include "define.h"


#define CNCS_UCCI_LINEEND		'\n'
#define CNCS_UCCI_WAIT4START	100 // milliseconds

using namespace std;

namespace cnchess {
#ifdef ANDROID
AndroidAIEnging* android_ai = NULL;
#endif
	
	
	UCCIEngine::UCCIEngine(const std::string& ucci_engine) :
	ucci_engine(ucci_engine), cpid(-1), status(IDLE)
	{

	}

	UCCIEngine::UCCIEngine(const std::string& ucci_engine,
			const StringList& argv) :
	ucci_engine(ucci_engine), ucci_engine_argv(argv), cpid(-1), status(IDLE)
	{

	}

	UCCIEngine::~UCCIEngine()
	{
		stop();
	}

	void UCCIEngine::onParentProcExit(int signo)
	{
		pdebug("got signal %d, UCCI engine exist", signo);
	
		kill(getpid(), SIGTERM);
		waitpid(getpid(), NULL, 0);
	}

	const std::string& UCCIEngine::getEngine() const
	{
		return ucci_engine;
	}

	void UCCIEngine::setStatus(int st)
	{
		status = st;
	}

	int UCCIEngine::getStatus() const
	{
		if (status == STARTED) {
			if (engineIsRunning()) {
				return status;
			} else {
				return STOPPED;
			}
		} else {
			return status;
		}
	}

	bool UCCIEngine::openPipe()
	{
		if ((-1 == pipe(ucci_eg_rpipe)) || (-1 == pipe(ucci_eg_wpipe))) {
			return false;
		}
		//		if (-1 == fcntl(ucci_eg_rpipe[0], F_SETFL, O_NONBLOCK)) {
		//			closePipe();
		//			pdebug("fcntl fail, %s", strerror(errno));
		//			return false;
		//		}

		return true;
	}
	
	bool UCCIEngine::start()
	{
		pdebug("UCCIEngine::start '%s '", this->ucci_engine.c_str());
		if (getStatus() == STARTED) {
			return true;
		}
#ifdef ANDROID
		android_ai = new AndroidAIEnging();
		if (!ucciStart()) {
			pdebug("UCCIEngine::start failed'");
			return false;
		}
		pdebug("UCCIEngine::start  ok '");
		setStatus(STARTED);
		return true;
#endif
		if (!is_executable(this->ucci_engine)) {
			pdebug("file not executable '%s'", this->ucci_engine.c_str());
			return false;
		}
		if (!openPipe()) {
			pdebug("%s", strerror(errno));
			return false;
		}

		pdebug("try to start UCCI engine '%s'", ucci_engine.c_str());

		pid_t pid = fork();
		if (pid == -1) {
			pdebug("fork fail: %s", strerror(errno));
			closePipe();
			return false;
		} else if (pid == 0) {
			// create new process group
			//			setpgid(0, 0);
			//			// set process name
			//			
			//			//prctl(PR_SET_PDEATHSIG, SIGTERM);
			//			// in child process
			//			pid_t ccpid = fork();
			//			if (ccpid == -1) {
			//				pdebug("fork fail: %s", strerror(errno));
			//				closePipe();
			//				_exit(0);
			//			} else if (ccpid == 0) {
			// child proc
			signal(SIGUSR1, onParentProcExit);
#ifdef ANDROID
			prctl(PR_SET_PDEATHSIG, SIGUSR1, NULL, NULL, NULL);
#else
			prctl(PR_SET_PDEATHSIG, SIGUSR1);
#endif
			// create new process group
			setpgid(0, 0);
			close(STDIN_FILENO);
			close(STDOUT_FILENO);
			close(STDERR_FILENO);
			close(ucci_eg_wpipe[1]);
			close(ucci_eg_rpipe[0]);
			if ((dup2(ucci_eg_wpipe[0], STDIN_FILENO) == -1) ||
					(dup2(ucci_eg_rpipe[1], STDOUT_FILENO) == -1) ||
					(dup2(ucci_eg_rpipe[1], STDERR_FILENO) == -1)) {
				closePipe();
				_exit(1);
			}
			close(ucci_eg_wpipe[0]);
			close(ucci_eg_rpipe[1]);

			char** argv = NULL;
			if (ucci_engine_argv.size()) {
				argv = new char*[ucci_engine_argv.size() + 1];
				for (int i = 0; i < ucci_engine_argv.size(); i++) {
					argv[i] = (char*) (ucci_engine_argv[i].c_str());
				}
				argv[ucci_engine_argv.size()] = NULL;
			}

			// 等待一下父进程安装信号控制函数
			usleep(1000 * 20);

			//kill(getppid(), CNCS_UCCI_SIGNOTIFY);
			char buf = 1;
			write(STDOUT_FILENO, &buf, 1);
			//creatTestFile("/data/data/cn.toltech.game.chinachess/elee0.txt");
			if (-1 == execv(ucci_engine.c_str(), argv)) {
				//creatTestFile("/data/data/cn.toltech.game.chinachess/elee1.txt");
				pdebug("start UCCI engine exec failed");
				closePipe();
				//_exit(1);
			}
			//			} else {
			//				// parent proc 
			//				waitpid(ccpid, NULL, 0);
			//				_exit(0);
			//			}
			//creatTestFile("/data/data/cn.toltech.game.chinachess/elee2.txt");
		} else {
			close(ucci_eg_wpipe[0]);
			close(ucci_eg_rpipe[1]);
			// parent process
			cpid = pid;

			struct timeval tv;
			tv.tv_sec = 3;
			tv.tv_usec = 0;
			if (!wait4io(ucci_eg_rpipe[0], IO_READABLE, &tv)) {
				kill(cpid, SIGTERM);
				waitpid(cpid, NULL, 0);
				cpid = -1;
				return false;
			}

			// 等待引擎启动成功
			sleepfor(CNCS_UCCI_WAIT4START);

			signal(SIGPIPE, SIG_IGN);

			if (!ucciStart()) {
				killChildProcess();
				return false;
			}

			setStatus(STARTED);

			return true;
		}
	}

	void UCCIEngine::stop()
	{
		if (getStatus() == STARTED) {
			ucciStop();
			killChildProcess();
			closePipe();
			setStatus(STOPPED);
		}
	}

	void UCCIEngine::killChildProcess()
	{
#ifdef ANDROID
		return;
#endif
		if (cpid > 0) {
			kill(cpid, SIGTERM);
			waitpid(cpid, NULL, 0);
			cpid = -1;
		}
	}

	void UCCIEngine::closePipe()
	{
#ifdef ANDROID
		return;
#endif
		close(ucci_eg_wpipe[0]);
		close(ucci_eg_rpipe[0]);
		close(ucci_eg_wpipe[1]);
		close(ucci_eg_rpipe[1]);
	}

	bool UCCIEngine::ucciStart()
	{
		if (-1 == writeToEngine("ucci")) {
			return false;
		}
		struct timeval tv;
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		while (true) {
			string line;
			if (-1 == readFromEngine(line, tv)) {
				pdebug("read response fail.");
				return false;
			}
			if (line == "ucciok") {
				break;
			}
		}

		return true;
	}

	void UCCIEngine::ucciStop()
	{
		writeToEngine("quit");
#ifdef ANDROID
		android_ai->destory();
		return;
#endif
	}

	bool UCCIEngine::ready()
	{
		if (getStatus() != STARTED) {
			return false;
		}

		if (-1 == writeToEngine("isready")) {
			return false;
		}
		string resp;
		struct timeval tv;
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		if (-1 == readFromEngine(resp, tv)) {
			return false;
		} else {
			return resp == "readyok";
		}
	}

	bool UCCIEngine::sendRequest(const std::string& req)
	{
		if ((getStatus() != STARTED)) {
			pdebug("request fail: engine not started");
			return false;
		}

		if (!ready()) {
			pdebug("request fail: engine not ready");
			return false;
		}
		return writeToEngine(req) != -1;
	}

	std::string* UCCIEngine::getResponse(std::string& resp)
	{
		if (getStatus() != STARTED) {
			return NULL;
		}
		if (-1 == readFromEngine(resp)) {
			return NULL;
		} else {
			return &resp;
		}
	}

	std::string* UCCIEngine::getResponse(std::string& resp, const timeval& timeo)
	{
		if (waitResponse(timeo)) {
			return getResponse(resp);
		} else {
			return NULL;
		}
	}

	bool UCCIEngine::waitResponse(const timeval& timeo)
	{
		if (getStatus() != STARTED) {
			return -1;
		}
		return wait4io(ucci_eg_rpipe[0], IO_READABLE, &timeo);
	}

	bool UCCIEngine::engineIsRunning() const
	{
#ifdef ANDROID
	return true;
#endif
		if (cpid > 0) {
			if (-1 != kill(cpid, 0)) {
				return true;
			}
		}
		return false;
	}

	ssize_t UCCIEngine::writeToEngine(const std::string& req)
	{
		pdebug("send request to engine: %s", req.c_str());
#ifdef ANDROID
		android_ai->writeToEngine(req);
		return req.size() + 1;
#endif
		struct timeval tv = {0, 0};
		int wfd = ucci_eg_wpipe[1];
		if (!wait4io(wfd, IO_WRITABLE, &tv)) {
			pdebug("write request fail, fd not writable");
			return -1;
		}
		static char line_end = CNCS_UCCI_LINEEND;
		if (-1 == write(wfd, req.c_str(), req.size())) {
			pdebug("write request fail, %s", strerror(errno));
			return -1;
		} else if (-1 == write(wfd, &line_end, 1)) {
			pdebug("write request fail, %s", strerror(errno));
			return -1;
		}
		return req.size() + 1;
	}

	ssize_t UCCIEngine::readFromEngine(std::string& resp)
	{
#ifdef ANDROID
		resp = android_ai->readFromEngine();
		return resp.size();
#endif
		resp.clear();
		int rfd = ucci_eg_rpipe[0];
		char buf;
		while (1) {
			ssize_t r = read(rfd, &buf, 1);

			if (r == -1 && errno == EAGAIN) {
				continue; //return -1;
			} else if (r > 0) {
				if (buf == CNCS_UCCI_LINEEND) {
					pdebug("responde from engine: %s", resp.c_str());
					return resp.size();
				} else {
					resp.append(1, buf);
				}
			} else {
				pdebug("engine pipe closed");
				return -1;
			}
		}
	}

	ssize_t UCCIEngine::readFromEngine(std::string& resp, const timeval& timeo)
	{
		if (wait4io(ucci_eg_rpipe[0], IO_READABLE, &timeo)) {
			return readFromEngine(resp);
		} else {
			pdebug("wait for read timeout");
			return -1;
		}
	}
}
