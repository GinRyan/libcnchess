#include <stdexcept>
#include "utility.h"
#include "piecepos.h"
#include "chessrow.h"
#include "chessbook.h"


using namespace std;

namespace cnchess {

	void PiecePos::setX(int x)
	{
		if (x < 0 || x >= ChessRow::size()) {
			throw invalid_argument(format("x '%d' invalid", x));
		}
		this->x = x;
	}

	void PiecePos::setY(int y)
	{
		if (y < 0 || y >= ChessBook::size()) {
			throw invalid_argument(format("y '%d' invalid", y));
		}
		this->y = y;
	}

	bool PiecePos::inrange(const PiecePosRange& range) const
	{
		for (int i = 0; i < range.size(); i++) {
			if (x == range[i].getX() && y == range[i].getY()) {
				return true;
			}
		}
		return false;
	}

	PieceMotion::PieceMotion(const PiecePos& start, const PiecePos& end) :
	start(start), end(end)
	{
		if (start.equal(end)) {
			throw invalid_argument(format("start(x: %d, y: %d) or end(x: %d, y: %d) invalid.",
				start.getX(), start.getY(), end.getX(), end.getY()));
		}
	}

}
