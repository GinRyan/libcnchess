/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#include <cstdio>
#include <ctype.h>
#include <cstdlib>
#include <time.h>
#include <cstring>
#include <cerrno>
#include <algorithm>
#include <stdexcept>
#include <functional>
#include <sstream>
#include <sys/stat.h>
#include "config.h"
#include "utility.h"
#ifdef ANDROID
#include <android/log.h>
#include <stdio.h>
#include <jni.h>
#endif

using namespace std;

static bool is_space(char c)
{
	return std::isspace(c);
}

bool is_executable(const std::string& file)
{
	struct stat file_st;
	if (-1 == stat(file.c_str(), &file_st)) {
		return false;
	}
	if ((S_ISREG(file_st.st_mode) || S_ISLNK(file_st.st_mode)) &&
		(access(file.c_str(), F_OK | X_OK) == 0)) {
		return true;
	}
	return false;
}

string format(const string& str, ...)
{
	va_list arg_list;
	va_start(arg_list, str);
	string result = vformat(str, arg_list);
	va_end(arg_list);
	return result;
}

string vformat(const string& str, va_list arg_list)
{
	va_list arg_list1;
	va_copy(arg_list1, arg_list);
	int bufsize = vsnprintf(NULL, 0, str.c_str(), arg_list) + 2;
	char* buf = new char[bufsize];
	vsnprintf(buf, bufsize, str.c_str(), arg_list1);
	string result(buf);
	delete[] buf;
	va_end(arg_list1);
	return result;
}

string& ltrim(std::string& dest, const string& str)
{
	dest = str;
	string::iterator p = find_if(dest.begin(), dest.end(), not1(ptr_fun(is_space)));
	dest.erase(dest.begin(), p);
	return dest;
}

string& rtrim(std::string& dest, const string& str)
{
	dest = str;
	string::reverse_iterator p = find_if(dest.rbegin(), dest.rend(), not1(ptr_fun(is_space)));
	dest.erase(p.base(), dest.end());
	return dest;
}

string& trim(std::string& dest, const string& str)
{
	return ltrim(dest, rtrim(dest, str));
}

StringList& split(StringList& dest, const string& str)
{
	return split(dest, str, ' ');
}

StringList& split(StringList& elems, const string& str, char delim)
{
	if (str.empty()) {
		elems.push_back(str);
		return elems;
	}
	stringstream ss(str);
	string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	if (str[str.size() - 1] == delim) {
		elems.push_back("");
	}
	return elems;
}

string join(const string& str, const StringList& slist)
{
	string s;
	StringList::const_iterator last_elem = slist.end();
	last_elem--;
	for (StringList::const_iterator it = slist.begin(); it != slist.end(); ++it) {
		s.append(*it);
		if (it != last_elem) {
			s.append(str);
		}
	}
	return s;
}

std::string asstring(unsigned int number)
{
	char buffer[64];
	std::sprintf(buffer, "%u", number);
	return buffer;
}

std::string asstring(int number)
{
	char buffer[64];
	std::sprintf(buffer, "%d", number);
	return buffer;
}

std::string asstring(long number)
{
	char buffer[64];
	std::sprintf(buffer, "%ld", number);
	return buffer;
}

std::string asstring(unsigned long number)
{
	char buffer[64];
	std::sprintf(buffer, "%lu", number);
	return buffer;
}

std::string asstring(double number)
{
	char buffer[64];
	std::sprintf(buffer, "%.*g", 16, number);
	return buffer;
}

std::string asstring(float number)
{
	return asstring((double) number);
}

std::string ashexstring(int number)
{
	char buffer[64];
	std::sprintf(buffer, "%X", number);
	return buffer;
}

std::string ashexstring(unsigned int number)
{
	return ashexstring((int) number);
}

std::string ashexstring(long number)
{
	char buffer[64];
	std::sprintf(buffer, "%lX", number);
	return buffer;
}

std::string ashexstring(unsigned long number)
{
	return ashexstring((long) number);
}

std::string asupper(const std::string& str)
{
	string res;
	for (size_t i = 0; i < str.size(); i ++) {
		res.append(1, toupper(str[i]));
	}
	return res;
}

std::string aslower(const std::string& str)
{
	string res;
	for (size_t i = 0; i < str.size(); i ++) {
		res.append(1, tolower(str[i]));
	}
	return res;
}

int ctoi(char c)
{
	char buf[2] = {c, '\0'};
	return atoi(buf);
}

void sleepfor(int millseconds)
{
	struct timespec slptm;
	slptm.tv_sec = (long) (millseconds / 1000);
	slptm.tv_nsec = (millseconds * 1000000) % 1000000000L;
	// @FIXME: slptm.tv_nsec 有时候会得到负数, 很费解
	if (slptm.tv_nsec < 0) {
		slptm.tv_nsec = 0;
	}
	struct timespec rem;
	while (-1 == nanosleep(&slptm, &rem)) {
		if (errno == EINTR) {
			slptm.tv_sec = rem.tv_sec;
			slptm.tv_nsec = rem.tv_nsec;
		} else {
			pdebug("sleepfor: %s", strerror(errno));
			return;
		}
	}
}
#if 0
int wait4signal(int signo, const struct timespec& timeout)
{
	sigset_t mask;
	sigset_t oldmask;
	sigemptyset(&mask);
	sigaddset(&mask, signo);

	if (sigprocmask(SIG_BLOCK, &mask, &oldmask) < 0) {
		pdebug("sigprocmask fail, %s", strerror(errno));
		return -1;
	}

	do {
		if (sigtimedwait(&mask, NULL, &timeout) < 0) {
			if (errno == EINTR) {
				continue;
			} else if (errno == EAGAIN) {
				if (sigprocmask(SIG_SETMASK, &oldmask, NULL) < 0) {
					throw runtime_error(strerror(errno));
				}
				pdebug("sigtimedwait fail, %s", strerror(errno));
				return 1;
			} else {
				if (sigprocmask(SIG_SETMASK, &oldmask, NULL) < 0) {
					throw runtime_error(strerror(errno));
				}
				pdebug("sigtimedwait fail, %s", strerror(errno));
				return -1;
			}
		}
		break;
	} while (true);

	if (sigprocmask(SIG_SETMASK, &oldmask, NULL) < 0) {
		throw runtime_error(strerror(errno));
	}
	return 0;
}

#endif
bool wait4io(int fd, enum IOStatus iost, const timeval* timeo)
{
#ifdef ANDROID
	return true;
#endif
	fd_set set;
	int val;
	FD_ZERO(&set);
	FD_SET(fd, &set);
	struct timeval* ptv = NULL;
	struct timeval tv;
	if (timeo) {
		tv.tv_sec = timeo->tv_sec;
		tv.tv_usec = timeo->tv_usec;
		ptv = &tv;
	}

	if (iost == IO_READABLE) {
		val = select(fd + 1, &set, NULL, NULL, ptv);
	} else if (iost == IO_WRITABLE) {
		val = select(fd + 1, NULL, &set, NULL, ptv);
	} else {
		val = select(fd + 1, NULL, NULL, &set, ptv);
	}
	return(val > 0 && FD_ISSET(fd, &set) > 0);
}

#ifdef ANDROID
void androidLog(const char * pszFormat, ...)
{
    char buf[1024];
    va_list args;
    va_start(args, pszFormat);
    vsprintf(buf, pszFormat, args);
    va_end(args);
    __android_log_print(ANDROID_LOG_INFO, "debug info",  buf);
}
#endif