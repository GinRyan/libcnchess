/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <assert.h>
#include <math.h>
#include <cstring>
#include <cerrno>
#include <stdexcept>
#include "gametimer.h"
#include "clock.h"
#include "utility.h"
#include "game.h"


#define GAMETIMER_DEFAULT_STEP_SECONDS	1

using namespace std;

namespace cnchess {

	GameTimer::GameTimer(long seconds, long pre_step_time, IClock* clock)
	{
		constructor(seconds, pre_step_time, clock);
	}

	GameTimer::GameTimer(const TimerInfo& timeri)
	{
		constructor(timeri.total - timeri.used, timeri.prestep, NULL);
	}

	void GameTimer::constructor(long seconds, long pre_step_time, IClock* clock)
	{
		this->status = PAUSED;
		this->totaltime = seconds;
		this->pre_step_time = pre_step_time;
		this->alarm = NULL;

		if (seconds <= 0) {
			throw invalid_argument("`seconds` invalid");
		}
		if (pre_step_time < 0) {
			this->pre_step_time = 0;
		} else if (pre_step_time > seconds) {
			this->pre_step_time = seconds;
		}
		remaintime = seconds;
		if (clock == NULL) {
			this->clock = RealClock::create(1000000);
			if (!this->clock) {
				throw runtime_error("RealClock not availabled");
			}
		} else {
			if (clock->getStatus() == STARTED) {
				clock->stop();
			}
			this->clock = clock;
		}
		this->clock->addListener(clockListener, this);
	}

	GameTimer::~GameTimer()
	{
		press();
		if (alarm) {
			delete alarm;
		}
		clock->decref();
	}

	bool GameTimer::start()
	{
		if (status == STARTED || status == EXPIRED) {
			return false;
		}
		clock->start();
		if (pre_step_time > 0 && remaintime > 0) {
			long alarm_time = 0;
			if (remaintime > pre_step_time) {
				alarm_time = pre_step_time * 1000;
			} else {
				alarm_time = remaintime * 1000;
			}

			if (alarm) {
				delete alarm;
			}
			alarm = new Alarm(clock, alarm_time);
			alarm->addListener(alarmListener, this);
			alarm->start();
		}
		setStatus(STARTED);
		return true;
	}

	bool GameTimer::press()
	{
		if (status == PAUSED || status == EXPIRED) {
			return false;
		}
		if (alarm) {
			alarm->stop();
			delete alarm;
			alarm = NULL;
		}
		clock->stop();
		if (remaintime <= 0) {
			setStatus(EXPIRED);
		} else {
			setStatus(PAUSED);
		}
		return true;
	}

	void GameTimer::overtime(long seconds)
	{
		if (seconds <= 0) {
			return;
		}
		mutex.lock();
		remaintime += seconds;
		totaltime += seconds;
		mutex.unlock();
		if (status == EXPIRED) {
			setStatus(PAUSED);
		}
	}

	long GameTimer::preStepRemainTime() const
	{
		if (alarm && (alarm->getStatus() == STARTED)) {
			return(long) roundf(alarm->getRemainTime() / 1000.0F);
		} else {
			return -1;
		}
	}

	void GameTimer::clockListener(const IClock* clock, void* ctx)
	{
		GameTimer* self = (GameTimer*) ctx;
		self->mutex.lock();
		self->remaintime -= ts2sec(clock->getInterval());
		self->mutex.unlock();
		if (self->remaintime <= 0) {
			self->press();
			self->dispatcher.fire(self, EXPIRED_TIMER);
		}
	}

	void GameTimer::alarmListener(const Alarm* alarm, void* ctx)
	{
		GameTimer* self = (GameTimer*) ctx;
		self->press();
		if (self->remaintime <= 0) {
			self->dispatcher.fire(self, EXPIRED_TIMER);
		} else {
			self->dispatcher.fire(self, EXPIRED_STEP);
		}

	}

}
