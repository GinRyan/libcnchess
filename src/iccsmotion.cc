/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#include <stdexcept>
#include "iccsmotion.h"
#include "utility.h"
#include "helper.h"
#include "piecepos.h"

using namespace std;

static bool iccsmotion_xpos_isvalid(char x)
{
	char x_upper = toupper(x);
	for (int i = 0; i < 10; i++) {
		if (cnchess::iccsmotion_x_range[i] == x_upper) {
			return true;
		}
	}
	return false;
}

static inline bool iccsmotion_ypos_isvalid(int y)
{
	return(y >= cnchess::iccsmotion_y_range[0]) &&
		(y <= cnchess::iccsmotion_y_range[9]);
}

namespace cnchess {

	bool iccsmotion_isvalid(const std::string& str1)
	{
		string str;
		trim(str, str1);
		string newstr;
		for (int i = 0; i < str.length(); i++) {
			if (str[i] == '-') {
				continue;
			}
			newstr.append(1, str[i]);
		}
		if (newstr.length() != 4) {
			return false;
		}
		char hs = toupper(newstr[0]);
		char vs = newstr[1];
		char he = toupper(newstr[2]);
		char ve = newstr[3];
		if (!isdigit(vs) || !isdigit(ve)) {
			return false;
		}
		int nvs = ctoi(vs);
		int nve = ctoi(ve);
		ICCSMotion motion;
		try {
			motion.start.setX(hs);
			motion.start.setY(nvs);
			motion.end.setX(he);
			motion.end.setY(nve);
		} catch (invalid_argument& e) {
			return false;
		}
		return iccsmotion_isvalid(motion);
	}

	bool iccsmotion_isvalid(const ICCSMotion& motion)
	{
		if (iccsmotion_xpos_isvalid(motion.start.getX()) && iccsmotion_xpos_isvalid(motion.end.getX()) &&
			iccsmotion_ypos_isvalid(motion.start.getY()) && iccsmotion_ypos_isvalid(motion.end.getY())) {
			if (motion.start.getX() == motion.end.getX() && motion.start.getY() == motion.end.getY()) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	int iccsmotion_ypos4chessbook(int y)
	{
		if (!iccsmotion_ypos_isvalid(y)) {
			return 0;
		}
		return -(y - (ChessBook::size() - 1));
	}

	int iccsmotion_xpos4chessbook(char x)
	{
		char x_upper = toupper(x);
		switch (x_upper) {
		case 'A':
			return 0;
		case 'B':
			return 1;
		case 'C':
			return 2;
		case 'D':
			return 3;
		case 'E':
			return 4;
		case 'F':
			return 5;
		case 'G':
			return 6;
		case 'H':
			return 7;
		case 'I':
			return 8;
		default:
			return 0;
		}
	}

	const ChessPiece* iccsmotion_getpiece(const ChessBook& cb, const ICCSPos& pos)
	{
		PiecePos piecepos;
		if (pos.toPiecePos(piecepos)) {
			return &(cb.get(piecepos));
		} else {
			return NULL;
		}
	}

	ICCSMotion* iccsmotion_parse(ICCSMotion& motion, const PieceMotion& pcemotion)
	{
		motion.start = ICCSPos(pcemotion.start);
		motion.end = ICCSPos(pcemotion.end);
		return &motion;
	}
	
	ICCSMotion* iccsmotion_parse(ICCSMotion& motion, const std::string& str)
	{
		string newstr;
		for (int i = 0; i < str.length(); i++) {
			if (str[i] == '-') {
				continue;
			}
			newstr.append(1, str[i]);
		}
		if (newstr.length() != 4) {
			return NULL;
		}
		char hs = newstr[0];
		char vs = newstr[1];
		char he = newstr[2];
		char ve = newstr[3];
		if (!isdigit(vs) || !isdigit(ve)) {
			return NULL;
		}
		int nvs = ctoi(vs);
		int nve = ctoi(ve);
		if (nvs > CHESSBOOK_COL || nve > CHESSBOOK_COL ||
			!iccsmotion_xpos_isvalid(hs) || !iccsmotion_xpos_isvalid(he)) {
			return NULL;
		}
		motion.start.setY(nvs);
		motion.start.setX(hs);
		motion.end.setY(nve);
		motion.end.setX(he);
		return &motion;
	}

	std::string* iccsmotion_tostring(std::string& str, const ICCSMotion& motion)
	{
		if (!iccsmotion_isvalid(motion)) {
			return NULL;
		}
		str.clear();
		str.append(1, motion.start.getX());
		str.append(asstring(motion.start.getY()));
		str.append(1, motion.end.getX());
		str.append(asstring(motion.end.getY()));
		return &str;
	}

	std::string iccsmotion_tostring(const ICCSMotion& motion)
	{
		string res;
		if (iccsmotion_tostring(res, motion)) {
			return res;
		} else {
			return "";
		}
	}

	ChessBook* iccsmotion_apply(ChessBook& cb, const ICCSMotion& motion)
	{
		if (!iccsmotion_isvalid(motion)) {
			return NULL;
		}
		int chessbook_sx = iccsmotion_xpos4chessbook(motion.start.getX());
		int chessbook_sy = iccsmotion_ypos4chessbook(motion.start.getY());
		int chessbook_ex = iccsmotion_xpos4chessbook(motion.end.getX());
		int chessbook_ey = iccsmotion_ypos4chessbook(motion.end.getY());
		if (cb[chessbook_sy][chessbook_sx].isType(ChessPiece::NONE)) {
			return NULL;
		}
		cb[chessbook_ey][chessbook_ex].setType(cb[chessbook_sy][chessbook_sx].getType());
		cb[chessbook_ey][chessbook_ex].setOwner(cb[chessbook_sy][chessbook_sx].getOwner());
		cb[chessbook_sy][chessbook_sx].setType(ChessPiece::NONE);
		cb[chessbook_sy][chessbook_sx].setOwner(RL_NONE);
		return &cb;
	}

	ChessBook* iccsmotion_apply(ChessBook& cb, const std::string& str)
	{
		ICCSMotion motion;
		if (NULL == iccsmotion_parse(motion, str)) {
			return NULL;
		}
		return iccsmotion_apply(cb, motion);
	}

}
