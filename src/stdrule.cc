#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include "stdrule.h"
#include "chessarea.h"
#include "utility.h"
#include "helper.h"
#include "gameplayer.h"

using namespace std;

namespace cnchess {

	typedef bool (*PathSelector)(const ChessBook& cb, const ChessPiece& curr_piece,
			const PiecePos& curr_pos, const PiecePos& next_pos);

	static PathList* stdrule_findpath(PathList& pathlist, const ChessBook& cb,
			const ChessPiece& piece, const PiecePosRange& range, PathSelector selector)
	{
		if (piece.isType(ChessPiece::NONE)) {
			return NULL;
		}

		PiecePos pos;
		if (!cb.locate(&pos, piece)) {
			return NULL;
		}

		for (int i = 0; i < range.size(); i++) {
			PiecePos next_pos = range[i];
			if (pos.equal(next_pos)) {
				continue;
			}
			if ((piece.getOwner() == cb.get(next_pos).getOwner()) &&
					!cb.get(next_pos).isType(ChessPiece::NONE)) {
				continue;
			}

			if (selector) {
				if (selector(cb, piece, pos, next_pos)) {
					pathlist.push_back(PieceMotion(pos, next_pos));
				}
			} else {
				pathlist.push_back(PieceMotion(pos, next_pos));
			}
		}

		return &pathlist;
	}

	static bool pathselector4king(const ChessBook& cb, const ChessPiece& curr_piece,
			const PiecePos& curr_pos, const PiecePos& next_pos)
	{
		if ((abs(curr_pos.getX() - next_pos.getX()) == 1) &&
				(curr_pos.getY() == next_pos.getY())) {
			return true;
		}

		if ((abs(curr_pos.getY() - next_pos.getY()) == 1) &&
				(curr_pos.getX() == next_pos.getX())) {
			return true;
		}

		return false;
	}

	static bool pathselector4advisor(const ChessBook& cb, const ChessPiece& curr_piece,
			const PiecePos& curr_pos, const PiecePos& next_pos)
	{
		if ((abs(curr_pos.getX() - next_pos.getX()) == 1) &&
				(abs(curr_pos.getY() - next_pos.getY()) == 1)) {
			return true;
		}

		return false;
	}

	static bool pathselector4bishop(const ChessBook& cb, const ChessPiece& curr_piece,
			const PiecePos& curr_pos, const PiecePos& next_pos)
	{
		if ((abs(curr_pos.getX() - next_pos.getX()) == 2) &&
				(abs(curr_pos.getY() - next_pos.getY()) == 2)) {
			PiecePos pos;
			if (curr_pos.getX() > next_pos.getX()) {
				pos.setX(curr_pos.getX() - 1);
			} else {
				pos.setX(curr_pos.getX() + 1);
			}
			if (curr_pos.getY() > next_pos.getY()) {
				pos.setY(curr_pos.getY() - 1);
			} else {
				pos.setY(curr_pos.getY() + 1);
			}
			if (!cb.get(pos).isType(ChessPiece::NONE)) {
				return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * 判断马可走路径
	 * 
	 * @param cb
	 * @param curr_piece
	 * @param curr_pos
	 * @param next_pos
	 * @return 
	 */
	static bool pathselector4knight(const ChessBook& cb, const ChessPiece& curr_piece,
			const PiecePos& curr_pos, const PiecePos& next_pos)
	{
		int x_distance = abs(curr_pos.getX() - next_pos.getX());
		int y_distance = abs(curr_pos.getY() - next_pos.getY());

		if (((x_distance == 1) && (y_distance == 2)) ||
				((x_distance == 2) && (y_distance == 1))) {

			if (x_distance > y_distance) {
				// 马横向走日
				int x_majiao;
				if (next_pos.getX() > curr_pos.getX()) {
					x_majiao = curr_pos.getX() + 1;
				} else {
					x_majiao = curr_pos.getX() - 1;
				}
				// 检查是否有棋子绊马脚
				if (cb[curr_pos.getY()][x_majiao].isType(ChessPiece::NONE)) {
					return true;
				}
			} else {
				// 马垂直方向走日
				int y_majiao;
				if (next_pos.getY() > curr_pos.getY()) {
					y_majiao = curr_pos.getY() + 1;
				} else {
					y_majiao = curr_pos.getY() - 1;
				}
				// 检查是否有棋子绊马脚
				if (cb[y_majiao][curr_pos.getX()].isType(ChessPiece::NONE)) {
					return true;
				}
			}

			return false;
		}
		return false;
	}

	/**
	 * 判断车可走路径
	 * 
	 * @param cb
	 * @param curr_piece
	 * @param curr_pos
	 * @param next_pos
	 * @return 
	 */
	static bool pathselector4rook(const ChessBook& cb, const ChessPiece& curr_piece,
			const PiecePos& curr_pos, const PiecePos& next_pos)
	{
		int x_distance = abs(curr_pos.getX() - next_pos.getX());
		int y_distance = abs(curr_pos.getY() - next_pos.getY());

		// 车横向走
		if ((x_distance > 0) && (y_distance == 0)) {
			if (curr_pos.getX() > next_pos.getX()) {
				// 向左走, 检查左边路线中间是否有别的棋子挡着车路
				for (int i = curr_pos.getX() - 1; i > next_pos.getX(); i--) {
					if (!cb[curr_pos.getY()][i].isType(ChessPiece::NONE)) {
						return false;
					}
				}
			} else {
				// 向右
				for (int i = curr_pos.getX() + 1; i < next_pos.getX(); i++) {
					if (!cb[curr_pos.getY()][i].isType(ChessPiece::NONE)) {
						return false;
					}
				}
			}

			return true;
		}

		// 车垂直方向行走
		if ((y_distance > 0) && (x_distance == 0)) {
			if (curr_pos.getY() > next_pos.getY()) {
				// 向上走
				for (int i = curr_pos.getY() - 1; i > next_pos.getY(); i--) {
					if (!cb[i][curr_pos.getX()].isType(ChessPiece::NONE)) {
						return false;
					}
				}
			} else {
				// 向下走
				for (int i = curr_pos.getY() + 1; i < next_pos.getY(); i++) {
					if (!cb[i][curr_pos.getX()].isType(ChessPiece::NONE)) {
						return false;
					}
				}
			}

			return true;
		}

		return false;
	}

	static bool pathselector4cannon(const ChessBook& cb, const ChessPiece& curr_piece,
			const PiecePos& curr_pos, const PiecePos& next_pos)
	{
		int x_distance = abs(curr_pos.getX() - next_pos.getX());
		int y_distance = abs(curr_pos.getY() - next_pos.getY());

		// 横向走
		if ((x_distance > 0) && (y_distance == 0)) {
			if (cb.get(next_pos).isType(ChessPiece::NONE)) {
				// 如果 next pos 位置上没有对方的棋子, 则按车的规则处理
				if (curr_pos.getX() > next_pos.getX()) {
					// 向左走, 检查左边路线中间是否有别的棋子挡着路
					for (int i = curr_pos.getX() - 1; i > next_pos.getX(); i--) {
						if (!cb[curr_pos.getY()][i].isType(ChessPiece::NONE)) {
							return false;
						}
					}
				} else {
					// 向右
					for (int i = curr_pos.getX() + 1; i < next_pos.getX(); i++) {
						if (!cb[curr_pos.getY()][i].isType(ChessPiece::NONE)) {
							return false;
						}
					}
				}

				return true;
			} else {
				// 如果 next pos 位置上有对方的棋子, 则形成吃子, 则检查中间是否有且仅有一个棋子充当 "炮架"
				int piece_nums = 0;
				if (curr_pos.getX() > next_pos.getX()) {
					// 向左走
					for (int i = curr_pos.getX() - 1; i > next_pos.getX(); i--) {
						if (!cb[curr_pos.getY()][i].isType(ChessPiece::NONE)) {
							piece_nums++;
						}
					}
				} else {
					// 向右
					for (int i = curr_pos.getX() + 1; i < next_pos.getX(); i++) {
						if (!cb[curr_pos.getY()][i].isType(ChessPiece::NONE)) {
							piece_nums++;
						}
					}
				}

				return piece_nums == 1;
			}

		}

		// 垂直方向行走
		if ((y_distance > 0) && (x_distance == 0)) {

			if (cb.get(next_pos).isType(ChessPiece::NONE)) {
				if (curr_pos.getY() > next_pos.getY()) {
					// 向上走
					for (int i = curr_pos.getY() - 1; i > next_pos.getY(); i--) {
						if (!cb[i][curr_pos.getX()].isType(ChessPiece::NONE)) {
							return false;
						}
					}
				} else {
					// 向下走
					for (int i = curr_pos.getY() + 1; i < next_pos.getY(); i++) {
						if (!cb[i][curr_pos.getX()].isType(ChessPiece::NONE)) {
							return false;
						}
					}
				}

				return true;
			} else {
				int piece_nums = 0;
				if (curr_pos.getY() > next_pos.getY()) {
					// 向上走
					for (int i = curr_pos.getY() - 1; i > next_pos.getY(); i--) {
						if (!cb[i][curr_pos.getX()].isType(ChessPiece::NONE)) {
							piece_nums++;
						}
					}
				} else {
					// 向下走
					for (int i = curr_pos.getY() + 1; i < next_pos.getY(); i++) {
						if (!cb[i][curr_pos.getX()].isType(ChessPiece::NONE)) {
							piece_nums++;
						}
					}
				}

				return piece_nums == 1;
			}
		}

		return false;
	}

	static bool pathselector4pown(const ChessBook& cb, const ChessPiece& curr_piece,
			const PiecePos& curr_pos, const PiecePos& next_pos)
	{
		int x_distance = abs(curr_pos.getX() - next_pos.getX());
		int y_distance = abs(curr_pos.getY() - next_pos.getY());

		if (curr_piece.getOwner() == RL_BLACK) {
			// 黑方的兵

			if (next_pos.getY() < curr_pos.getY()) {
				// 兵是不能后退的
				return false;
			}
			if (x_distance == 0 && y_distance == 1) {
				// 兵无论在哪都可以向前走一步
				return true;
			}

			ChessArea area;
			chessarea_mkred(area);
			if (chessarea_contains(area, curr_pos)) {
				// 如果兵在对方棋盘内, 则还可以左右走一步
				if (x_distance == 1 && y_distance == 0) {
					return true;
				}
			}
		} else {
			// 红方的兵

			if (next_pos.getY() > curr_pos.getY()) {
				// 兵是不能后退的
				return false;
			}
			if (x_distance == 0 && y_distance == 1) {
				// 兵无论在哪都可以向前走一步
				return true;
			}

			ChessArea area;
			chessarea_mkblack(area);
			if (chessarea_contains(area, curr_pos)) {
				// 如果在对方棋盘内, 则兵还可以左右走一步
				if (x_distance == 1 && y_distance == 0) {
					return true;
				}
			}
		}

		return false;
	}

	PathList* stdrule_getpath(PathList& pathlist, const ChessBook& cb,
			const ChessPiece& piece)
	{
		PiecePos pos;
		if (!cb.locate(&pos, piece)) {
			// 无法取得棋子的坐标
			return NULL;
		}

		if (!stdrule_posvalid(pos, piece)) {
			// 首先判断棋子位置是否合法
			return NULL;
		}

		if (piece.isType(ChessPiece::KING)) {
			ChessArea area;
			chessarea_mkking(area, piece.getOwner());
			PiecePosRange range;
			chessarea_torange(area, range);
			if (!stdrule_findpath(pathlist, cb, piece, range, pathselector4king)) {
				return NULL;
			}
			return &pathlist;
		} else if (piece.isType(ChessPiece::ADVISOR)) {
			ChessArea area;
			chessarea_mkking(area, piece.getOwner());
			PiecePosRange range;
			chessarea_torange(area, range);
			if (!stdrule_findpath(pathlist, cb, piece, range, pathselector4advisor)) {
				return NULL;
			}
			return &pathlist;
		} else if (piece.isType(ChessPiece::BISHOP)) {
			// 取得 "象" 能走的区域, "象" 只能在己方棋盘走, 不能过河
			ChessArea area;
			if (piece.getOwner() == RL_RED) {
				chessarea_mkred(area);
			} else {
				chessarea_mkblack(area);
			}
			PiecePosRange range;
			chessarea_torange(area, range);
			if (!stdrule_findpath(pathlist, cb, piece, range, pathselector4bishop)) {
				return NULL;
			}
			return &pathlist;
		} else if (piece.isType(ChessPiece::KNIGHT)) {
			// 取得 "马" 能走的区域
			ChessArea area;

			int ltop_xmax = pos.getX() - 2;
			int ltop_ymax = pos.getY() - 2;
			if (ltop_xmax < 0) {
				ltop_xmax = 0;
			}
			if (ltop_ymax < 0) {
				ltop_ymax = 0;
			}

			int rtop_xmax = pos.getX() + 2;
			int rtop_ymax = pos.getY() + 2;
			if (rtop_xmax > (ChessRow::size() - 1)) {
				rtop_xmax = ChessRow::size() - 1;
			}
			if (rtop_ymax > (ChessBook::size() - 1)) {
				rtop_ymax = ChessBook::size() - 1;
			}

			int lbtm_xmax = pos.getX() - 2;
			int lbtm_ymax = pos.getY() + 2;
			if (lbtm_xmax < 0) {
				lbtm_xmax = 0;
			}
			if (lbtm_ymax > (ChessBook::size() - 1)) {
				lbtm_ymax = ChessBook::size() - 1;
			}

			int rbtm_xmax = pos.getX() + 2;
			int rbtm_ymax = pos.getY() + 2;
			if (rbtm_xmax > (ChessRow::size() - 1)) {
				rbtm_xmax = ChessRow::size() - 1;
			}
			if (rbtm_ymax > (ChessBook::size() - 1)) {
				rbtm_ymax = ChessBook::size() - 1;
			}

			area.ltop.setX(ltop_xmax);
			area.ltop.setY(ltop_ymax);
			area.rtop.setX(rtop_xmax);
			area.rtop.setY(rtop_ymax);
			area.lbtm.setX(lbtm_xmax);
			area.lbtm.setY(lbtm_ymax);
			area.rbtm.setX(rbtm_xmax);
			area.rbtm.setY(rbtm_ymax);

			PiecePosRange range;
			chessarea_torange(area, range);
			if (!stdrule_findpath(pathlist, cb, piece, range, pathselector4knight)) {
				return NULL;
			}
			return &pathlist;
		} else if (piece.isType(ChessPiece::ROOK) || piece.isType(ChessPiece::CANNON)) {
			PathSelector path_selector;
			if (piece.isType(ChessPiece::ROOK)) {
				path_selector = pathselector4rook;
			} else {
				path_selector = pathselector4cannon;
			}
			PiecePosRange range;
			for (int x = 0; x < ChessRow::size(); x++) {
				if (x != pos.getX()) {
					range.push_back(PiecePos(x, pos.getY()));
				}
			}
			for (int y = 0; y < ChessBook::size(); y++) {
				if (y != pos.getY()) {
					range.push_back(PiecePos(pos.getX(), y));
				}
			}
			if (!stdrule_findpath(pathlist, cb, piece, range, path_selector)) {
				return NULL;
			}
			return &pathlist;
		} else if (piece.isType(ChessPiece::PAWN)) {
			// 取得 "兵" 能走的区域
			PiecePosRange range;
			if (piece.getOwner() == RL_BLACK) {
				ChessArea area;
				chessarea_mkblack(area);
				if (chessarea_contains(area, pos)) {
					// 兵在自己地盘只能前移
					range.push_back(PiecePos(pos.getX(), pos.getY() + 1));
				} else {
					if ((pos.getX() - 1) >= 0) {
						// 可以左移
						range.push_back(PiecePos(pos.getX() - 1, pos.getY()));
					}
					if ((pos.getX() + 1) < ChessRow::size()) {
						// 可以右移
						range.push_back(PiecePos(pos.getX() + 1, pos.getY()));
					}
					if ((pos.getY() + 1) < ChessBook::size()) {
						// 可以前移
						range.push_back(PiecePos(pos.getX(), pos.getY() + 1));
					}
				}
			} else {
				ChessArea area;
				chessarea_mkred(area);
				if (chessarea_contains(area, pos)) {
					// 兵在自己地盘只能前移
					range.push_back(PiecePos(pos.getX(), pos.getY() - 1));
				} else {
					if ((pos.getX() - 1) >= 0) {
						// 可以左移
						range.push_back(PiecePos(pos.getX() - 1, pos.getY()));
					}
					if ((pos.getX() + 1) < ChessRow::size()) {
						// 可以右移
						range.push_back(PiecePos(pos.getX() + 1, pos.getY()));
					}
					if ((pos.getY() - 1) >= 0) {
						// 可以前移
						range.push_back(PiecePos(pos.getX(), pos.getY() - 1));
					}
				}
			}

			if (!stdrule_findpath(pathlist, cb, piece, range, pathselector4pown)) {
				return NULL;
			}

			return &pathlist;
		} else {
			return NULL;
		}
	}

	ChessBook* stdrule_initchessbook(ChessBook& cb)
	{
		// default: rnbakabnr/9/1c5c1/p1p1p1p1p/9/9/P1P1P1P1P/1C5C1/9/RNBAKABNR
		cb.clear();

		//rnbakabnr
		cb[0][0] = ChessPiece(RL_BLACK, ChessPiece::ROOK);
		cb[0][1] = ChessPiece(RL_BLACK, ChessPiece::KNIGHT);
		cb[0][2] = ChessPiece(RL_BLACK, ChessPiece::BISHOP);
		cb[0][3] = ChessPiece(RL_BLACK, ChessPiece::ADVISOR);
		cb[0][4] = ChessPiece(RL_BLACK, ChessPiece::KING);
		cb[0][5] = ChessPiece(RL_BLACK, ChessPiece::ADVISOR);
		cb[0][6] = ChessPiece(RL_BLACK, ChessPiece::BISHOP);
		cb[0][7] = ChessPiece(RL_BLACK, ChessPiece::KNIGHT);
		cb[0][8] = ChessPiece(RL_BLACK, ChessPiece::ROOK);

		// 9

		// 1c5c1
		cb[2][1] = ChessPiece(RL_BLACK, ChessPiece::CANNON);
		cb[2][7] = ChessPiece(RL_BLACK, ChessPiece::CANNON);

		// p1p1p1p1p
		cb[3][0] = ChessPiece(RL_BLACK, ChessPiece::PAWN);
		cb[3][2] = ChessPiece(RL_BLACK, ChessPiece::PAWN);
		cb[3][4] = ChessPiece(RL_BLACK, ChessPiece::PAWN);
		cb[3][6] = ChessPiece(RL_BLACK, ChessPiece::PAWN);
		cb[3][8] = ChessPiece(RL_BLACK, ChessPiece::PAWN);

		// 9

		// 9

		// p1p1p1p1p
		cb[6][0] = ChessPiece(RL_RED, ChessPiece::PAWN);
		cb[6][2] = ChessPiece(RL_RED, ChessPiece::PAWN);
		cb[6][4] = ChessPiece(RL_RED, ChessPiece::PAWN);
		cb[6][6] = ChessPiece(RL_RED, ChessPiece::PAWN);
		cb[6][8] = ChessPiece(RL_RED, ChessPiece::PAWN);

		// 1c5c1
		cb[7][1] = ChessPiece(RL_RED, ChessPiece::CANNON);
		cb[7][7] = ChessPiece(RL_RED, ChessPiece::CANNON);

		// 9

		//rnbakabnr
		cb[9][0] = ChessPiece(RL_RED, ChessPiece::ROOK);
		cb[9][1] = ChessPiece(RL_RED, ChessPiece::KNIGHT);
		cb[9][2] = ChessPiece(RL_RED, ChessPiece::BISHOP);
		cb[9][3] = ChessPiece(RL_RED, ChessPiece::ADVISOR);
		cb[9][4] = ChessPiece(RL_RED, ChessPiece::KING);
		cb[9][5] = ChessPiece(RL_RED, ChessPiece::ADVISOR);
		cb[9][6] = ChessPiece(RL_RED, ChessPiece::BISHOP);
		cb[9][7] = ChessPiece(RL_RED, ChessPiece::KNIGHT);
		cb[9][8] = ChessPiece(RL_RED, ChessPiece::ROOK);

		return &cb;
	}

	bool stdrule_posvalid(const PiecePos& pos, const ChessPiece& piece)
	{
		if (piece.getOwner() == RL_NONE || piece.isType(ChessPiece::NONE)) {
			return false;
		}
		if (piece.isType(ChessPiece::KING)) {
			ChessArea area;
			chessarea_mkking(area, piece.getOwner());
			return chessarea_contains(area, pos);
		} else if (piece.isType(ChessPiece::ADVISOR) ||
				piece.isType(ChessPiece::BISHOP) || piece.isType(ChessPiece::PAWN)) {
			PiecePosRange range;
			if (stdrule_getrange(range, piece)) {
				return pos.inrange(range);
			} else {
				return false;
			}
		} else {
			// 马,车,炮可以在双方棋盘的任意位置
			return true;
		}
	}

	bool stdrule_posvalid(int x, int y, const ChessPiece& piece)
	{
		try {
			PiecePos pos(x, y);
			return stdrule_posvalid(pos, piece);
		} catch (invalid_argument& e) {
			return false;
		}
	}

	PiecePosRange* stdrule_getrange(PiecePosRange& range, const ChessPiece& piece)
	{
		if (piece.getOwner() == RL_NONE || piece.isType(ChessPiece::NONE)) {
			return NULL;
		}
		if (piece.isType(ChessPiece::KING)) {
			ChessArea area;
			chessarea_mkking(area, piece.getOwner());
			return chessarea_torange(area, range);
		} else if (piece.isType(ChessPiece::ADVISOR)) {
			if (RL_BLACK == piece.getOwner()) {
				range.push_back(PiecePos(3, 0));
				range.push_back(PiecePos(5, 0));
				range.push_back(PiecePos(4, 1));
				range.push_back(PiecePos(3, 2));
				range.push_back(PiecePos(5, 2));
			} else {
				range.push_back(PiecePos(3, 9));
				range.push_back(PiecePos(5, 9));
				range.push_back(PiecePos(4, 8));
				range.push_back(PiecePos(3, 7));
				range.push_back(PiecePos(5, 7));
			}
			return &range;
		} else if (piece.isType(ChessPiece::BISHOP)) {
			if (piece.getOwner() == RL_BLACK) {
				range.push_back(PiecePos(2, 0));
				range.push_back(PiecePos(0, 2));
				range.push_back(PiecePos(4, 2));
				range.push_back(PiecePos(2, 4));
				range.push_back(PiecePos(6, 4));
				range.push_back(PiecePos(8, 2));
				range.push_back(PiecePos(6, 0));
			} else {
				range.push_back(PiecePos(2, 5));
				range.push_back(PiecePos(0, 7));
				range.push_back(PiecePos(4, 7));
				range.push_back(PiecePos(2, 9));
				range.push_back(PiecePos(6, 9));
				range.push_back(PiecePos(8, 7));
				range.push_back(PiecePos(6, 5));
			}
			return &range;
		} else if (piece.isType(ChessPiece::PAWN)) {
			ChessArea area;
			PiecePosRange range1;
			if (piece.getOwner() == RL_BLACK) {
				range.push_back(PiecePos(0, 3));
				range.push_back(PiecePos(2, 3));
				range.push_back(PiecePos(4, 3));
				range.push_back(PiecePos(6, 3));
				range.push_back(PiecePos(8, 3));
				range.push_back(PiecePos(0, 4));
				range.push_back(PiecePos(2, 4));
				range.push_back(PiecePos(4, 4));
				range.push_back(PiecePos(6, 4));
				range.push_back(PiecePos(8, 4));

				chessarea_mkred(area);
			} else { // red
				range.push_back(PiecePos(0, 5));
				range.push_back(PiecePos(2, 5));
				range.push_back(PiecePos(4, 5));
				range.push_back(PiecePos(6, 5));
				range.push_back(PiecePos(8, 5));
				range.push_back(PiecePos(0, 6));
				range.push_back(PiecePos(2, 6));
				range.push_back(PiecePos(4, 6));
				range.push_back(PiecePos(6, 6));
				range.push_back(PiecePos(8, 6));

				chessarea_mkblack(area);
			}

			chessarea_torange(area, range1);
			size_t dest_size = range1.size();
			size_t src_size = range.size();
			range.reserve(src_size + dest_size);
			range.insert(range.begin() + src_size, range1.begin(), range1.end());

			return &range;
		} else {
			// 马,车,炮可以在双方棋盘的任意位置
			ChessArea area;
			return chessarea_torange(area, range);
		}
	}

	PieceMotion* stdrule_findstep(PieceMotion& motion, const ChessBook& cb, GameRole role, const ExcludePiece* exclude)
	{
		for (size_t row = 0; row < cb.size(); row++) {
			for (size_t col = 0; col < CHESSBOOK_COL; col++) {
				if (cb[row][col].getOwner() == role && !cb[row][col].isType(ChessPiece::NONE)) {
					if (exclude) {
						bool need_exclude = false;
						for (size_t i = 0; i < exclude->size(); i ++) {
							if ((*exclude)[i] == ChessPiece::NONE) {
								continue;
							}
							if (cb[row][col].isType((*exclude)[i])) {
								need_exclude = true;
								break;
							}
						}
						if (need_exclude) {
							continue;
						}
					}
					PathList pl;
					if (stdrule_getpath(pl, cb, cb[row][col])) {
						if (pl.size()) {
							motion = pl[0];
							return &motion;
						}
					}
				}
			}
		}
		return NULL;
	}

	bool stdrule_laststep(PieceMotion* motion, const ChessBook& cb, GameRole role)
	{
		for (size_t row = 0; row < cb.size(); row++) {
			for (size_t col = 0; col < CHESSBOOK_COL; col++) {
				if (cb[row][col].getOwner() == role && !cb[row][col].isType(ChessPiece::NONE)) {
					PathList pl;
					if (stdrule_getpath(pl, cb, cb[row][col])) {
						for (size_t i = 0; i < pl.size(); i++) {
							const ChessPiece& piece = cb.get(pl[i].end);
							if (piece.isType(ChessPiece::KING)) {
								if (motion != NULL) {
									*motion = pl[i];
								}
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	GameRole stdrule_whowin(const ChessBook& cb, const PieceMotion& lastmotion)
	{
		bool found_red_king = false;
		bool found_black_king = false;
		PiecePos red_king_pos;
		PiecePos black_king_pos;
		for (size_t i = 0; i < cb.size(); i++) {
			for (size_t j = 0; j < ChessRow::size(); j++) {
				if (cb[i][j].isType(ChessPiece::KING)) {
					if (cb[i][j].getOwner() == RL_RED) {
						red_king_pos.setY(i);
						red_king_pos.setX(j);
						found_red_king = true;
					} else {
						black_king_pos.setY(i);
						black_king_pos.setX(j);
						found_black_king = true;
					}
				}
			}
		}

		if (!found_red_king) {
			return RL_BLACK;
		} 
		
		if (!found_black_king) {
			return RL_RED;
		}
		
		// 判断双方的帅是否 “照面”
		if (red_king_pos.getX() != black_king_pos.getX()) {
			return RL_NONE;
		}
		
		for (int y = red_king_pos.getY() - 1; y > black_king_pos.getY(); y --) {
			if (!cb[y][red_king_pos.getX()].isType(ChessPiece::NONE)) {
				return RL_NONE;
			}
		}
		
		const ChessPiece& end_piece = cb.get(lastmotion.end);
		GameRole lastrl = end_piece.getOwner();
		if (lastrl == RL_NONE) {
			return RL_NONE;
		} else if (lastrl == RL_RED) {
			return RL_BLACK;
		} else {
			return RL_RED;
		}
	}
	
	const ICCSMotion* stdrule_findbanmove(const Game::History& history, GameRole role, int roundmax)
	{
		int lastindex = history.size() - 1;
		if (lastindex < 0) {
			return NULL;
		}
		if (history[lastindex].player->getMyRole() != role) {
			if (history.size() < 4) {
				return NULL;
			}
			lastindex --;
		} else {
			if (history.size() < 3) {
				return NULL;
			}
		}
		
		int round = 0;
		
		const ICCSMotion* prev_motion = &history[lastindex].motion;
		for (size_t i = lastindex - 2; i >= 2; i -= 2) {
			if (history[i].player->getMyRole() != role) {
				return NULL;
			}
			if (history[i].motion.start.equal(prev_motion->end) && history[i].motion.end.equal(prev_motion->start)) {
				round ++;
				if (round >= roundmax) {
					return &history[lastindex - 2].motion;
				}
				prev_motion = &history[i].motion;
			} else {
				return NULL;
			}
		}
		return NULL;
	}
	
}
