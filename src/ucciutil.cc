#include <string.h>
#include "ucciutil.h"
#include "fen.h"
#include "utility.h"

using namespace std;

namespace cnchess {

	std::string ucci_reqbuild_postion(const ChessBook& cb, GameRole myrole,
			StringList* move_history, int rnd)
	{
		string fen;
		int round = 1;
		string moves;
		
		fen_tostring(fen, cb);
		
		if (rnd != -1) {
			round = rnd;
		}
		
		if (move_history) {
			moves = join(" ", *move_history);
			if (rnd == -1) {
				round += (move_history->size() / 2);
			}
		}
		
		fen += " w - - 0 " + asstring(round);
		
		string req = "position fen " + fen;
		
		if (move_history && (move_history->size() > 0)) {
			req += " moves " + aslower(moves);
		}
		
		return req;
	}
	
	ICCSMotion* ucci_recommend(ICCSMotion& bestmove, UCCIEngine& uccieg, const ChessBook& cb, 
		GameRole myrole, StringList* move_history, int depth)
	{
		if (uccieg.getStatus() != STARTED) {
			return NULL;
		}
		
		uccieg.incref();
		
		string position_str = ucci_reqbuild_postion(cb, myrole, move_history);
		
		if (!uccieg.sendRequest(position_str)) {
			return NULL;
		}
		
		string go_str = "go";
		
		if (depth > 0) {
			go_str += " depth " + asstring(depth);
		}
		
		if (!uccieg.sendRequest(go_str)) {
			return NULL;
		}

		bool has_bestmove = false;
		string resp;
		while (true) {
			if (!uccieg.getResponse(resp)) {
				return NULL;
			}
			if (resp.substr(0, strlen("bestmove")) == "bestmove") {
				has_bestmove = true;
				break;
			} else if (resp.substr(0, strlen("nobestmove")) == "nobestmove") {
				has_bestmove = false;
				break;
			}
		}
		
		uccieg.decref();
		
		string eg_bestmove;
		
		if (has_bestmove) {
			StringList resp_parts;
			split(resp_parts, resp);
			
			if (resp_parts.size() < 2) {
				return NULL;
			}
			
			eg_bestmove = resp_parts[1];
			string eg_action = resp_parts[resp_parts.size() - 1];
			if (eg_action == "resign") {
				return NULL;
			} else if (eg_action == "draw") {
				// 忽略引擎求和， 朝阳返回 bestmove
				//return NULL;
			}
			iccsmotion_parse(bestmove, eg_bestmove);
			return &bestmove;
		}
		
		return NULL;
	}

}
