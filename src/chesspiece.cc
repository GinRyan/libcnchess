/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#include <stdexcept>
#include "chesspiece.h"

using namespace std;

namespace cnchess {

	ChessPiece::ChessPiece(GameRole owner, ChessPiece::Type piece) : owner(owner)
	{
		setType(piece);
		if (piece == NONE) {
			owner = RL_NONE;
		}
	}

	ChessPiece::~ChessPiece()
	{

	}

	void ChessPiece::setType(ChessPiece::Type piece)
	{
		if (piece != KING &&
			piece != ADVISOR &&
			piece != BISHOP &&
			piece != KNIGHT &&
			piece != ROOK &&
			piece != CANNON &&
			piece != PAWN &&
			piece != NONE) {
			throw invalid_argument("piece type invalid");
		} else {
			piece_type = piece;
		}
		if (piece == NONE) {
			owner = RL_NONE;
		}
	}

}

