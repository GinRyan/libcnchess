/* 
 * File:   clock.h
 * Author: alec
 *
 * Created on 2012年5月18日, 上午11:09
 */

#include <cstring>
#include <cerrno>
#include <assert.h>
#include <signal.h>
#include <math.h>
#include <stdexcept>
#include <iostream>
#include "clock.h"
#include "utility.h"
#include "config.h"

using namespace std;

namespace cnchess {

	int RealClock::curr_alloced_signo = 0;

	int RealClock::maxClock()
	{
		int res = CNCS_REALCLOCK_SIGMAX - CNCS_REALCLOCK_SIGMIN + 1;
		return(res > 0) ? res : 0;
	}

	int RealClock::remainClock()
	{
		if (curr_alloced_signo < CNCS_REALCLOCK_SIGMIN) {
			return maxClock();
		} else if (curr_alloced_signo >= CNCS_REALCLOCK_SIGMAX) {
			return 0;
		} else {
			return CNCS_REALCLOCK_SIGMAX - curr_alloced_signo;
		}
	}

	RealClock* RealClock::create(long expire)
	{
		if (remainClock()) {
			if (curr_alloced_signo < CNCS_REALCLOCK_SIGMIN) {
				curr_alloced_signo = CNCS_REALCLOCK_SIGMIN;
			} else {
				curr_alloced_signo++;
			}
			return new RealClock(expire, curr_alloced_signo);
		} else {
			return NULL;
		}
	}

	RealClock* RealClock::createFromTS(const struct timespec& ts)
	{
		if (remainClock()) {
			if (curr_alloced_signo < CNCS_REALCLOCK_SIGMIN) {
				curr_alloced_signo = CNCS_REALCLOCK_SIGMIN;
			} else {
				curr_alloced_signo++;
			}
			return new RealClock(ts, curr_alloced_signo);
		} else {
			return NULL;
		}
	}

	void RealClock::signalHandler(int sig, siginfo_t *si, void *uc)
	{
		RealClock * self = (RealClock*) si->si_value.sival_ptr;
		self->dispatcher.fire(self);
	}

	RealClock::RealClock(long interval, int signum) : timerid(NULL), signum(signum), status(IDLE)
	{
		assert(interval > 0 && signum > 0);

		ts.tv_sec = (long) (interval / 1000000);
		ts.tv_nsec = (long) ((interval * 1000) % 1000000000);
	}

	RealClock::RealClock(const struct timespec& ts, int signum)
	: timerid(NULL), ts(ts), signum(signum), status(IDLE)
	{
		assert((ts.tv_sec > 0 || ts.tv_nsec > 0) && signum > 0);
	}

	RealClock::~RealClock()
	{
		stop();

		curr_alloced_signo--;
	}

	void RealClock::start()
	{
		if (status == STARTED) {
			return;
		}
		struct sigevent sev;
		struct itimerspec its;
		sigset_t mask;
		struct sigaction sa;

		memset(&sev, 0, sizeof(sigevent));
		memset(&sa, 0, sizeof(sa));

		sa.sa_flags = SA_SIGINFO;
		sa.sa_sigaction = signalHandler;
		sigemptyset(&sa.sa_mask);
		if (sigaction(signum, &sa, NULL) == -1) {
			throw runtime_error(strerror(errno));
		}

		sigemptyset(&mask);
		sigaddset(&mask, signum);
		if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1) {
			throw runtime_error(strerror(errno));
		}

		sev.sigev_notify = SIGEV_SIGNAL;
		sev.sigev_signo = signum;
		sev.sigev_value.sival_ptr = this;

		if (timer_create(CLOCK_REALTIME, &sev, &timerid) == -1) {
			throw runtime_error(strerror(errno));
		}

		its.it_value.tv_sec = ts.tv_sec;
		its.it_value.tv_nsec = ts.tv_nsec;
		its.it_interval.tv_sec = its.it_value.tv_sec;
		its.it_interval.tv_nsec = its.it_value.tv_nsec;

		if (timer_settime(timerid, 0, &its, NULL) == -1) {
			throw runtime_error(strerror(errno));
		}

		if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1) {
			throw runtime_error(strerror(errno));
		}

		setStatus(STARTED);
	}

	void RealClock::stop()
	{
		if (status == STOPPED || status == IDLE) {
			return;
		}
		if (timerid) {
			struct itimerspec its;
			memset(&its, 0, sizeof(struct itimerspec));
			timer_settime(timerid, 0, &its, NULL);
			timer_delete(timerid);
			signal(signum, SIG_DFL);
			setStatus(STOPPED);
		}
	}

}
