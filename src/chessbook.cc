/***
 * Copyright (c) 2012,  alecH <egteam.lucifer@gmail.com>
 *
 * All rights reserved.
 *
 */

#include <assert.h>
#include <ctype.h>
#include <cstdio>
#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <assert.h>
#include "config.h"
#include "chessbook.h"
#include "utility.h"


#define get_out_of_range_emsg(index)	format("index %d out of range", index).c_str()


using namespace std;

namespace cnchess {

	ChessBook::ChessBook()
	{
		mutex = new Mutex;
	}

	ChessBook::~ChessBook()
	{
		delete mutex;
	}

	ChessBook::ChessBook(const ChessBook& other)
	{
		for (int i = 0; i < CHESSBOOK_ROW; i++) {
			this->rows[i] = other.rows[i];
		}
		this->mutex = new Mutex;
	}

	ChessBook& ChessBook::operator =(const ChessBook& other)
	{
		this->mutex->lock();
		for (int i = 0; i < CHESSBOOK_ROW; i++) {
			this->rows[i] = other.rows[i];
		}
		this->mutex->unlock();
		return *this;
	}

	ChessRow& ChessBook::operator[](int index)
	{
		if (index >= size()) {
			throw out_of_range(get_out_of_range_emsg(index));
		}

		return rows[index];
	}

	const ChessRow& ChessBook::operator[](int index) const
	{
		if (index >= size()) {
			throw out_of_range(get_out_of_range_emsg(index));
		}

		return rows[index];
	}

	bool ChessBook::locate(PiecePos* pos, const ChessPiece& piece) const
	{
		for (int i = 0; i < size(); i++) {
			for (int j = 0; j < ChessRow::size(); j++) {
				if (&(rows[i][j]) == &piece) {
					if (pos) {
						pos->setX(j);
						pos->setY(i);
					}
					return true;
				}
			}
		}
		return false;
	}

	bool ChessBook::locate(PiecePos& pos, const ChessPiece& piece) const
	{
		return locate(&pos, piece);
	}

	size_t ChessBook::find(PiecePosList& pos, ChessPiece::Type piecetype, GameRole role) const
	{
		if (piecetype == ChessPiece::NONE) {
			return 0;
		}
		for (int i = 0; i < size(); i++) {
			for (int j = 0; j < ChessRow::size(); j++) {
				if (rows[i][j].isType(piecetype)) {
					if (role == rows[i][j].getOwner()) {
						pos.push_back(PiecePos(j, i));
					}
				}
			}
		}
		return pos.size();
	}
	
	bool ChessBook::move(const PiecePos& start, const PiecePos& end)
	{
		if (rows[start.getY()][start.getX()].isType(ChessPiece::NONE)) {
			return false;
		}
		rows[end.getY()][end.getX()] = rows[start.getY()][start.getX()];
		rows[start.getY()][start.getX()].setType(ChessPiece::NONE);
		rows[start.getY()][start.getX()].setOwner(RL_NONE);
		return true;
	}

	bool ChessBook::move(const PieceMotion& motion)
	{
		return move(motion.start, motion.end);
	}

	bool ChessBook::move(const ChessPiece& piece, const PiecePos& end)
	{
		PiecePos start;
		if (!locate(&start, piece)) {
			return false;
		}
		return move(start, end);
	}

	bool ChessBook::move(const ChessPiece& piece, int x, int y)
	{
		try {
			PiecePos end(x, y);
			return move(piece, end);
		} catch (invalid_argument& e) {
			return false;
		}
	}

	void ChessBook::clear()
	{
		mutex->lock();
		for (size_t i = 0; i < size(); i++) {
			rows[i].clear();
		}
		mutex->unlock();
	}

	bool ChessBook::isEmpty() const
	{
		mutex->lock();
		for (int i = 0; i < size(); i++) {
			if (!rows[i].isEmpty()) {
				mutex->unlock();
				return false;
			}
		}
		mutex->unlock();
		return true;
	}

	bool ChessBook::equal(const ChessBook& other) const
	{
		mutex->lock();
		for (int i = 0; i < size(); i++) {
			if (!rows[i].equal(other[i])) {
				mutex->unlock();
				return false;
			}
		}
		mutex->unlock();
		return true;
	}

}
