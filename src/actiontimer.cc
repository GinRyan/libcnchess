#include "actiontimer.h"

namespace cnchess {

	int ActionTimer::getStatus() const
	{
		int st = gtimer->getStatus();
		if (st == IDLE || st == STOPPED || st == EXPIRED) {
			return st;
		} else if (totalTime() == 0) {
			return IDLE;
		} else if (remainTime() == -1) {
			return IDLE;
		} else if (remainTime() == 0) {
			return EXPIRED;
		} else {
			return st;
                }
	}

}
