/* 
 * File:   helper.h
 * Author: xhni
 *
 * Created on 2012年6月26日, 下午5:21
 */

#ifndef CNCS_HELPER_H
#define	CNCS_HELPER_H

#include "utility.h"

#define piecepos_pdebug(piecepos)			\
	pdebug("PiecePos(x: %d, y: %d)", (piecepos).getX(), (piecepos).getY())

#define iccspos_pdebug(iccspos)			\
	pdebug("ICCSPos(x: %d, y: %d)", (iccspos).getX(), (iccspos).getY())

#define iccsmotion_pdebug(iccsmotion)	\
	pdebug("ICCSMotion(start(x: %d, y: %d) - end(x: %d, y: %d))", iccsmotion.start.getX(), \
	iccsmotion.start.getY(), iccsmotion.end.getX(), iccsmotion.end.getY())

#define piecemotion_pdebug(motion)	\
	pdebug("PieceMotion(start(x: %d, y: %d) - end(x: %d, y: %d))", motion.start.getX(), \
	motion.start.getY(), motion.end.getX(), motion.end.getY())

#endif	/* CNCS_HELPER_H */

