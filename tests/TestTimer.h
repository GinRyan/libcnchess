/*
 * File:   TestTimer.h
 * Author: alec
 *
 * Created on 2012-5-17, 10:14:57
 */

#ifndef TESTTIMER_H
#define	TESTTIMER_H

#include <cppunit/extensions/HelperMacros.h>

class TestTimer : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestTimer);

	CPPUNIT_TEST(testTimer);
	CPPUNIT_TEST(testAddExpiredListener);
	CPPUNIT_TEST(testGetStatus);
	CPPUNIT_TEST(testRemainTime);
	CPPUNIT_TEST(testRemoveExpiredListener);
	CPPUNIT_TEST(testRestart);
	CPPUNIT_TEST(testStart);
	CPPUNIT_TEST(testStop);
	CPPUNIT_TEST(testTotalTime);

	CPPUNIT_TEST_SUITE_END();

public:
	TestTimer();
	virtual ~TestTimer();
	void setUp();
	void tearDown();

private:
	void testTimer();
	void testAddExpiredListener();
	void testGetStatus();
	void testRemainTime();
	void testRemoveExpiredListener();
	void testRestart();
	void testStart();
	void testStop();
	void testTotalTime();

};

#endif	/* TESTTIMER_H */

