/*
 * File:   TestUCCIEngine.cc
 * Author: alec
 *
 * Created on 2012-6-18, 17:53:08
 */

#include "TestUCCIEngine.h"
#include "src/include/cnchess/ucciengine.h"
#include "cnchess.h"

#define UCCI_ENGINE	"/home/xhni/alec/workspace/chinachess/code/trunk/eleeye/dist/Debug/NXP-Linux-x86/eleeye"

using namespace std;
using namespace cnchess;

string get_engine()
{
#ifdef CNCS_TEST_FOR_ALECH
	return UCCI_ENGINE;
#else
	printf("UCCI engine path: ");
	char buf[4096] = {0};
	scanf("%4096s", buf);
	return string(buf);
#endif
}

CPPUNIT_TEST_SUITE_REGISTRATION(TestUCCIEngine);

TestUCCIEngine::TestUCCIEngine()
{
}

TestUCCIEngine::~TestUCCIEngine()
{
}

void TestUCCIEngine::setUp()
{
}

void TestUCCIEngine::tearDown()
{
}

void TestUCCIEngine::testUCCIEngine()
{
	;
}

void TestUCCIEngine::testGetResponse()
{
	;
}

void TestUCCIEngine::testGetStatus()
{return;
	UCCIEngine* uccip = NULL;
	uccip = new UCCIEngine("/bin/cat");
	CPPUNIT_ASSERT(uccip->getStatus() == IDLE);
	CPPUNIT_ASSERT(!uccip->start());
	CPPUNIT_ASSERT(uccip->getStatus() == IDLE);
	uccip->decref();
	
	string ucci_engine = get_engine();
	uccip = new UCCIEngine(ucci_engine);
	CPPUNIT_ASSERT(uccip->start());
	CPPUNIT_ASSERT(uccip->getStatus() == STARTED);
	uccip->stop();
	CPPUNIT_ASSERT(uccip->getStatus() == STOPPED);
}

void TestUCCIEngine::testSendRequest()
{return;
	UCCIEngine* uccip = NULL;
	string ucci_engine = get_engine();
	uccip = new UCCIEngine(ucci_engine);
	CPPUNIT_ASSERT(uccip->start());
	
	CPPUNIT_ASSERT(uccip->sendRequest("isready"));
	string resp;
	CPPUNIT_ASSERT(*uccip->getResponse(resp) == "readyok");
	uccip->decref();
}

void TestUCCIEngine::testStart()
{return;
	UCCIEngine* uccip = NULL;
	
	uccip = new UCCIEngine("/etc/passwd");
	CPPUNIT_ASSERT(!uccip->start());
	uccip->decref();
	
	uccip = new UCCIEngine("/usr/bin/test");
	bool st = uccip->start();
	CPPUNIT_ASSERT(!st);
	uccip->decref();
	
	uccip = new UCCIEngine("/usr");
	CPPUNIT_ASSERT(!uccip->start());
	uccip->decref();
	
	uccip = new UCCIEngine("/sbin/poweroff");
	CPPUNIT_ASSERT(!uccip->start());
	uccip->decref();
	
	uccip = new UCCIEngine("/bin/cat");
	CPPUNIT_ASSERT(!uccip->start());
	uccip->decref();
	
	for (int i = 0; i < 10; i++) {
		string ucci_engine = get_engine();
		uccip = new UCCIEngine(ucci_engine);
		bool res = uccip->start();
		CPPUNIT_ASSERT(res);
		uccip->decref();
	}
}

void TestUCCIEngine::testStop()
{
	UCCIEngine* uccip = NULL;
	string ucci_engine = get_engine();
	uccip = new UCCIEngine(ucci_engine);
	bool res = uccip->start();
	CPPUNIT_ASSERT(res);
	for (int i = 0; i < 10; i++) {
		uccip->stop();
	}
	uccip->start();
	uccip->decref();
}

