/*
 * File:   TestGame.cc
 * Author: alec
 *
 * Created on 2012-5-24, 14:56:43
 */

#include "TestGame.h"
#include "src/include/cnchess/game.h"
#include "cnchess.h"

using namespace std;
using namespace cnchess;

class Player : public GamePlayer {
public:
	int requestHandle(const GameAction& ac, Coordinator& reqhandler, IActionTimer& timer)
	{
		return HANDLED;
	}
};

Game* createNewGame() {
	GamePlayer* red = new Player();
	GamePlayer* black = new Player();
	GameReferee* referee = new STDReferee();
	ChessBook cb;
	stdrule_initchessbook(cb);
	return new Game(cb, red, black, referee);
}

CPPUNIT_TEST_SUITE_REGISTRATION(TestGame);

TestGame::TestGame()
{
}

TestGame::~TestGame()
{
}

void TestGame::setUp()
{
}

void TestGame::tearDown()
{
}

void TestGame::testGame()
{
	Game* game = createNewGame();
	CPPUNIT_ASSERT_EQUAL(Game::PAUSED, game->getStatus());
	CPPUNIT_ASSERT_EQUAL(game->getPlayer(RL_RED), game->getFirst());
	CPPUNIT_ASSERT_EQUAL(game->getPlayer(RL_BLACK), game->getNext());
	CPPUNIT_ASSERT_EQUAL(game->getPlayer(RL_RED), game->getCurrent());
	CPPUNIT_ASSERT_EQUAL(game->getPlayer(RL_BLACK), game->getAdversary(game->getCurrent()));
	CPPUNIT_ASSERT_EQUAL((int)0, (int)game->getHistory().size());
	game->decref();
}

void TestGame::testGame2()
{
	;
}

void TestGame::testGetAdversary()
{
	;
}

void TestGame::testGetBoard()
{
	Game* game = createNewGame();
	ChessBook& result = game->getBoard();
	ChessBook cb;
	stdrule_initchessbook(cb);
	CPPUNIT_ASSERT(cb.equal(result));
}

void TestGame::testGetChessBook()
{
	Game* game = createNewGame();
	ChessBook& result = game->getBoard();
	ChessBook cb;
	stdrule_initchessbook(cb);
	CPPUNIT_ASSERT(cb.equal(result));
}

void TestGame::testGetCurrent()
{
	Game* game = createNewGame();
	CPPUNIT_ASSERT_EQUAL(game->getPlayer(RL_RED), game->getCurrent());
	game->setNext(game->getPlayer(RL_RED));
	CPPUNIT_ASSERT_EQUAL(game->getPlayer(RL_BLACK), game->getCurrent());
	game->decref();
}

void TestGame::testGetFirst()
{
	Game* game = createNewGame();
	CPPUNIT_ASSERT_EQUAL(game->getPlayer(RL_RED), game->getFirst());
	game->setNext(game->getPlayer(RL_BLACK));
	CPPUNIT_ASSERT_EQUAL(game->getPlayer(RL_RED), game->getFirst());
	game->decref();
}

void TestGame::testGetHistory()
{
	;
}

void TestGame::testGetNext()
{
	Game* game = createNewGame();
	CPPUNIT_ASSERT_EQUAL(game->getPlayer(RL_BLACK), game->getNext());
	game->setNext(game->getPlayer(RL_RED));
	CPPUNIT_ASSERT_EQUAL(game->getPlayer(RL_RED), game->getNext());
	game->decref();
}

void TestGame::testGetPlayer()
{
	;
}

void TestGame::testGetReferee()
{
	Game* game = createNewGame();
	CPPUNIT_ASSERT(NULL != game->getReferee());
	game->decref();
}

void TestGame::testGetRole()
{
	Game* game = createNewGame();
	CPPUNIT_ASSERT_EQUAL(RL_RED, game->getRole(game->getFirst()));
	CPPUNIT_ASSERT_EQUAL(RL_BLACK, game->getRole(game->getNext()));
	game->decref();
}

void TestGame::testGetStatus()
{
	Game* game = createNewGame();
	CPPUNIT_ASSERT_EQUAL(Game::PAUSED, game->getStatus());
	game->setStatus(Game::PLAYING);
	CPPUNIT_ASSERT_EQUAL(Game::PLAYING, game->getStatus());
	game->decref();
}
