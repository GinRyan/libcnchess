/*
 * File:   TestStdCoordinator.cc
 * Author: alec
 *
 * Created on 2012-5-28, 11:08:17
 */

#include "TestStdCoordinator.h"
#include "src/include/cnchess/stdcoordinator.h"
#include "cnchess.h"
#include "utility.h"

using namespace std;
using namespace cnchess;

class PlayerGiveUp1 : public GamePlayer {
public:
	
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		/* 认输 */
		if (Coordinator::INVALID == coordinator.requestGiveUp(this, GURSON_LOSED)) {
			return ERROR;
		} else {
			return IGNORED;
		}
	}
	
};

class PlayerGiveUp2 : public GamePlayer {
public:
	
	bool got_giveup_req;
	
	PlayerGiveUp2() : got_giveup_req(false) {
		
	}
	
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		if (ac.action == AC_REQ_GIVEUP) {
			got_giveup_req = true;
			return HANDLED;
		} else {
			return ERROR;
		}
	}
	
};

class PlayerDraw1 : public GamePlayer {
public:
	
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		/* 求和 */
		if (Coordinator::INVALID == coordinator.requestDraw(this)) {
			return ERROR;
		} else {
			return IGNORED;
		}
	}
	
};

class PlayerDraw2 : public GamePlayer {
public:
	
	bool allow_draw;
	
	PlayerDraw2(bool allow_draw) : allow_draw(allow_draw) {
		
	}
	
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		if (ac.action == AC_REQ_DRAW) {
			if (allow_draw) {
				return HANDLED;
			} else {
				return IGNORED;
			}
		} else {
			return ERROR;
		}
	}
	
};

class PlayerRetract1 : public GamePlayer {
public:
	
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		/* 求和 */
		if (Coordinator::REFUSE == coordinator.requestRetract(this)) {
			return IGNORED;
		} else {
			return HANDLED;
		}
	}
	
};

class PlayerRetract2 : public GamePlayer {
public:
	
	bool allow;
	
	PlayerRetract2(bool allow) : allow(allow) {
		
	}
	
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		if (ac.action == AC_REQ_RETRACT) {
			if (allow) {
				return HANDLED;
			} else {
				return IGNORED;
			}
		} else {
			return ERROR;
		}
	}
	
};


CPPUNIT_TEST_SUITE_REGISTRATION(TestStdCoordinator);

TestStdCoordinator::TestStdCoordinator()
{
}

TestStdCoordinator::~TestStdCoordinator()
{
}

void TestStdCoordinator::setUp()
{
}

void TestStdCoordinator::tearDown()
{
}

void TestStdCoordinator::testMove()
{
	;
}

void TestStdCoordinator::testRequestDraw()
{
	ChessBook cb;
	stdrule_initchessbook(cb);
	PlayerDraw1* red = new PlayerDraw1();
	PlayerDraw2* black = new PlayerDraw2(true);
	PlayerDraw2* black2 = new PlayerDraw2(false);
	Game* game = new Game(cb, red, black, new DummyReferee);
	game->setStatus(Game::PLAYING);
	
	StdCoordinator coo(game);
	
	/* 请求红方下棋, 这里红方会直接认输 */
	CPPUNIT_ASSERT_EQUAL(Coordinator::REFUSE, coo.requestMove(black));
	
	/* 黑方应该收到红方的求和请求并同意和棋, 游戏应该结束 */
	CPPUNIT_ASSERT(coo.isOver());
	/* 对已经结束的游戏再次请求将被视为无效请求 */
	CPPUNIT_ASSERT_EQUAL(Coordinator::INVALID, coo.requestMove(red));
	
	game->decref();
	
	stdrule_initchessbook(cb);
	red = new PlayerDraw1();
	Game* game2 = new Game(cb, red, black2, new DummyReferee);
	game2->setStatus(Game::PLAYING);
	StdCoordinator coo2(game2);
	/* 请求红方下棋, 这里红方会直接认输 */
	CPPUNIT_ASSERT_EQUAL(Coordinator::REFUSE, coo2.requestMove(black2));
	
	/* 黑方应该收到红方的求和请求, 但是黑方不同意和棋, 游戏应该不会结束 */
	CPPUNIT_ASSERT(!coo2.isOver());
	
	game2->decref();
}

void TestStdCoordinator::testRequestGiveUp()
{
	ChessBook cb;
	stdrule_initchessbook(cb);
	PlayerGiveUp1* red = new PlayerGiveUp1();
	PlayerGiveUp2* black = new PlayerGiveUp2();
	Game* game = new Game(cb, red, black, new DummyReferee);
	game->setStatus(Game::PLAYING);
	
	StdCoordinator coo(game);
	
	/* 请求红方下棋, 这里红方会直接认输 */
	CPPUNIT_ASSERT_EQUAL(Coordinator::REFUSE, coo.requestMove(black));
	
	/* 黑方应该收到红方的认输请求 */
	CPPUNIT_ASSERT(black->got_giveup_req);
	/* 游戏应该结束 */
	CPPUNIT_ASSERT(coo.isOver());
	/* 对已经结束的游戏再次请求将被视为无效请求 */
	CPPUNIT_ASSERT_EQUAL(Coordinator::INVALID, coo.requestMove(red));
	
	game->decref();
}

void TestStdCoordinator::testRequestMove()
{
	;
}

void TestStdCoordinator::testRequestRetract()
{
	ChessBook cb;
	stdrule_initchessbook(cb);
	PlayerRetract1* red = new PlayerRetract1();
	PlayerRetract2* black = new PlayerRetract2(true);
	PlayerRetract2* black2 = new PlayerRetract2(false);
	Game* game = new Game(cb, red, black, new DummyReferee);
	game->setStatus(Game::PLAYING);
	
	StdCoordinator coo(game);
	
	/* 请求红方下棋, 这里红方会直接悔棋, 黑方同意悔棋 */
	CPPUNIT_ASSERT_EQUAL(Coordinator::AGREE, coo.requestMove(black));
	game->decref();
	
	stdrule_initchessbook(cb);
	red = new PlayerRetract1();
	Game* game2 = new Game(cb, red, black2, new DummyReferee);
	game2->setStatus(Game::PLAYING);
	StdCoordinator coo2(game2);
	/* 请求红方下棋, 这里红方会直接悔棋, 黑方不同意悔棋 */
	CPPUNIT_ASSERT_EQUAL(Coordinator::REFUSE, coo2.requestMove(black2));
	
	game2->decref();
}

