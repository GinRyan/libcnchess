/*
 * File:   TestChessBook.cc
 * Author: alec
 *
 * Created on 2012-5-15, 17:54:00
 */

#include "TestChessBook.h"
#include "src/include/cnchess/chessbook.h"
#include "cnchess.h"

using namespace std;
using namespace cnchess;

CPPUNIT_TEST_SUITE_REGISTRATION(TestChessBook);

TestChessBook::TestChessBook()
{
}

TestChessBook::~TestChessBook()
{
}

void TestChessBook::setUp()
{
}

void TestChessBook::tearDown()
{
}

void TestChessBook::testChessBook()
{
	cnchess::ChessBook cb;
	if (true /*check result*/) {
		for (int i = 0; i < cb.size(); i++) {
			for (int j = 0; j < cb[i].size(); j++) {
				CPPUNIT_ASSERT(cb[i][j].isType(ChessPiece::NONE));
			}
		}
	}
}

void TestChessBook::testChessBook2()
{
	ChessBook other;
	cnchess::ChessBook chessBook(other);
	if (true /*check result*/) {
		CPPUNIT_ASSERT(chessBook.equal(other));
	}
}

void TestChessBook::testClear()
{
	cnchess::ChessBook chessBook;
	chessBook[0][1].setType(ChessPiece::BISHOP);
	CPPUNIT_ASSERT(!chessBook.isEmpty());
	chessBook.clear();
	if (true /*check result*/) {
		CPPUNIT_ASSERT(chessBook.isEmpty());
	}
}

void TestChessBook::testEqual()
{
	if (true /*check result*/) {
		ChessBook b;
		ChessBook b1;
		CPPUNIT_ASSERT(b.equal(b1));
		b[0][1].setType(ChessPiece::BISHOP);
		CPPUNIT_ASSERT(!b.equal(b1));
		b1[0][1].setType(ChessPiece::BISHOP);
		CPPUNIT_ASSERT(b.equal(b1));
	}
}

void TestChessBook::testIsEmpty()
{
	cnchess::ChessBook cb;
	bool result = cb.isEmpty();
	if (true /*check result*/) {
		CPPUNIT_ASSERT(cb.isEmpty());
		cb[0][1].setType(ChessPiece::BISHOP);
		CPPUNIT_ASSERT(!cb.isEmpty());
	}
}

void TestChessBook::testSize()
{
	cnchess::ChessBook chessBook;
	std::size_t result = chessBook.size();
	if (true /*check result*/) {
		CPPUNIT_ASSERT_EQUAL((size_t) CHESSBOOK_ROW, result);
	}
}

void TestChessBook::testGet()
{
	cnchess::ChessBook cb;
	stdrule_initchessbook(cb);
	PiecePos pos(4, 3); // 红中兵
	CPPUNIT_ASSERT(cb.get(pos).isType(ChessPiece::PAWN));
}

void TestChessBook::testLocate()
{
	cnchess::ChessBook cb;
	stdrule_initchessbook(cb);
	PiecePos pos;
	ChessPiece other_piece;
	CPPUNIT_ASSERT(!cb.locate(pos, other_piece));
	CPPUNIT_ASSERT(cb.locate(pos, cb[3][4]));
	CPPUNIT_ASSERT(cb.get(pos).isType(ChessPiece::PAWN));
}

void TestChessBook::testContains()
{
	cnchess::ChessBook cb;
	stdrule_initchessbook(cb);
	ChessPiece other_piece;
	CPPUNIT_ASSERT(!cb.contains(other_piece));
	CPPUNIT_ASSERT(cb.contains(cb[3][4]));
}

void TestChessBook::testMove()
{
	cnchess::ChessBook cb;
	stdrule_initchessbook(cb);
	
	// bad move
	// 移动的棋子是空棋
	CPPUNIT_ASSERT(!cb.move(cb[1][0], 0, 0));
	// 移动终点无效
	CPPUNIT_ASSERT(!cb.move(cb[3][4], 40, 14));
	
	// 红中兵向前一步
	CPPUNIT_ASSERT(cb.move(cb[3][4], 4, 4));
	CPPUNIT_ASSERT(cb[4][4].isType(ChessPiece::PAWN));
	
	// 红帅往左一步吃掉自己的士
	CPPUNIT_ASSERT(cb.move(cb[0][4], 3, 0));
	CPPUNIT_ASSERT(cb[0][3].isType(ChessPiece::KING));
}