/*
 * File:   TestChessRow.cc
 * Author: alec
 *
 * Created on 2012-5-15, 17:19:12
 */

#include "TestChessRow.h"
#include "src/include/cnchess/chessrow.h"
#include "cnchess.h"

using namespace std;
using namespace cnchess;

CPPUNIT_TEST_SUITE_REGISTRATION(TestChessRow);

TestChessRow::TestChessRow()
{
}

TestChessRow::~TestChessRow()
{
}

void TestChessRow::setUp()
{
}

void TestChessRow::tearDown()
{
}

void TestChessRow::testChessRow()
{
	if (true /*check result*/) {
		ChessRow r;
		ChessRow r1;
		r1[1].setType(ChessPiece::BISHOP);
		r = r1;
		CPPUNIT_ASSERT(r[1].isType(ChessPiece::BISHOP));
		
		// test for operator[]
		ChessRow r3;
		r3[1].setType(ChessPiece::BISHOP);
		CPPUNIT_ASSERT_EQUAL(ChessPiece::BISHOP, r3[1].getType());
	}
}

void TestChessRow::testChessRow2()
{
	ChessRow other;
	other[1].setType(ChessPiece::BISHOP);
	cnchess::ChessRow chessRow(other);
	if (true /*check result*/) {
		CPPUNIT_ASSERT(chessRow.equal(other));
	}
}

void TestChessRow::testClear()
{
	cnchess::ChessRow chessRow;
	chessRow[1].setType(ChessPiece::BISHOP);
	CPPUNIT_ASSERT(!chessRow.isEmpty());
	chessRow.clear();
	if (true /*check result*/) {
		CPPUNIT_ASSERT(chessRow.isEmpty());
	}
}

void TestChessRow::testEqual()
{
	ChessRow r;
	ChessRow r1;
	if (true /*check result*/) {
		CPPUNIT_ASSERT(r.equal(r1));
		r[1].setType(ChessPiece::BISHOP);
		CPPUNIT_ASSERT(!r.equal(r1));
		r1[1].setType(ChessPiece::BISHOP);
		CPPUNIT_ASSERT(r.equal(r1));
	}
}

void TestChessRow::testIsEmpty()
{
	cnchess::ChessRow chessRow;
	bool result = chessRow.isEmpty();
	if (true /*check result*/) {
		CPPUNIT_ASSERT(result);
		chessRow[1].setType(ChessPiece::BISHOP);
		CPPUNIT_ASSERT(!chessRow.isEmpty());
	}
}

void TestChessRow::testSize()
{
	ChessRow r;
	if (true /*check result*/) {
		CPPUNIT_ASSERT_EQUAL((size_t)CHESSBOOK_COL, r.size());
	}
}

