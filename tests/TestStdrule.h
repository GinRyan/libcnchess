/*
 * File:   TestStdrule.h
 * Author: alec
 *
 * Created on 2012-6-14, 9:32:08
 */

#ifndef TESTSTDRULE_H
#define	TESTSTDRULE_H

#include <cppunit/extensions/HelperMacros.h>

class TestStdrule : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestStdrule);

	CPPUNIT_TEST(testStdrule_genpath);
	CPPUNIT_TEST(testStdrule_initchessbook);
	CPPUNIT_TEST(testStdrule_posvalid);
	CPPUNIT_TEST(testStdrule_whowin);

	CPPUNIT_TEST_SUITE_END();

public:
	TestStdrule();
	virtual ~TestStdrule();
	void setUp();
	void tearDown();

private:
	void testStdrule_genpath();
	void testStdrule_initchessbook();
	void testStdrule_posvalid();
	void testStdrule_whowin();

};

#endif	/* TESTSTDRULE_H */

