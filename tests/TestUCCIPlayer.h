/*
 * File:   TestUCCIPlayer.h
 * Author: alec
 *
 * Created on 2012-6-18, 15:44:19
 */

#ifndef TESTUCCIPLAYER_H
#define	TESTUCCIPLAYER_H

#include <cppunit/extensions/HelperMacros.h>

class TestUCCIPlayer : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestUCCIPlayer);

	CPPUNIT_TEST(testUCCIPlayer);
	CPPUNIT_TEST(testOnGameOver);
	CPPUNIT_TEST(testOnGameStart);
	CPPUNIT_TEST(testRequestHandle);

	CPPUNIT_TEST_SUITE_END();

public:
	TestUCCIPlayer();
	virtual ~TestUCCIPlayer();
	void setUp();
	void tearDown();

private:
	void testUCCIPlayer();
	void testOnGameOver();
	void testOnGameStart();
	void testRequestHandle();

};

#endif	/* TESTUCCIPLAYER_H */

