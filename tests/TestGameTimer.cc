/*
 * File:   TestGameTimer.cc
 * Author: alec
 *
 * Created on 2012-5-17, 15:01:32
 */

#include "TestGameTimer.h"
#include "src/include/cnchess/gametimer.h"
#include "cnchess.h"
#include "utility.h"


using namespace std;
using namespace cnchess;

void dummy_expired_listener(const GameTimer* timer, void* args, GameTimer::ExpiredType expire_type)
{
	
}

void print_expired_listener(const GameTimer* timer, void* args, GameTimer::ExpiredType expire_type)
{
	printf("dummy_expired_listener fire\n");
}

static volatile int g_counter = 0;

void timer_expired_listener(const GameTimer* timer, void* args, GameTimer::ExpiredType expire_type)
{
	g_counter ++;
}

CPPUNIT_TEST_SUITE_REGISTRATION(TestGameTimer);

TestGameTimer::TestGameTimer()
{
}

TestGameTimer::~TestGameTimer()
{
}

void TestGameTimer::setUp()
{
}

void TestGameTimer::tearDown()
{
}

void TestGameTimer::testGameTimer()
{
	long seconds = 0;
	CPPUNIT_ASSERT_THROW(new GameTimer(seconds), invalid_argument);
	GameTimer t1(1, 2);
	CPPUNIT_ASSERT_EQUAL(1L, t1.preStepTime());
	GameTimer t2(1, -1);
	CPPUNIT_ASSERT_EQUAL(0L, t2.preStepTime());
	GameTimer t3(1, 0);
	CPPUNIT_ASSERT_EQUAL(0L, t3.preStepTime());
}

void TestGameTimer::testAddExpiredListener()
{
	cnchess::GameTimer timer(2);
	timer.addListener(timer_expired_listener);
	g_counter = 0;
	timer.start();
	sleepfor(1000 * 3);
	CPPUNIT_ASSERT(g_counter);
}

void TestGameTimer::testGetExpiredListeners()
{
	;
}

void TestGameTimer::testGetPlayer()
{
	;
}

void TestGameTimer::testGetStatus()
{
	cnchess::GameTimer timer(5);
	int result = timer.getStatus();
	CPPUNIT_ASSERT(result ==PAUSED);
}

void TestGameTimer::testPreStepRemainTime()
{
	// 没设置 pre_step_time
	cnchess::GameTimer t1(3);
	CPPUNIT_ASSERT_EQUAL(-1L, t1.preStepRemainTime());
	
	// 未 start()
	cnchess::GameTimer t2(3, 3);
	CPPUNIT_ASSERT_EQUAL(-1L, t2.preStepRemainTime());
	
	cnchess::GameTimer t3(10, 5);
	t3.start();
	sleepfor(1000 * 3);
	// 5 -3 = 2L
	CPPUNIT_ASSERT_EQUAL(2L, t3.preStepRemainTime());
	//t3.press();
	
	cnchess::GameTimer t4(5, 4);
	t4.start();
	sleepfor(1000 * 4);
	CPPUNIT_ASSERT_EQUAL(1L, t4.remainTime());
	t4.press();
	t4.start();
	// 5 - 4 = 1L
	CPPUNIT_ASSERT_EQUAL(1L, t4.preStepRemainTime());
}

void TestGameTimer::testPreStepTime()
{
	;
}

void TestGameTimer::testPress()
{
	cnchess::GameTimer t1(5, 4);
	t1.start();
	sleepfor(1000 * 1);
	t1.press();
	CPPUNIT_ASSERT_EQUAL(4L, t1.remainTime());
	sleepfor(1000 * 2);
	CPPUNIT_ASSERT_EQUAL(4L, t1.remainTime());
}

void TestGameTimer::testRemainTime()
{
	;
}

void TestGameTimer::testRemoveExpiredListener()
{
	;
}

void TestGameTimer::testStart()
{
	GameTimer t1(50, 4);
	for (int i = 0; i < 100; i ++) {
		t1.press();
		t1.press();
		CPPUNIT_ASSERT_EQUAL((int)PAUSED, t1.getStatus());
		t1.start();
		CPPUNIT_ASSERT_EQUAL((int)STARTED, t1.getStatus());
		t1.start();
		CPPUNIT_ASSERT_EQUAL((int)STARTED, t1.getStatus());
		t1.press();
		CPPUNIT_ASSERT_EQUAL((int)PAUSED, t1.getStatus());
		t1.start();
		CPPUNIT_ASSERT_EQUAL((int)STARTED, t1.getStatus());
		t1.press();
	}
}

void TestGameTimer::testTotalTime()
{
	cnchess::GameTimer timer(5);
	long result = timer.totalTime();
	CPPUNIT_ASSERT_EQUAL(5L, result);
}

void TestGameTimer::testOvertime()
{
	cnchess::GameTimer timer(1);
	timer.start();
	sleepfor(1000);
	CPPUNIT_ASSERT_EQUAL((int)EXPIRED, timer.getStatus());
	timer.overtime(1);
	CPPUNIT_ASSERT_EQUAL((int)PAUSED, timer.getStatus());
	CPPUNIT_ASSERT_EQUAL(2L, timer.totalTime());
	CPPUNIT_ASSERT_EQUAL(1L, timer.remainTime());
	timer.start();
	timer.overtime(5);
	sleepfor(3000);
	CPPUNIT_ASSERT_EQUAL(3L, timer.remainTime());
}
