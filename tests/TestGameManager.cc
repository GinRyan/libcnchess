/*
 * File:   TestGameManager.cc
 * Author: alec
 *
 * Created on 2012-5-24, 11:37:50
 */

#include "TestGameManager.h"
#include "src/include/cnchess/gamemanager.h"
#include "cnchess.h"
#include "utility.h"

using namespace std;
using namespace cnchess;

class PlayerDraw1 : public GamePlayer {
public:
	
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		switch (ac.action) {
		case AC_REQ_DRAW:
			return HANDLED;
		default:
			return ERROR;
		}
	}
	
};

class RedPlayer : public GamePlayer {

public:
	
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		ICCSMotion motion;
		switch (ac.action) {
		case AC_REQ_MOVE:
			timer.start();
			
			iccsmotion_parse(motion, "h7e7");
			if (Coordinator::AGREE == coordinator.move(this, motion)) {
				sleepfor(2000);
				timer.press();
				pdebug("red agree");
				return HANDLED;
			} else {
				pdebug("red refuse");
				return IGNORED;
			}
		case AC_REQ_DRAW:
			return HANDLED;
		case AC_REQ_RETRACT:
			return HANDLED;
		}
	}
	
};

class BlackPlayer : public GamePlayer {

public:

	int requestHandle(const GameAction& ac, Coordinator& coordinator, IActionTimer& timer)
	{
		pdebug("black handler");
		ICCSMotion motion;
		switch (ac.action) {
		case AC_REQ_MOVE:
			timer.start();
			
			iccsmotion_parse(motion, "e7h7");
			if (Coordinator::AGREE == coordinator.move(this, motion)) {
				timer.press();
				pdebug("block agree");
				return HANDLED;
			} else {
				pdebug("block refuse");
				return IGNORED;
			}
		case AC_REQ_DRAW:
			return HANDLED;
		case AC_REQ_RETRACT:
			return HANDLED;
		}
	}

private:
	const Game* game;
};


CPPUNIT_TEST_SUITE_REGISTRATION(TestGameManager);

TestGameManager::TestGameManager()
{
}

TestGameManager::~TestGameManager()
{
}

void TestGameManager::setUp()
{
}

void TestGameManager::tearDown()
{
}

void TestGameManager::testStart()
{
	GamePlayer* red = new RedPlayer();
	GamePlayer* black = new BlackPlayer();

	ChessBook cb;
	stdrule_initchessbook(cb);
	Game* game = new Game(cb, red, black, new DummyReferee);

	GameManager gmgr(game);

//	gmgr.setExpiredHandler(expired_handler);
//	gmgr.addActionListener(action_listener);

	Game::Status gamest = gmgr.start();
}

void TestGameManager::testStop()
{
	;
}

