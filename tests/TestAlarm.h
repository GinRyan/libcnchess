/*
 * File:   TestAlarm.h
 * Author: alec
 *
 * Created on 2012-5-19, 3:24:25
 */

#ifndef TESTALARM_H
#define	TESTALARM_H

#include <cppunit/extensions/HelperMacros.h>

class TestAlarm : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestAlarm);

	CPPUNIT_TEST(testAlarm);
	CPPUNIT_TEST(testGetExpireTime);
	CPPUNIT_TEST(testGetRemainTime);
	CPPUNIT_TEST(testStart);
	CPPUNIT_TEST(testStop);

	CPPUNIT_TEST_SUITE_END();

public:
	TestAlarm();
	virtual ~TestAlarm();
	void setUp();
	void tearDown();

private:
	void testAlarm();
	void testGetExpireTime();
	void testGetRemainTime();
	void testStart();
	void testStop();

};

#endif	/* TESTALARM_H */

