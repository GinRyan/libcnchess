/*
 * File:   TestFEN.cc
 * Author: alec
 *
 * Created on 2012-5-15, 18:16:16
 */

#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include <sstream>
#include <errno.h>
#include <err.h>
#include <sys/types.h>
#include <unistd.h>
#include "TestFEN.h"
#include "src/include/cnchess/fen.h"
#include "cnchess.h"

#define TEST_FEN_STRING1	"rnbakabnr/9/1c5c1/p1p1p1p1p/9/9/P1P1P1P1P/1C5C1/9/RNBAKABNR"
#define TEST_FEN_STRING2	"9/9/9/9/9/9/9/9/9/9"
#define TEST_FEN_STRING3	"9/9/9/9/9/9/9/9/9/9"
#define TEST_FEN_STRING4	"r8/R8/9/9/9/9/9/9/9/9"

using namespace std;
using namespace cnchess;

void check_row_is_empty(const ChessRow& r, int start, int count)
{
	for (start; start < count; start++) {
		CPPUNIT_ASSERT(r[start].isType(ChessPiece::NONE));
	}
}

void check_cb_for_fenstring1(const ChessBook& cb)
{
	// row: rnbakabnr
	CPPUNIT_ASSERT(cb[0][0].isType(char2piece('r')));
	CPPUNIT_ASSERT(cb[0][1].isType(char2piece('n')));
	CPPUNIT_ASSERT(cb[0][2].isType(char2piece('b')));
	CPPUNIT_ASSERT(cb[0][3].isType(char2piece('a')));
	CPPUNIT_ASSERT(cb[0][4].isType(char2piece('k')));
	CPPUNIT_ASSERT(cb[0][5].isType(char2piece('a')));
	CPPUNIT_ASSERT(cb[0][6].isType(char2piece('b')));
	CPPUNIT_ASSERT(cb[0][7].isType(char2piece('n')));
	CPPUNIT_ASSERT(cb[0][8].isType(char2piece('r')));

	// row: 9
	check_row_is_empty(cb[1], 0, 9);

	// row: 1c5c1
	CPPUNIT_ASSERT(cb[2][0].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[2][1].isType(char2piece('c')));
	check_row_is_empty(cb[2], 2, 5);
	CPPUNIT_ASSERT(cb[2][7].isType(char2piece('c')));
	CPPUNIT_ASSERT(cb[2][8].isType(ChessPiece::NONE));

	// row: p1p1p1p1p
	CPPUNIT_ASSERT(cb[3][0].isType(char2piece('p')));
	CPPUNIT_ASSERT(cb[3][1].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[3][2].isType(char2piece('p')));
	CPPUNIT_ASSERT(cb[3][3].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[3][4].isType(char2piece('p')));
	CPPUNIT_ASSERT(cb[3][5].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[3][6].isType(char2piece('p')));
	CPPUNIT_ASSERT(cb[3][7].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[3][6].isType(char2piece('p')));
	
	// row: 9
	check_row_is_empty(cb[4], 0, 9);

	// row: 9
	check_row_is_empty(cb[5], 0, 9);

	// row: p1p1p1p1p
	CPPUNIT_ASSERT(cb[6][0].isType(char2piece('p')));
	CPPUNIT_ASSERT(cb[6][1].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[6][2].isType(char2piece('p')));
	CPPUNIT_ASSERT(cb[6][3].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[6][4].isType(char2piece('p')));
	CPPUNIT_ASSERT(cb[6][5].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[6][6].isType(char2piece('p')));
	CPPUNIT_ASSERT(cb[6][7].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[6][6].isType(char2piece('p')));

	// row: 1c5c1
	CPPUNIT_ASSERT(cb[7][0].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[7][1].isType(char2piece('c')));
	check_row_is_empty(cb[7], 2, 5);
	CPPUNIT_ASSERT(cb[7][7].isType(char2piece('c')));
	CPPUNIT_ASSERT(cb[7][8].isType(ChessPiece::NONE));

	// row: 9
	check_row_is_empty(cb[8], 0, 9);

	// row: rnbakabnr
	CPPUNIT_ASSERT(cb[9][0].isType(char2piece('r')));
	CPPUNIT_ASSERT(cb[9][1].isType(char2piece('n')));
	CPPUNIT_ASSERT(cb[9][2].isType(char2piece('b')));
	CPPUNIT_ASSERT(cb[9][3].isType(char2piece('a')));
	CPPUNIT_ASSERT(cb[9][4].isType(char2piece('k')));
	CPPUNIT_ASSERT(cb[9][5].isType(char2piece('a')));
	CPPUNIT_ASSERT(cb[9][6].isType(char2piece('b')));
	CPPUNIT_ASSERT(cb[9][7].isType(char2piece('n')));
	CPPUNIT_ASSERT(cb[9][8].isType(char2piece('r')));
	
	for (int i = 0; i < 5; i ++) {
		for (int j = 0; j < ChessRow::size(); j ++) {
			if (cb[i][j].isType(ChessPiece::NONE)) {
				continue;
			}
			CPPUNIT_ASSERT_EQUAL(RL_BLACK, cb[i][j].getOwner());
		}
	}
	
	for (int i = 5; i < ChessBook::size(); i ++) {
		for (int j = 0; j < ChessRow::size(); j ++) {
			if (cb[i][j].isType(ChessPiece::NONE)) {
				continue;
			}
			CPPUNIT_ASSERT_EQUAL(RL_RED, cb[i][j].getOwner());
		}
	}
}

CPPUNIT_TEST_SUITE_REGISTRATION(TestFEN);

TestFEN::TestFEN()
{
}

TestFEN::~TestFEN()
{
}

void TestFEN::setUp()
{
}

void TestFEN::tearDown()
{
}

void TestFEN::testChar2piece()
{
	CPPUNIT_ASSERT_EQUAL(ChessPiece::ADVISOR, char2piece('a'));
	CPPUNIT_ASSERT_EQUAL(ChessPiece::CANNON, char2piece('c'));
	CPPUNIT_ASSERT_EQUAL(ChessPiece::NONE, char2piece('X')); // 'x' invalid
}

/**
 * test for fen_parse for string 
 */
void TestFEN::testFen_parse()
{

	ChessBook cb;
	ChessBook *pcb;
	pcb = fen_parse(cb, TEST_FEN_STRING1);
	CPPUNIT_ASSERT(pcb);
	check_cb_for_fenstring1(cb);

	// test for bad string
	CPPUNIT_ASSERT(NULL == fen_parse("/9/9/9/9/9/9/9/9/9/9"));
	CPPUNIT_ASSERT(NULL == fen_parse("9/9/9/9/9/9/9/9/9/9/"));
	CPPUNIT_ASSERT(NULL == fen_parse("9/9/9/9/9/9 /9/9/9/9"));
	CPPUNIT_ASSERT(NULL == fen_parse("8X/9/9/9/9/9/9/9/9/9"));
	CPPUNIT_ASSERT(NULL == fen_parse("0/9/9/9/9/9/9/9/9/9"));
	CPPUNIT_ASSERT(NULL == fen_parse("10/9/9/9/9/9/9/9/9/9"));
	
	cb.clear();
	// "r8/R8/9/9/9/9/9/9/9/9"
	CPPUNIT_ASSERT(fen_parse(cb, TEST_FEN_STRING4));
	CPPUNIT_ASSERT_EQUAL(ChessPiece::ROOK, cb[0][0].getType());
	CPPUNIT_ASSERT_EQUAL(ChessPiece::ROOK, cb[1][0].getType());
	CPPUNIT_ASSERT_EQUAL(RL_BLACK, cb[0][0].getOwner());
	CPPUNIT_ASSERT_EQUAL(RL_RED, cb[1][0].getOwner());
}

void TestFEN::testFen_parse2()
{
	char fen[] = TEST_FEN_STRING1;
	FILE *fp = fmemopen(fen, strlen(fen), "r");
	CPPUNIT_ASSERT(fp);
	
	ChessBook cb;
	ChessBook *pcb;
	pcb = fen_parse(cb, fp);
	fclose(fp);
	CPPUNIT_ASSERT(pcb);
	check_cb_for_fenstring1(cb);
}

void TestFEN::testFen_parse3()
{
	char tmpfile_tpl[] = "/tmp/test_fenXXXXXX";
	int fd = mkstemp(tmpfile_tpl);
	char fen[] = TEST_FEN_STRING1;
	write(fd, fen, strlen(fen) + 1);
	lseek(fd, SEEK_SET, 0);
	ChessBook cb;
	ChessBook *pcb;
	pcb = fen_parse(cb, fd);
	CPPUNIT_ASSERT(pcb);
	close(fd);
	check_cb_for_fenstring1(cb);
	
	CPPUNIT_ASSERT_THROW(fen_parse(cb, -1), invalid_argument);
}

void TestFEN::testFen_parse4()
{
	stringstream in(stringstream::in | stringstream::out);
	in << TEST_FEN_STRING1;
	ChessBook cb;
	ChessBook *pcb;
	pcb = fen_parse(cb, in);
	CPPUNIT_ASSERT(pcb);
	check_cb_for_fenstring1(cb);
}

void TestFEN::testFen_parse5()
{
	;
}

void TestFEN::testFen_parse6()
{
	;
}

void TestFEN::testFen_parse7()
{
	;
}

void TestFEN::testFen_parse8()
{
	;
}

void TestFEN::testFen_tostring()
{
	ChessBook cb;
	fen_parse(cb, TEST_FEN_STRING1);
	string res;
	CPPUNIT_ASSERT_EQUAL(string(TEST_FEN_STRING1), fen_tostring(res, cb));
	
	cb.clear();
	CPPUNIT_ASSERT_EQUAL(string(TEST_FEN_STRING2), fen_tostring(res, cb));
}

void TestFEN::testFen_tostring2()
{
	ChessBook cb;
	fen_parse(cb, TEST_FEN_STRING1);
	CPPUNIT_ASSERT_EQUAL(string(TEST_FEN_STRING1), fen_tostring(cb));
	
	cb.clear();
	CPPUNIT_ASSERT_EQUAL(string(TEST_FEN_STRING2), fen_tostring(cb));
}

void TestFEN::testIs_piece()
{

	CPPUNIT_ASSERT(is_piece('k'));
	CPPUNIT_ASSERT(is_piece('a'));
	CPPUNIT_ASSERT(is_piece('b'));
	CPPUNIT_ASSERT(is_piece('n'));
	CPPUNIT_ASSERT(is_piece('r'));
	CPPUNIT_ASSERT(is_piece('c'));
	CPPUNIT_ASSERT(is_piece('p'));
	CPPUNIT_ASSERT(is_piece('K'));
	CPPUNIT_ASSERT(is_piece('A'));
	CPPUNIT_ASSERT(is_piece('B'));
	CPPUNIT_ASSERT(is_piece('N'));
	CPPUNIT_ASSERT(is_piece('R'));
	CPPUNIT_ASSERT(is_piece('C'));
	CPPUNIT_ASSERT(is_piece('P'));
	CPPUNIT_ASSERT(is_piece(0));
	CPPUNIT_ASSERT(is_piece('\0'));
	
	CPPUNIT_ASSERT(!is_piece('x'));
}

void TestFEN::testPiece2chiness()
{
	;
}

