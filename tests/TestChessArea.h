/*
 * File:   TestChessArea.h
 * Author: alec
 *
 * Created on 2012-6-13, 11:24:38
 */

#ifndef TESTCHESSAREA_H
#define	TESTCHESSAREA_H

#include <cppunit/extensions/HelperMacros.h>

class TestChessArea : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestChessArea);

	CPPUNIT_TEST(testChessArea);
	CPPUNIT_TEST(testChessarea_contains);
	CPPUNIT_TEST(testChessarea_contains2);
	CPPUNIT_TEST(testChessarea_mkblack);
	CPPUNIT_TEST(testChessarea_mkking);
	CPPUNIT_TEST(testChessarea_mkred);
	CPPUNIT_TEST(testChessarea_torange);

	CPPUNIT_TEST_SUITE_END();

public:
	TestChessArea();
	virtual ~TestChessArea();
	void setUp();
	void tearDown();

private:
	void testChessArea();
	void testChessarea_contains();
	void testChessarea_contains2();
	void testChessarea_mkblack();
	void testChessarea_mkking();
	void testChessarea_mkred();
	void testChessarea_torange();

};

#endif	/* TESTCHESSAREA_H */

