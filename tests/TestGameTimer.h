/*
 * File:   TestGameTimer.h
 * Author: alec
 *
 * Created on 2012-5-17, 15:01:32
 */

#ifndef TESTGAMETIMER_H
#define	TESTGAMETIMER_H

#include <cppunit/extensions/HelperMacros.h>

class TestGameTimer : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestGameTimer);

	CPPUNIT_TEST(testGameTimer);
	CPPUNIT_TEST(testAddExpiredListener);
	CPPUNIT_TEST(testGetExpiredListeners);
	CPPUNIT_TEST(testGetPlayer);
	CPPUNIT_TEST(testGetStatus);
	CPPUNIT_TEST(testPreStepRemainTime);
	CPPUNIT_TEST(testPreStepTime);
	CPPUNIT_TEST(testPress);
	CPPUNIT_TEST(testRemainTime);
	CPPUNIT_TEST(testRemoveExpiredListener);
	CPPUNIT_TEST(testStart);
	CPPUNIT_TEST(testTotalTime);
	CPPUNIT_TEST(testOvertime);

	CPPUNIT_TEST_SUITE_END();

public:
	TestGameTimer();
	virtual ~TestGameTimer();
	void setUp();
	void tearDown();

private:
	void testGameTimer();
	void testAddExpiredListener();
	void testGetExpiredListeners();
	void testGetPlayer();
	void testGetStatus();
	void testPreStepRemainTime();
	void testPreStepTime();
	void testPress();
	void testRemainTime();
	void testRemoveExpiredListener();
	void testStart();
	void testTotalTime();
	void testOvertime();

};

#endif	/* TESTGAMETIMER_H */

