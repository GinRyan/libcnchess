/*
 * File:   TestChessRow.h
 * Author: alec
 *
 * Created on 2012-5-15, 17:19:11
 */

#ifndef TESTCHESSROW_H
#define	TESTCHESSROW_H

#include <cppunit/extensions/HelperMacros.h>

class TestChessRow : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestChessRow);

	CPPUNIT_TEST(testChessRow);
	CPPUNIT_TEST(testChessRow2);
	CPPUNIT_TEST(testClear);
	CPPUNIT_TEST(testEqual);
	CPPUNIT_TEST(testIsEmpty);
	CPPUNIT_TEST(testSize);

	CPPUNIT_TEST_SUITE_END();

public:
	TestChessRow();
	virtual ~TestChessRow();
	void setUp();
	void tearDown();

private:
	void testChessRow();
	void testChessRow2();
	void testClear();
	void testEqual();
	void testIsEmpty();
	void testSize();

};

#endif	/* TESTCHESSROW_H */

