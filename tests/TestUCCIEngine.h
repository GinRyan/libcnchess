/*
 * File:   TestUCCIEngine.h
 * Author: alec
 *
 * Created on 2012-6-18, 17:53:08
 */

#ifndef TESTUCCIENGINE_H
#define	TESTUCCIENGINE_H

#include <cppunit/extensions/HelperMacros.h>

class TestUCCIEngine : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestUCCIEngine);

	CPPUNIT_TEST(testUCCIEngine);
	CPPUNIT_TEST(testGetResponse);
	CPPUNIT_TEST(testGetStatus);
	CPPUNIT_TEST(testSendRequest);
	CPPUNIT_TEST(testStart);
	CPPUNIT_TEST(testStop);

	CPPUNIT_TEST_SUITE_END();

public:
	TestUCCIEngine();
	virtual ~TestUCCIEngine();
	void setUp();
	void tearDown();

private:
	void testUCCIEngine();
	void testGetResponse();
	void testGetStatus();
	void testSendRequest();
	void testStart();
	void testStop();

};

#endif	/* TESTUCCIENGINE_H */

