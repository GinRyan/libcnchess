/*
 * File:   TestChessBook.h
 * Author: alec
 *
 * Created on 2012-5-15, 17:54:00
 */

#ifndef TESTCHESSBOOK_H
#define	TESTCHESSBOOK_H

#include <cppunit/extensions/HelperMacros.h>

class TestChessBook : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestChessBook);

	CPPUNIT_TEST(testChessBook);
	CPPUNIT_TEST(testChessBook2);
	CPPUNIT_TEST(testClear);
	CPPUNIT_TEST(testEqual);
	CPPUNIT_TEST(testIsEmpty);
	CPPUNIT_TEST(testSize);
	CPPUNIT_TEST(testGet);
	CPPUNIT_TEST(testLocate);
	CPPUNIT_TEST(testContains);
	CPPUNIT_TEST(testMove);
	CPPUNIT_TEST_SUITE_END();

public:
	TestChessBook();
	virtual ~TestChessBook();
	void setUp();
	void tearDown();

private:
	void testChessBook();
	void testChessBook2();
	void testClear();
	void testEqual();
	void testIsEmpty();
	void testSize();
	void testGet();
	void testLocate();
	void testContains();
	void testMove();

};

#endif	/* TESTCHESSBOOK_H */

