/*
 * File:   TestChessPiece.h
 * Author: alec
 *
 * Created on 2012-5-15, 17:17:59
 */

#ifndef TESTCHESSPIECE_H
#define	TESTCHESSPIECE_H

#include <cppunit/extensions/HelperMacros.h>

class TestChessPiece : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestChessPiece);

	CPPUNIT_TEST(testChessPiece);
	CPPUNIT_TEST(testGetOwner);
	CPPUNIT_TEST(testGetType);
	CPPUNIT_TEST(testIsType);
	CPPUNIT_TEST(testSetOwner);
	CPPUNIT_TEST(testSetType);

	CPPUNIT_TEST_SUITE_END();

public:
	TestChessPiece();
	virtual ~TestChessPiece();
	void setUp();
	void tearDown();

private:
	void testChessPiece();
	void testGetOwner();
	void testGetType();
	void testIsType();
	void testSetOwner();
	void testSetType();

};

#endif	/* TESTCHESSPIECE_H */

