/*
 * File:   TestUCCIPlayer.cc
 * Author: alec
 *
 * Created on 2012-6-18, 15:44:19
 */

#include "TestUCCIPlayer.h"
#include "src/include/cnchess/ucciplayer.h"
#include "cnchess.h"
#include <stdexcept>

using namespace std;
using namespace cnchess;


class Player : public GamePlayer {
public:
	
	int requestHandle(const GameAction& ac, Coordinator& coordinator, 
		IActionTimer& timer)
	{
		/* 认输 */
		if (Coordinator::INVALID == coordinator.requestGiveUp(this, GURSON_LOSED)) {
			return ERROR;
		} else {
			return IGNORED;
		}
	}
	
};


CPPUNIT_TEST_SUITE_REGISTRATION(TestUCCIPlayer);

TestUCCIPlayer::TestUCCIPlayer()
{
}

TestUCCIPlayer::~TestUCCIPlayer()
{
}

void TestUCCIPlayer::setUp()
{
}

void TestUCCIPlayer::tearDown()
{
}

void TestUCCIPlayer::testUCCIPlayer()
{
	;
}

void TestUCCIPlayer::testOnGameOver()
{
	;
}

void TestUCCIPlayer::testOnGameStart()
{
	;
}

void TestUCCIPlayer::testRequestHandle()
{
	;
}

