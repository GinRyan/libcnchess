/*
 * File:   TestGameManager.h
 * Author: alec
 *
 * Created on 2012-5-24, 11:37:50
 */

#ifndef TESTGAMEMANAGER_H
#define	TESTGAMEMANAGER_H

#include <cppunit/extensions/HelperMacros.h>

class TestGameManager : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestGameManager);

	CPPUNIT_TEST(testStart);
	CPPUNIT_TEST(testStop);

	CPPUNIT_TEST_SUITE_END();

public:
	TestGameManager();
	virtual ~TestGameManager();
	void setUp();
	void tearDown();

private:
	void testStart();
	void testStop();

};

#endif	/* TESTGAMEMANAGER_H */

