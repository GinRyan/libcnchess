/*
 * File:   TestRealClock.cc
 * Author: alec
 *
 * Created on 2012-5-18, 17:22:44
 */

#include "TestRealClock.h"
#include "src/include/cnchess/clock.h"
#include "cnchess.h"
#include "utility.h"
#include <unistd.h>
#include "config.h"


using namespace std;
using namespace cnchess;

static volatile int g_counter = 0;

void counter_listenr(const IClock* clock, void* ctx)
{
	g_counter++;
}

CPPUNIT_TEST_SUITE_REGISTRATION(TestRealClock);

TestRealClock::TestRealClock()
{
}

TestRealClock::~TestRealClock()
{
}

void TestRealClock::setUp()
{
}

void TestRealClock::tearDown()
{
}

void TestRealClock::testRealClock()
{
	long interval = 1000 * 1000; // 1 second
	cnchess::RealClock* c1 = RealClock::create(interval);
	CPPUNIT_ASSERT_EQUAL(interval, ts2microsec(c1->getInterval()));
	c1->decref();

	interval = 1500000;
	RealClock* c2 = RealClock::create(interval);
	CPPUNIT_ASSERT_EQUAL(interval, ts2microsec(c2->getInterval()));
	c2->decref();
}

void TestRealClock::testRealClock2()
{
	timespec ts;
	ts.tv_sec = 1;
	ts.tv_nsec = 500000000;

	cnchess::RealClock* c1 = RealClock::createFromTS(ts);
	CPPUNIT_ASSERT_EQUAL(1500000L, ts2microsec(c1->getInterval()));
	c1->decref();
}

void TestRealClock::testAddListener()
{
	;
}

void TestRealClock::testGetInterval()
{
	;
}

void TestRealClock::testGetStatus()
{
	;
}

void TestRealClock::testRemoveListener()
{
	;
}

void TestRealClock::testStart()
{
	RealClock* c = RealClock::create(1000000);
	CPPUNIT_ASSERT_EQUAL(IDLE, c->getStatus());
	c->addListener(counter_listenr);
	c->start();
	CPPUNIT_ASSERT_EQUAL(STARTED, c->getStatus());
	sleepfor(1000 * 5);
	CPPUNIT_ASSERT_EQUAL((int) 5, (int) g_counter);
	c->decref();
}

void TestRealClock::testStop()
{
	RealClock* c = RealClock::create(1000000);
	g_counter = 0;
	c->addListener(counter_listenr);
	c->start();
	CPPUNIT_ASSERT_EQUAL(STARTED, c->getStatus());

	sleepfor(1000 * 2);

	c->stop();
	sleepfor(1000 * 2);
	CPPUNIT_ASSERT_EQUAL((int) 2, (int) g_counter);
	CPPUNIT_ASSERT_EQUAL(STOPPED, c->getStatus());
	for (int i = 0; i < 100; i++) {
		c->start();
		c->start();
		c->stop();
		c->stop();
	}
	c->decref();
}

void TestRealClock::testCreate()
{

	CPPUNIT_ASSERT_EQUAL((int) (CNCS_REALCLOCK_SIGMAX - CNCS_REALCLOCK_SIGMIN + 1),
		RealClock::maxClock());

	CPPUNIT_ASSERT_EQUAL(RealClock::maxClock(), RealClock::remainClock());

	RealClock* c = RealClock::create(1000000);
	CPPUNIT_ASSERT_EQUAL(RealClock::maxClock() - 1, RealClock::remainClock());
	c->decref();
	CPPUNIT_ASSERT_EQUAL(RealClock::maxClock(), RealClock::remainClock());

	for (int i = 0; i < RealClock::maxClock(); i++) {
		RealClock* c = RealClock::create(1000000);
		CPPUNIT_ASSERT_EQUAL((int) (CNCS_REALCLOCK_SIGMIN + i), c->getSignalNo());
		c->start();
	}
	RealClock* c4 = RealClock::create(1000000);
	CPPUNIT_ASSERT(c4 == NULL);
	CPPUNIT_ASSERT_EQUAL((int) 0, RealClock::remainClock());
}
