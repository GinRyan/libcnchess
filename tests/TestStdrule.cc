/*
 * File:   TestStdrule.cc
 * Author: alec
 *
 * Created on 2012-6-14, 9:32:09
 */

#include "TestStdrule.h"
#include "src/include/cnchess/stdrule.h"
#include "cnchess.h"

using namespace std;
using namespace cnchess;

bool inpath(const ICCSMotion& mymotion, const PathList& pathlist) {
	PiecePos start_pos;
	PiecePos end_pos;
	mymotion.start.toPiecePos(start_pos);
	mymotion.end.toPiecePos(end_pos);
	for (int i = 0; i < pathlist.size(); i++) {
		if (pathlist[i].start.equal(start_pos) && pathlist[i].end.equal(end_pos)) {
			return true;
		}
	}
	return false;
}

CPPUNIT_TEST_SUITE_REGISTRATION(TestStdrule);

TestStdrule::TestStdrule()
{
}

TestStdrule::~TestStdrule()
{
}

void TestStdrule::setUp()
{
}

void TestStdrule::tearDown()
{
}

void TestStdrule::testStdrule_genpath()
{
	ChessBook cb;
	stdrule_initchessbook(cb);
	PathList pathlist1;
	CPPUNIT_ASSERT(NULL == stdrule_getpath(pathlist1, cb, ChessPiece()));
		
	// 遍历棋盘上每一个棋子, 取得每一个棋子的可走路径, 
	// 然后检查每一个棋子的可走路径上的每一个点是否是有效可落子的点
//	for (int row = 9; row < CHESSBOOK_ROW; row ++) {
//		for (int col = 0; col < CHESSBOOK_COL; col ++) {
//			if (!cb[row][col].isType(ChessPiece::NONE)) {
//				PathList pathlist;
//				CPPUNIT_ASSERT(stdrule_getpath(pathlist, cb, cb[row][col]));
//				
//				for (int i = 0; i < pathlist.size(); i ++) {
//					CPPUNIT_ASSERT(stdrule_posvalid(pathlist[i].start, cb[row][col]));
//					CPPUNIT_ASSERT(stdrule_posvalid(pathlist[i].end, cb[row][col]));
//				}
//				
//			}
//		}
//	}
	
	PathList pathlist;
	// check for ROOK
	stdrule_initchessbook(cb);
	cb[0][0].setType(ChessPiece::NONE);
	cb[1][0].setType(ChessPiece::ROOK);
	cb[1][0].setOwner(RL_BLACK);
	CPPUNIT_ASSERT(stdrule_getpath(pathlist, cb, cb[1][0]));
	ICCSMotion m("a8f8");
	CPPUNIT_ASSERT(inpath(m, pathlist));
	
	// check for PAWN
	stdrule_initchessbook(cb);
	cb[4][6].setType(ChessPiece::PAWN);
	cb[4][6].setOwner(RL_RED);
	pathlist.clear();
	CPPUNIT_ASSERT(stdrule_getpath(pathlist, cb, cb[4][6]));
	CPPUNIT_ASSERT(inpath(*new ICCSMotion("g5f5"), pathlist));
}

void TestStdrule::testStdrule_initchessbook()
{
	ChessBook cb;
	cb[0][1].setType(ChessPiece::BISHOP);

	ChessBook* pcb = NULL;
	pcb = stdrule_initchessbook(cb);
	CPPUNIT_ASSERT(pcb);

	ChessBook cb1;
	fen_parse(cb1, "rnbakabnr/9/1c5c1/p1p1p1p1p/9/9/P1P1P1P1P/1C5C1/9/RNBAKABNR");
	CPPUNIT_ASSERT(cb.equal(cb1));
}

void TestStdrule::testStdrule_posvalid()
{
	ChessPiece piece_none;
	CPPUNIT_ASSERT(!stdrule_posvalid(1,1, piece_none));
	piece_none.setOwner(RL_RED);
	CPPUNIT_ASSERT(!stdrule_posvalid(1,1, piece_none));
	
	// 车, 马, 炮可以位于棋盘上任意位置
	ChessPiece rook(RL_RED, ChessPiece::ROOK);
	ChessPiece cannon(RL_RED, ChessPiece::CANNON);
	ChessPiece knight(RL_RED, ChessPiece::KNIGHT);
	for (int row = 0; row < CHESSBOOK_ROW; row ++) {
		for (int col = 0; col < CHESSBOOK_COL; col ++) {
			CPPUNIT_ASSERT(stdrule_posvalid(col, row, rook));
			CPPUNIT_ASSERT(stdrule_posvalid(col, row, cannon));
			CPPUNIT_ASSERT(stdrule_posvalid(col, row, knight));
		}
	}
	
	ChessPiece pawn_1(RL_BLACK, ChessPiece::PAWN);
	CPPUNIT_ASSERT(stdrule_posvalid(0, 3, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(2, 3, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(4, 3, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(6, 3, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(8, 3, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(0, 4, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(2, 4, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(4, 4, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(6, 4, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(8, 4, pawn_1));
	// bad pos
	CPPUNIT_ASSERT(!stdrule_posvalid(7, 4, pawn_1));
	// in black area
	CPPUNIT_ASSERT(stdrule_posvalid(8, 5, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(7, 5, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(6, 5, pawn_1));
	pawn_1.setOwner(RL_RED);
	CPPUNIT_ASSERT(stdrule_posvalid(0, 5, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(2, 5, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(4, 5, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(6, 5, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(8, 5, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(0, 6, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(2, 6, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(4, 6, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(6, 6, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(8, 6, pawn_1));
	// bad pos
	CPPUNIT_ASSERT(!stdrule_posvalid(7, 5, pawn_1));
	// in black area
	CPPUNIT_ASSERT(stdrule_posvalid(8, 4, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(7, 4, pawn_1));
	CPPUNIT_ASSERT(stdrule_posvalid(6, 4, pawn_1));
	
	// 帅
	ChessPiece king_1(RL_BLACK, ChessPiece::KING);
	// bad pos
	CPPUNIT_ASSERT(!stdrule_posvalid(0, 0, king_1));
	// bad
	CPPUNIT_ASSERT(!stdrule_posvalid(4, 9, king_1));
	// ok
	CPPUNIT_ASSERT(stdrule_posvalid(4, 1, king_1));
	king_1.setOwner(RL_RED);
	// bad pos
	CPPUNIT_ASSERT(!stdrule_posvalid(0, 9, king_1));
	// bad
	CPPUNIT_ASSERT(!stdrule_posvalid(4, 0, king_1));
	// ok
	CPPUNIT_ASSERT(stdrule_posvalid(4, 8, king_1));
	
	// 士
	ChessPiece advisor_1(RL_BLACK, ChessPiece::ADVISOR);
	// bad pos
	CPPUNIT_ASSERT(!stdrule_posvalid(0, 0, advisor_1));
	// bad
	CPPUNIT_ASSERT(!stdrule_posvalid(4, 8, advisor_1));
	// ok
	CPPUNIT_ASSERT(stdrule_posvalid(4, 1, advisor_1));
	advisor_1.setOwner(RL_RED);
	// bad pos
	CPPUNIT_ASSERT(!stdrule_posvalid(0, 9, advisor_1));
	// bad
	CPPUNIT_ASSERT(!stdrule_posvalid(4, 1, advisor_1));
	// ok
	CPPUNIT_ASSERT(stdrule_posvalid(4, 8, advisor_1));
	
	// 象
	ChessPiece bishop_1(RL_BLACK, ChessPiece::BISHOP);
	// bad pos
	CPPUNIT_ASSERT(!stdrule_posvalid(0, 0, bishop_1));
	// bad
	CPPUNIT_ASSERT(!stdrule_posvalid(4, 7, bishop_1));
	// ok
	CPPUNIT_ASSERT(stdrule_posvalid(4, 2, bishop_1));
	bishop_1.setOwner(RL_RED);
	// bad pos
	CPPUNIT_ASSERT(!stdrule_posvalid(0, 9, bishop_1));
	// bad
	CPPUNIT_ASSERT(!stdrule_posvalid(4, 2, bishop_1));
	// ok
	CPPUNIT_ASSERT(stdrule_posvalid(4, 7, bishop_1));
}

void TestStdrule::testStdrule_whowin()
{
	ChessBook cb1;
	fen_parse(cb1, "4k4/c8/9/9/9/9/9/9/9/4K4");
	
	PieceMotion piecemotion;
	
	piecemotion.end.setX(0);
	piecemotion.end.setY(1);
	
	CPPUNIT_ASSERT_EQUAL(RL_RED, stdrule_whowin(cb1, piecemotion));
}