/*
 * File:   TestChessArea.cc
 * Author: alec
 *
 * Created on 2012-6-13, 11:24:39
 */

#include "TestChessArea.h"
#include "src/include/cnchess/chessarea.h"
#include "cnchess.h"

using namespace std;
using namespace cnchess;

CPPUNIT_TEST_SUITE_REGISTRATION(TestChessArea);

TestChessArea::TestChessArea()
{
}

TestChessArea::~TestChessArea()
{
}

void TestChessArea::setUp()
{
}

void TestChessArea::tearDown()
{
}

void TestChessArea::testChessArea()
{
	ChessArea area;
	
	CPPUNIT_ASSERT(area.ltop.getX() == 0);
	CPPUNIT_ASSERT(area.ltop.getY() == 0);
	CPPUNIT_ASSERT(area.rtop.getX() == 8);
	CPPUNIT_ASSERT(area.rtop.getY() == 0);
	CPPUNIT_ASSERT(area.lbtm.getX() == 0);
	CPPUNIT_ASSERT(area.lbtm.getY() == 9);
	CPPUNIT_ASSERT(area.rbtm.getX() == 8);
	CPPUNIT_ASSERT(area.rbtm.getY() == 9);
}

void TestChessArea::testChessarea_contains()
{
	ChessArea area;
	CPPUNIT_ASSERT(chessarea_contains(area, 0, 0));
	chessarea_contains(area, 5, 5);
	CPPUNIT_ASSERT(chessarea_contains(area, 5, 5));
	
	area.ltop.setY(2);
	area.rtop.setY(2);
	
	CPPUNIT_ASSERT(!chessarea_contains(area, 0, 0));
	CPPUNIT_ASSERT(chessarea_contains(area, 0, 2));
}

void TestChessArea::testChessarea_contains2()
{
	;
}

void TestChessArea::testChessarea_mkblack()
{
	ChessArea area;
	chessarea_mkblack(area);
	CPPUNIT_ASSERT(!chessarea_contains(area, 0, 5));
	CPPUNIT_ASSERT(chessarea_contains(area, 0, 0));
}

void TestChessArea::testChessarea_mkking()
{
	ChessArea area;
	chessarea_mkking(area, RED);
	CPPUNIT_ASSERT(!chessarea_contains(area, 0, 0));
	CPPUNIT_ASSERT(chessarea_contains(area, 4, 8));
	
	chessarea_mkking(area, BLACK);
	CPPUNIT_ASSERT(!chessarea_contains(area, 4, 8));
	CPPUNIT_ASSERT(chessarea_contains(area, 4, 1));
}

void TestChessArea::testChessarea_mkred()
{
	ChessArea area;
	chessarea_mkred(area);
	CPPUNIT_ASSERT(chessarea_contains(area, 0, 5));
	CPPUNIT_ASSERT(!chessarea_contains(area, 0, 0));
}

void TestChessArea::testChessarea_torange()
{
	ChessArea area;
	chessarea_mkking(area, BLACK);
	PiecePosRange range;
	chessarea_torange(area, range);
	PiecePos pos(4, 1);
	CPPUNIT_ASSERT(pos.inrange(range));
}

