/*
 * File:   TestTimer.cc
 * Author: alec
 *
 * Created on 2012-5-17, 10:14:58
 */

#include <stdio.h>
#include <unistd.h>
#include "TestTimer.h"
#include "src/include/cnchess/timer.h"
#include "cnchess.h"

using namespace std;
using namespace cnchess;

void dummy_expired_listener(const Timer* timer, void* args)
{
	
}

void print_expired_listener(const Timer* timer, void* args)
{
	printf("dummy_expired_listener fire\n");
}

static volatile int g_counter = 0;

void timer_expired_listener(const Timer* timer, void* args)
{
	g_counter ++;
}

CPPUNIT_TEST_SUITE_REGISTRATION(TestTimer);

TestTimer::TestTimer()
{
}

TestTimer::~TestTimer()
{
}

void TestTimer::setUp()
{
}

void TestTimer::tearDown()
{
}

void TestTimer::testTimer()
{
	long seconds = 0;
	CPPUNIT_ASSERT_THROW(new Timer(seconds), invalid_argument);
}

void TestTimer::testAddExpiredListener()
{
	cnchess::Timer timer(2);
	CPPUNIT_ASSERT(timer.getExpiredListeners().size() == 0);
	timer.addExpiredListener(dummy_expired_listener);
	CPPUNIT_ASSERT(timer.getExpiredListeners().size() == 1);
	timer.removeExpiredListener(dummy_expired_listener);
	CPPUNIT_ASSERT(timer.getExpiredListeners().size() == 0);
	timer.addExpiredListener(timer_expired_listener);
	g_counter = 0;
	timer.start();
	sleep(3);
	CPPUNIT_ASSERT(g_counter);
}

void TestTimer::testGetStatus()
{
	cnchess::Timer timer(5);
	int result = timer.getStatus();
	CPPUNIT_ASSERT(result == Timer::PAUSED);
}

void TestTimer::testRemainTime()
{
	cnchess::Timer timer(4);
	timer.addExpiredListener(print_expired_listener);
	long result = timer.remainTime();
	CPPUNIT_ASSERT(result == -1);
	timer.start();
	sleep(2);
	CPPUNIT_ASSERT_EQUAL((int)Timer::STARTED, timer.getStatus());
	CPPUNIT_ASSERT_EQUAL(2L, timer.remainTime());
	
	
	timer.restart();
	usleep(2600000);
	// remain 1.4 sec
	CPPUNIT_ASSERT_EQUAL(1L, timer.remainTime());
	
	timer.restart();
	usleep(2300000);
	// remain 1.7 sec
	CPPUNIT_ASSERT_EQUAL(2L, timer.remainTime());
}

void TestTimer::testRemoveExpiredListener()
{
	;
}

void TestTimer::testRestart()
{
	;
}

void TestTimer::testStart()
{
	cnchess::Timer timer(1);
	timer.start();
	CPPUNIT_ASSERT_EQUAL((int)Timer::STARTED, timer.getStatus());
	usleep(1500000);
	CPPUNIT_ASSERT_EQUAL((int)Timer::PAUSED, timer.getStatus());

	for (int i = 0; i < 100; i ++) {
		timer.start();
		timer.stop();
	}
}

void TestTimer::testStop()
{
	cnchess::Timer timer(3);
	timer.addExpiredListener(timer_expired_listener);
	g_counter = 0;
	timer.start();
	sleep(1);
	timer.stop();
	CPPUNIT_ASSERT(0 == g_counter);
}

void TestTimer::testTotalTime()
{
	cnchess::Timer timer(5);
	long result = timer.totalTime();
	CPPUNIT_ASSERT_EQUAL(5L, result);
}

