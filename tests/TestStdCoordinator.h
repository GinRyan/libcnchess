/*
 * File:   TestStdCoordinator.h
 * Author: alec
 *
 * Created on 2012-5-28, 11:08:16
 */

#ifndef TESTSTDCOORDINATOR_H
#define	TESTSTDCOORDINATOR_H

#include <cppunit/extensions/HelperMacros.h>

class TestStdCoordinator : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestStdCoordinator);

	CPPUNIT_TEST(testMove);
	CPPUNIT_TEST(testRequestDraw);
	CPPUNIT_TEST(testRequestGiveUp);
	CPPUNIT_TEST(testRequestMove);
	CPPUNIT_TEST(testRequestRetract);

	CPPUNIT_TEST_SUITE_END();

public:
	TestStdCoordinator();
	virtual ~TestStdCoordinator();
	void setUp();
	void tearDown();

private:
	void testMove();
	void testRequestDraw();
	void testRequestGiveUp();
	void testRequestMove();
	void testRequestRetract();

};

#endif	/* TESTSTDCOORDINATOR_H */

