/*
 * File:   TestFEN.h
 * Author: alec
 *
 * Created on 2012-5-15, 18:16:16
 */

#ifndef TESTFEN_H
#define	TESTFEN_H

#include <cppunit/extensions/HelperMacros.h>

class TestFEN : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestFEN);

	CPPUNIT_TEST(testChar2piece);
	CPPUNIT_TEST(testFen_parse);
	CPPUNIT_TEST(testFen_parse2);
	CPPUNIT_TEST(testFen_parse3);
	CPPUNIT_TEST(testFen_parse4);
	CPPUNIT_TEST(testFen_parse5);
	CPPUNIT_TEST(testFen_parse6);
	CPPUNIT_TEST(testFen_parse7);
	CPPUNIT_TEST(testFen_parse8);
	CPPUNIT_TEST(testFen_tostring);
	CPPUNIT_TEST(testFen_tostring2);
	CPPUNIT_TEST(testIs_piece);
	CPPUNIT_TEST(testPiece2chiness);

	CPPUNIT_TEST_SUITE_END();

public:
	TestFEN();
	virtual ~TestFEN();
	void setUp();
	void tearDown();

private:
	void testChar2piece();
	void testFen_parse();
	void testFen_parse2();
	void testFen_parse3();
	void testFen_parse4();
	void testFen_parse5();
	void testFen_parse6();
	void testFen_parse7();
	void testFen_parse8();
	void testFen_tostring();
	void testFen_tostring2();
	void testIs_piece();
	void testPiece2chiness();

};

#endif	/* TESTFEN_H */

