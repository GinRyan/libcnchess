/*
 * File:   TestICCSMotion.cc
 * Author: alec
 *
 * Created on 2012-5-16, 11:41:58
 */

#include "TestICCSMotion.h"
#include "src/include/cnchess/iccsmotion.h"
#include "cnchess.h"
#include "utility.h"

using namespace std;
using namespace cnchess;

CPPUNIT_TEST_SUITE_REGISTRATION(TestICCSMotion);

TestICCSMotion::TestICCSMotion()
{
}

TestICCSMotion::~TestICCSMotion()
{
}

void TestICCSMotion::setUp()
{
}

void TestICCSMotion::tearDown()
{
}

void TestICCSMotion::testIccsmotion_apply()
{
	ChessBook cb;
	stdrule_initchessbook(cb);
	// 黑方炮二平五
	std::string str = "h2e2";
	CPPUNIT_ASSERT(iccsmotion_apply(cb, str));
	CPPUNIT_ASSERT(cb[7][7].isType(ChessPiece::NONE));
	CPPUNIT_ASSERT(cb[7][4].isType(ChessPiece::CANNON));
	
	// 再走 h2e2, h2 位置上已经没有子了, 应该失败返回 NULL
	CPPUNIT_ASSERT(NULL == iccsmotion_apply(cb, str));
	
	CPPUNIT_ASSERT(NULL == iccsmotion_apply(cb, "invalid str"));
}

void TestICCSMotion::testIccsmotion_apply2()
{
	;
}

void TestICCSMotion::testIccsmotion_isvalid()
{
	CPPUNIT_ASSERT(iccsmotion_isvalid("h2-e2"));
	CPPUNIT_ASSERT(iccsmotion_isvalid("h2e2"));
	CPPUNIT_ASSERT(iccsmotion_isvalid("H2E2"));
	CPPUNIT_ASSERT(iccsmotion_isvalid(" h2e2 "));
	
	for (int i = 0; i < 9; i ++) {
		string s;
		s.append(1, iccsmotion_x_range[i]);
		s.append(asstring(iccsmotion_y_range[i]));
		s.append("-e2");
		CPPUNIT_ASSERT(iccsmotion_isvalid(s));
	}
	
	CPPUNIT_ASSERT(!iccsmotion_isvalid("e2e2"));
	CPPUNIT_ASSERT(!iccsmotion_isvalid("h2 e2"));
	CPPUNIT_ASSERT(!iccsmotion_isvalid("2h2e"));
	CPPUNIT_ASSERT(!iccsmotion_isvalid("x2e2"));
	CPPUNIT_ASSERT(!iccsmotion_isvalid("e2e10"));
}

void TestICCSMotion::testIccsmotion_isvalid2()
{
	;
}

void TestICCSMotion::testIccsmotion_getpiece()
{
	ChessBook cb;
	stdrule_initchessbook(cb);
	ICCSPos ok_pos('i', 1);
	CPPUNIT_ASSERT(iccsmotion_getpiece(cb, ok_pos));
}

void TestICCSMotion::testIccsmotion_parse()
{
	ICCSMotion motion;
	string str = "e2h2";
	
	ICCSMotion* result = iccsmotion_parse(motion, str);
	CPPUNIT_ASSERT(result == &motion);
	CPPUNIT_ASSERT(result->start.getX() == 'E');
	CPPUNIT_ASSERT(result->start.getY() == 2);
	CPPUNIT_ASSERT(result->end.getX() == 'H');
	CPPUNIT_ASSERT(result->end.getY() == 2);
	
	result = iccsmotion_parse(motion, "invalid");
	CPPUNIT_ASSERT(!result);
}

void TestICCSMotion::testIccsmotion_tostring()
{
	ICCSMotion motion;
	string str = "E2H2";
	iccsmotion_parse(motion, str);
	
	std::string newstr;
	iccsmotion_tostring(newstr, motion);
	CPPUNIT_ASSERT(str == newstr);
	
	// bad motion
	CPPUNIT_ASSERT_THROW(motion.start.setY(100), invalid_argument);
}

void TestICCSMotion::testIccsmotion_xpos4chessbook()
{
	;
}

void TestICCSMotion::testIccsmotion_ypos4chessbook()
{
	;
}
