/*
 * File:   TestAlarm.cc
 * Author: alec
 *
 * Created on 2012-5-19, 3:24:25
 */

#include "TestAlarm.h"
#include "src/include/cnchess/alarm.h"
#include "cnchess.h"
#include "utility.h"
#include "clock.h"
#include <unistd.h>

using namespace std;
using namespace cnchess;


static volatile int g_counter = 0;

void counter_listenr(const Alarm* alarm, void* ctx)
{
	g_counter ++;
}


CPPUNIT_TEST_SUITE_REGISTRATION(TestAlarm);

TestAlarm::TestAlarm()
{
}

TestAlarm::~TestAlarm()
{
}

void TestAlarm::setUp()
{
}

void TestAlarm::tearDown()
{
}

void TestAlarm::testAlarm()
{
	IClock* clock = RealClock::create(1000000);
	CPPUNIT_ASSERT_THROW(new Alarm(clock, 100), invalid_argument);
	clock->decref();
}

void TestAlarm::testGetExpireTime()
{
	IClock* clock = RealClock::create(1000000);
	cnchess::Alarm alarm(clock, 1000000);
	
	CPPUNIT_ASSERT_EQUAL(1000000L, alarm.getExpireTime());

	clock->decref();
}

void TestAlarm::testGetRemainTime()
{
	IClock* clock = RealClock::create(1000000);
	clock->start();
	cnchess::Alarm alarm(clock, 5000);
	CPPUNIT_ASSERT_EQUAL(-1L, alarm.getRemainTime());
	alarm.start();
	int i = 0;
	while (i < 5) {
		sleepfor(1000 * 1);
		if (alarm.getStatus() == STARTED) {
			CPPUNIT_ASSERT_EQUAL((long)((5 - i - 1) * 1000), alarm.getRemainTime());
		}
		i ++;
	}
	alarm.stop();
	clock->decref();
}

void TestAlarm::testStart()
{
	IClock* clock = RealClock::create(1000000);
	cnchess::Alarm alarm(clock, 2000);
	
	CPPUNIT_ASSERT_EQUAL(IDLE, alarm.getStatus());
	
	clock->start();
	alarm.addListener(counter_listenr);
	alarm.start();
	
	CPPUNIT_ASSERT_EQUAL(STARTED, alarm.getStatus());
	
	sleepfor(1000 * 2);
	
	CPPUNIT_ASSERT_EQUAL((int)1, (int)g_counter);
	CPPUNIT_ASSERT_EQUAL(STOPPED, alarm.getStatus());
	
	cnchess::Alarm alarm1(clock, 1000, Alarm::REPEAT);
	g_counter = 0;
	alarm1.addListener(counter_listenr);
	alarm1.start();
	
	sleepfor(1000 * 5);
	
	CPPUNIT_ASSERT_EQUAL((int)5, (int)g_counter);
	CPPUNIT_ASSERT_EQUAL(STARTED, alarm1.getStatus());
	clock->decref();
}

void TestAlarm::testStop()
{
	IClock* clock = RealClock::create(1000000);
	clock->start();
	cnchess::Alarm alarm1(clock, 1000, Alarm::REPEAT);
	g_counter = 0;
	alarm1.addListener(counter_listenr);
	alarm1.start();
	
	sleepfor(1000 * 2);
	alarm1.stop();
	sleepfor(1000 * 3);
	
	CPPUNIT_ASSERT_EQUAL((int)2, (int)g_counter);
	CPPUNIT_ASSERT_EQUAL(STOPPED, alarm1.getStatus());
	clock->decref();
}

