/*
 * File:   TestPiecePos.cc
 * Author: alec
 *
 * Created on 2012-6-13, 11:17:39
 */

#include "TestPiecePos.h"
#include "src/include/cnchess/piecepos.h"
#include "cnchess.h"


using namespace std;
using namespace cnchess;

CPPUNIT_TEST_SUITE_REGISTRATION(TestPiecePos);

TestPiecePos::TestPiecePos()
{
}

TestPiecePos::~TestPiecePos()
{
}

void TestPiecePos::setUp()
{
}

void TestPiecePos::tearDown()
{
}

void TestPiecePos::testPiecePos()
{
	CPPUNIT_ASSERT_THROW(new cnchess::PiecePos(100, 100), invalid_argument);
	PiecePos pos(1, 2);
	CPPUNIT_ASSERT_EQUAL(1, pos.getX());
	CPPUNIT_ASSERT_EQUAL(2, pos.getY());
}

void TestPiecePos::testEqual()
{
	PiecePos pos1(1, 2);
	PiecePos pos2(1, 2);
	PiecePos pos3(2, 2);
	
	CPPUNIT_ASSERT(pos1.equal(pos2));
	CPPUNIT_ASSERT(!pos1.equal(pos3));
}

void TestPiecePos::testInrange()
{
	PiecePosRange range;
	range.push_back(PiecePos(1, 2));
	range.push_back(PiecePos(3, 2));
	
	PiecePos pos1(1, 2);
	CPPUNIT_ASSERT(pos1.inrange(range));
	
	PiecePos pos2(2, 2);
	CPPUNIT_ASSERT(!pos2.inrange(range));
}

