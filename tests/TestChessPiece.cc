/*
 * File:   TestChessPiece.cc
 * Author: alec
 *
 * Created on 2012-5-15, 17:17:59
 */

#include "TestChessPiece.h"
#include "src/include/cnchess/chesspiece.h"

using namespace std;
using namespace cnchess;

CPPUNIT_TEST_SUITE_REGISTRATION(TestChessPiece);

TestChessPiece::TestChessPiece()
{
}

TestChessPiece::~TestChessPiece()
{
}

void TestChessPiece::setUp()
{
}

void TestChessPiece::tearDown()
{
}

void TestChessPiece::testChessPiece()
{
	cnchess::ChessPiece chessPiece;
	if (true /*check result*/) {
		CPPUNIT_ASSERT(chessPiece.isType(cnchess::ChessPiece::NONE));
		CPPUNIT_ASSERT(chessPiece.getOwner() == RL_NONE);
	}
}

void TestChessPiece::testGetOwner()
{
	cnchess::ChessPiece chessPiece(RL_BLACK);
	GameRole result = chessPiece.getOwner();
	if (true /*check result*/) {
		CPPUNIT_ASSERT(result == RL_BLACK);
	}
}

void TestChessPiece::testGetType()
{
	cnchess::ChessPiece chessPiece(RL_BLACK, ChessPiece::ROOK);
	if (true /*check result*/) {
		CPPUNIT_ASSERT(chessPiece.getType() == ChessPiece::ROOK);
	}
}

void TestChessPiece::testIsType()
{
	cnchess::ChessPiece chessPiece(RL_BLACK, ChessPiece::ROOK);
	if (true /*check result*/) {
		CPPUNIT_ASSERT(chessPiece.isType(ChessPiece::ROOK));
	}
}

void TestChessPiece::testSetOwner()
{
	GameRole owner = RL_BLACK;
	cnchess::ChessPiece chessPiece;
	chessPiece.setOwner(owner);
	if (true /*check result*/) {
		CPPUNIT_ASSERT(chessPiece.getOwner() == owner);
	}
}

void TestChessPiece::testSetType()
{
	ChessPiece::Type piece = ChessPiece::KING;
	cnchess::ChessPiece chessPiece;
	chessPiece.setType(piece);
	if (true /*check result*/) {
		CPPUNIT_ASSERT(chessPiece.isType(ChessPiece::KING));
	}
}

