/*
 * File:   TestICCSMotion.h
 * Author: alec
 *
 * Created on 2012-5-16, 11:41:58
 */

#ifndef TESTICCSMOTION_H
#define	TESTICCSMOTION_H

#include <cppunit/extensions/HelperMacros.h>

class TestICCSMotion : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestICCSMotion);

	CPPUNIT_TEST(testIccsmotion_apply);
	CPPUNIT_TEST(testIccsmotion_apply2);
	CPPUNIT_TEST(testIccsmotion_isvalid);
	CPPUNIT_TEST(testIccsmotion_isvalid2);
	CPPUNIT_TEST(testIccsmotion_parse);
	CPPUNIT_TEST(testIccsmotion_tostring);
	CPPUNIT_TEST(testIccsmotion_xpos4chessbook);
	CPPUNIT_TEST(testIccsmotion_ypos4chessbook);
	CPPUNIT_TEST(testIccsmotion_getpiece);
	CPPUNIT_TEST_SUITE_END();

public:
	TestICCSMotion();
	virtual ~TestICCSMotion();
	void setUp();
	void tearDown();

private:
	void testIccsmotion_apply();
	void testIccsmotion_apply2();
	void testIccsmotion_isvalid();
	void testIccsmotion_isvalid2();
	void testIccsmotion_parse();
	void testIccsmotion_tostring();
	void testIccsmotion_xpos4chessbook();
	void testIccsmotion_ypos4chessbook();
	void testIccsmotion_getpiece();

};

#endif	/* TESTICCSMOTION_H */

