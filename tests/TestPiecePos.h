/*
 * File:   TestPiecePos.h
 * Author: alec
 *
 * Created on 2012-6-13, 11:17:38
 */

#ifndef TESTPIECEPOS_H
#define	TESTPIECEPOS_H

#include <cppunit/extensions/HelperMacros.h>

class TestPiecePos : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestPiecePos);

	CPPUNIT_TEST(testPiecePos);
	CPPUNIT_TEST(testEqual);
	CPPUNIT_TEST(testInrange);

	CPPUNIT_TEST_SUITE_END();

public:
	TestPiecePos();
	virtual ~TestPiecePos();
	void setUp();
	void tearDown();

private:
	void testPiecePos();
	void testEqual();
	void testInrange();

};

#endif	/* TESTPIECEPOS_H */

