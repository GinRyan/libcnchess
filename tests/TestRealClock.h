/*
 * File:   TestRealClock.h
 * Author: alec
 *
 * Created on 2012-5-18, 17:22:43
 */

#ifndef TESTREALCLOCK_H
#define	TESTREALCLOCK_H

#include <cppunit/extensions/HelperMacros.h>

class TestRealClock : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestRealClock);

	CPPUNIT_TEST(testRealClock);
	CPPUNIT_TEST(testRealClock2);
	CPPUNIT_TEST(testAddListener);
	CPPUNIT_TEST(testGetInterval);
	CPPUNIT_TEST(testGetStatus);
	CPPUNIT_TEST(testRemoveListener);
	CPPUNIT_TEST(testStart);
	CPPUNIT_TEST(testStop);
	CPPUNIT_TEST(testCreate);
	
	CPPUNIT_TEST_SUITE_END();

public:
	TestRealClock();
	virtual ~TestRealClock();
	void setUp();
	void tearDown();

private:
	void testRealClock();
	void testRealClock2();
	void testAddListener();
	void testGetInterval();
	void testGetStatus();
	void testRemoveListener();
	void testStart();
	void testStop();
	void testCreate();

};

#endif	/* TESTREALCLOCK_H */

