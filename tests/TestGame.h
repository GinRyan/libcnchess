/*
 * File:   TestGame.h
 * Author: alec
 *
 * Created on 2012-5-24, 14:56:40
 */

#ifndef TESTGAME_H
#define	TESTGAME_H

#include <cppunit/extensions/HelperMacros.h>

class TestGame : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(TestGame);

	CPPUNIT_TEST(testGame);
	CPPUNIT_TEST(testGame2);
	CPPUNIT_TEST(testGetAdversary);
	CPPUNIT_TEST(testGetBoard);
	CPPUNIT_TEST(testGetChessBook);
	CPPUNIT_TEST(testGetCurrent);
	CPPUNIT_TEST(testGetFirst);
	CPPUNIT_TEST(testGetHistory);
	CPPUNIT_TEST(testGetNext);
	CPPUNIT_TEST(testGetPlayer);
	CPPUNIT_TEST(testGetReferee);
	CPPUNIT_TEST(testGetRole);
	CPPUNIT_TEST(testGetStatus);

	CPPUNIT_TEST_SUITE_END();

public:
	TestGame();
	virtual ~TestGame();
	void setUp();
	void tearDown();

private:
	void testGame();
	void testGame2();
	void testGetAdversary();
	void testGetBoard();
	void testGetChessBook();
	void testGetCurrent();
	void testGetFirst();
	void testGetHistory();
	void testGetNext();
	void testGetPlayer();
	void testGetReferee();
	void testGetRole();
	void testGetStatus();

};

#endif	/* TESTGAME_H */

